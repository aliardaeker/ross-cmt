#ifndef INC_gvt_mattern_orig_h
#define INC_gvt_mattern_orig_h


static inline unsigned long long get_tsc() 
{
    unsigned int a, d;
    asm volatile("lfence; rdtsc; lfence" : "=a" (a), "=d" (d));
    return (unsigned long) a | (((unsigned long) d) << 32);
}

static inline int 
tw_gvt_inprogress(tw_pe * me) 
{
	// when interval counter is 0, gvt is being computed
	if (!(me -> interval_counter)) return 1;
	else return 0;
}

static inline void 
gvt_print(tw_pe * me)
{

	fprintf(stderr, "GVT #%d: simulation %d%% complete (", 
		g_tw_gvt_done, (int) min(100, floor(100 * (me -> GVT / g_tw_ts_end))));

	if (me -> GVT == DBL_MAX) fprintf(stderr, "GVT = %s", "MAX");
	else fprintf(stderr, "GVT = %.4f", me -> GVT);
	fprintf(stderr, ")");
    fprintf(stderr, "\n"); 

    #ifdef disparity_check
	static double std_avg = 0;
	
	if ((int) min(100, floor(100 * (me -> GVT / g_tw_ts_end))) < 100)
	{
		double avg = 0, total = 0, dist_to_mean = 0, std_dev = 0;

		for (int i = 1; i < g_tw_n_threads; i++) total += lvts[i].lvt;	
		avg = total / (g_tw_n_threads - 1);		
		
		for (int i = 1; i < g_tw_n_threads; i++) dist_to_mean += (lvts[i].lvt - avg) * (lvts[i].lvt - avg);
		std_dev = sqrt(dist_to_mean / (g_tw_n_threads - 1));	
		
		me -> std_dev += std_dev;

		std_avg += std_dev;
		//fprintf(stderr, "Std dev: %.2f\n", std_dev);
	}
	else fprintf(stderr, "\nStd dev avg: %.2f\n", std_avg / g_tw_gvt_done);
	#endif

}
#endif
