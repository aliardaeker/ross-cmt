#include <ross.h>
#include <stddef.h>

#ifdef SL_BARRIER	
static pthread_barrier_t fossil_barrier;
#endif

// Statistics
//static __thread unsigned int gvt_force = 0;
static __thread tw_stime measure_fc = 0;
static __thread tw_stime measure_gvt = 0;
static __thread tw_stime mpi_b = 0;

static int c1_counter; 		// Counts how many threads reached the C 1
static int c1_completed;
static int c2_counter;		// Counts how many threads reached the C 2
static int c2_completed;

static int gvt_done_counter;
static double global_gvt;

#define num_nodes 8
typedef struct control // control block
{
	double t_min;
	double t_red;
	int msg_counters[256][num_nodes]; // [0][1] holds number of messages sent from all thread to the thread 0 in machine 1
} _control;

static _control control_message;
static pthread_mutex_t cm_lock;

static MPI_Datatype mpi_control_message;
static MPI_Op custom_op;

void 
tw_gvt_start()
{
	if (!g_tw_tid)
	{
		c1_completed = 0;
		c1_counter = 0;
		c2_counter = 0;
		c2_completed = 0;
		gvt_done_counter = 0;
		reset_control_message();

		MPI_Aint displacements[3] = {offsetof(_control, t_min), 
				     	     offsetof(_control, t_red),
				             offsetof(_control, msg_counters)};
		
		int block_lengths[3] = {1, 1, no_threads * g_tw_machines};
		MPI_Datatype types[3] = {MPI_DOUBLE, MPI_DOUBLE, MPI_INT};

		int rv = MPI_Type_create_struct(3, block_lengths, displacements, types, &mpi_control_message);
		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "mpi type creation failed!");
		
		rv = MPI_Type_commit(&mpi_control_message);
		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "mpi type commit failed!");	
		
		if (pthread_mutex_init(&cm_lock, NULL) != 0) tw_error(TW_LOC, "Mutex init failed\n");
	
		rv = MPI_Op_create(mpi_func, 1, &custom_op);
		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "mpi op create failed!");	
	}	
}	

void 
tw_gvt_step1(tw_pe * me)
{
	// C 1 is being constructed
	if (me -> color == white && !gvt_done_counter)
	{
		#ifdef DEBUG_M
		if (!g_tw_pid) fprintf(stderr, "%u becomes red (%.0f)\n", g_tw_tid, me -> GVT);
		else fprintf(stderr, "	%u becomes red (%.0f)\n", g_tw_tid, me -> GVT);
		#endif
		
		// Each thread checks the C 1 and accumulate its message counters
		measure_gvt = tw_clock_read();
		me -> color = red;
		me -> t_red = g_tw_ts_end;			
			
		accumulate_msg_counters(me); // control block is updated 
			
		__sync_add_and_fetch(&c1_counter, 1);	
	}
	// Each thread is done with C 1, now accumulate CM between the machines
	else if (c1_counter == g_tw_n_threads && !g_tw_tid)
	{
		#ifdef DEBUG_M
		if (!g_tw_pid) fprintf(stderr, "%u exchanges cm (%.0f)\n", g_tw_tid, me -> GVT);
		else fprintf(stderr, "	%u exchanges cm (%.0f)\n", g_tw_tid, me -> GVT);
		#endif
		
		if (g_tw_machines > 1) first_cut(me);
	
		// At this point the message accumulation should be done
		// Local cm of both machines should be same and has all the message counts
		c1_counter = 0;
		c1_completed = 1;
	}
	// C 2 is being contructed
	else if (me -> color == red && c1_completed)
	{
		//while (g_tw_tid && !me -> c2_checked)
		//if (g_tw_tid)
		{
			if (!me -> c2_checked && 
			     me -> msg_counters[g_tw_tid][g_tw_pid] + control_message.msg_counters[g_tw_tid][g_tw_pid] == 0)
			{
				#ifdef DEBUG_M
				if (!g_tw_pid) fprintf(stderr, "%u updates cm (%.0f)\n", g_tw_tid, me -> GVT);
				else fprintf(stderr, "	%u updates cm (%.0f)\n", g_tw_tid, me -> GVT);
				#endif

				me -> c2_checked = 1; 
				update_cm(me);			
				__sync_add_and_fetch(&c2_counter, 1);
				//break;
			}
			//else if (!me -> c2_checked) {__sync_add_and_fetch(&cm_check_fails, 1);}
			//else if (!me -> c2_checked) {me -> cm_check_fails++;}
			
			//tw_net_read(me);
		}		

		// Each thread is done with C 2, now update CM between the machines
		//if (c2_counter == g_tw_n_threads - 1 && !g_tw_tid)
		if (c2_counter == g_tw_n_threads && !g_tw_tid)
		{		
			#ifdef DEBUG_M
			if (!g_tw_pid) fprintf(stderr, "%u exchanges gvt (%.0f)\n", g_tw_tid, me -> GVT);
			else fprintf(stderr, "	%u exchanges gvt (%.0f)\n", g_tw_tid, me -> GVT);
			#endif
	
			if (g_tw_machines > 1) second_cut(me);	
            else global_gvt = min(control_message.t_min, control_message.t_red); // For single node

			c2_counter = 0;
			c2_completed = 1;
		}
		//else if (!g_tw_tid) {mpi_thread_waits_c2++;};

		if (c2_completed)
		{
			//if (g_tw_tid) lvts[g_tw_tid].lvt = me -> LVT;

			//if (!me -> id) gvt_prev = me -> GVT;
			fossil_collect(me);
			finish_gvt(me);

			#ifdef DEBUG_M
			if (!g_tw_pid) fprintf(stderr, "%u fossil (%.0f)\n", g_tw_tid, me -> GVT);
			else fprintf(stderr, "	%u fossil (%.0f)\n", g_tw_tid, me -> GVT);
			#endif

			//if (!(me -> id) && me -> GVT != gvt_prev) gvt_print(me -> GVT);
			if (me -> id == 0) gvt_print(me);
			
			if (__sync_add_and_fetch(&gvt_done_counter, 1) == g_tw_n_threads)
			{
				reset_control_message();
				c1_completed = 0;
				c2_completed = 0;
				gvt_done_counter = 0;
				
				//if (g_tw_pid == 0) gvt_print(me);
			} 
	
			me -> stats.s_gvt += tw_clock_read() - measure_gvt;
		}
	}
}

void
first_cut(tw_pe * me)
{
	//mpi_b = tw_clock_read();
	//if (!me -> id) exchange_cm(1);
	//else exchange_cm(0);
	//me -> stats.s_mpi_mattern_c1 += tw_clock_read() - mpi_b;
		
	_control cm;
	
	mpi_b = tw_clock_read();
    all_reduce_cm(&cm);
	me -> stats.s_mpi_mattern_c1 += tw_clock_read() - mpi_b;
        
	reset_local_cm_with_other_cm(&cm);
}

void 
second_cut(tw_pe * me)
{
	//mpi_b = tw_clock_read();
	//if (!me -> id) exchange_gvt(1);
	//else exchange_gvt(0);
	//me -> stats.s_mpi_mattern_c2 += tw_clock_read() - mpi_b;
			
	double local_gvt = min(control_message.t_min, control_message.t_red);
			
	mpi_b = tw_clock_read();
    all_reduce_gvt(&local_gvt);	
	me -> stats.s_mpi_mattern_c2 += tw_clock_read() - mpi_b;
}

void
tw_gvt_force_update(tw_pe * me) 
{
	me -> gvt_force++;
	//__sync_add_and_fetch(&gvt_force_global, 1);	
	//fprintf(stderr, "GVT forced\n");
	me -> interval_counter = 0;
}

void 
mpi_func(_control * in, _control * inout, int * len, MPI_Datatype * dptr)
{
	if (*len != 1) MPI_Abort(MPI_COMM_WORLD, 1);	

	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++)
		{
			inout -> msg_counters[i][j] = inout -> msg_counters[i][j] + in -> msg_counters[i][j];		
		}
	}	
}

void
all_reduce_cm(void * cm)
{
	int rv = MPI_Allreduce(
		&control_message,
		cm,
		1,
		mpi_control_message,
		custom_op,
		MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_ALL_REDUCE Failed");	
}

void
reset_local_cm_with_other_cm(_control * cm)
{
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++)
		{
			control_message.msg_counters[i][j] = cm -> msg_counters[i][j];
		}
	}
}

void
all_reduce_gvt(void * local_gvt)
{
	int rv = MPI_Allreduce(
			local_gvt,
			&global_gvt,
			1,
			MPI_DOUBLE,
			MPI_MIN,
			MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_ALL_REDUCE Failed");	
}

void
update_cm(tw_pe * me)
{
	#ifdef SEPARATE_1
		if (g_tw_tid > 1) update_control(me);
	#elif defined(SEPARATE_0)
		if (g_tw_tid) update_control(me);
	#else
		update_control(me);
	#endif
}

void 
update_control(tw_pe * me)
{
	// count lock fails here	

    /*
	int s = pthread_mutex_trylock(&cm_lock);

	if (!s) // try lock succeeded
	{
		control_message.t_min = min(control_message.t_min, min_pq_outq(me));
		control_message.t_red = min(control_message.t_red, me -> t_red);	
		pthread_mutex_unlock(&cm_lock);
	}
	else // try lock failed
	{
		//__sync_add_and_fetch(&try_lock_fail_counter, 1);
		me -> try_lock_fails++;

		pthread_mutex_lock(&cm_lock);
		control_message.t_min = min(control_message.t_min, min_pq_outq(me));
		control_message.t_red = min(control_message.t_red, me -> t_red);	
		pthread_mutex_unlock(&cm_lock);
	}
    */

	pthread_mutex_lock(&cm_lock);
	control_message.t_min = min(control_message.t_min, min_pq_outq(me));
	control_message.t_red = min(control_message.t_red, me -> t_red);	
	pthread_mutex_unlock(&cm_lock);
	
	me -> msg_counters[g_tw_tid][g_tw_pid] = 0;
}

void
print_local_cm()
{
	fprintf(stderr, "Local CM:\n");
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++) fprintf(stderr, "%d ", control_message.msg_counters[i][j]);
		fprintf(stderr, "\n");
	}
}

void
sync_local_cm_with_other_cm(_control * cm)
{
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++)
		{
			control_message.msg_counters[i][j] += cm -> msg_counters[i][j];
		}
	}
}

void 
accumulate_msg_counters(tw_pe * me)
{
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++)
		{
			if (i != g_tw_tid || j != g_tw_pid)
			{
				__sync_add_and_fetch(&control_message.msg_counters[i][j], me -> msg_counters[i][j]); 
				me -> msg_counters[i][j] = 0;
			}
		}
	}
}

void
reset_control_message()
{
	control_message.t_min = g_tw_ts_end + 1;
	control_message.t_red = g_tw_ts_end + 1;
	for (int i = 0; i < g_tw_n_threads; i++) for (int j = 0; j < g_tw_machines; j++) 
	{
		control_message.msg_counters[i][j] = 0;
	}
}

void 
fossil_collect(tw_pe * me)
{
	//me -> GVT = global_gvt - 1;
	me -> GVT = global_gvt;
	
	measure_fc = tw_clock_read();

	#ifdef SL_LOCKS
		//if (!g_tw_pid) fprintf(stderr, "Node %d enter, GVT = %lf\n", g_tw_mynode, gvt);
		//else fprintf(stderr, "	Node %d enter, GVT = %lf\n", g_tw_mynode, gvt);
		if (g_tw_tid == 1) tw_pe_fossil_collect(me);
	#elif defined(SL_BARRIER)
		pthread_barrier_wait(&fossil_barrier);
		//if (g_tw_tid == 1)  tw_pe_fossil_collect(me);
		if (!g_tw_tid)  tw_pe_fossil_collect(me);
		pthread_barrier_wait(&fossil_barrier);
	#elif defined(SEPARATE_1)
		if (g_tw_tid > 1) tw_pe_fossil_collect(me);
	#elif defined(SEPARATE_0)
		if (g_tw_tid) tw_pe_fossil_collect(me);
	#else
		tw_pe_fossil_collect(me);
	#endif
	
	me -> stats.s_fossil_collect += tw_clock_read() - measure_fc;
}

void 
finish_gvt(tw_pe * me)
{
	g_tw_gvt_done++;
	
	me -> interval_counter = g_tw_gvt_interval;
	me -> trans_msg_ts = DBL_MAX + 3;
	me -> c2_checked = 0;
	me -> color = white;
}

tw_stime
min_pq_outq(tw_pe * me)
{
	// tw_net_read(me); // might be neccessary

	// return the minimum of pq of PE	
	tw_stime pq_min = tw_pq_minimum(me -> pq);
	me -> pq_min = pq_min;

	// return the minimum of outq and posted_sends of PE. Buffers for mpi.
	// populated during network_send, extracted during send_begin
	tw_stime net_min = tw_net_minimum(me);
	me -> net_min = net_min;

	tw_stime t_msg = me -> trans_msg_ts;
	me -> trans_msg = t_msg;
	
	double lvt = t_msg;

	if (lvt > pq_min) lvt = pq_min;
	if (lvt > net_min) lvt = net_min;
	
	if (lvt > g_tw_ts_end) 
	{
		me -> LVT = g_tw_ts_end + 2;
		return g_tw_ts_end + 2;
	}
	else 
	{
		//me -> LVT = lvt; min of pq_min, net_min, t_msg
		//if (pq_min < g_tw_ts_end) me -> LVT = pq_min;
		//if (net_min < g_tw_ts_end) me -> LVT = net_min;

		return lvt;
	}
}

static const tw_optdef gvt_opts [] =
{
	TWOPT_GROUP("ROSS MPI GVT"),
	TWOPT_UINT("gvt-interval", g_tw_gvt_interval, "GVT Interval"),
	TWOPT_END()
};

const tw_optdef *
tw_gvt_setup(void) 
{
	#ifdef SL_BARRIER
	pthread_barrier_init(&fossil_barrier, NULL, no_threads);
	#endif
	
	return gvt_opts;
}

void tw_gvt_step2(tw_pe * me) {}

void
tw_gvt_stats(FILE * f)
{
	fprintf(f, "\nTW GVT Statistics: Mattern\n");
	fprintf(f, "\t%-50s %11d\n", "GVT Interval", g_tw_gvt_interval);
	fprintf(f, "\t%-50s %11d\n", "Batch Size", g_tw_mblock);
	//fprintf(f, "\t%-50s %11d\n", "Forced GVT", gvt_force);
	fprintf(f, "\t%-50s %11d\n", "Total GVT Computations", g_tw_gvt_done);
}

void
exchange_gvt(int receive_from)
{
	double local_gvt = min(control_message.t_min, control_message.t_red);
	
	// Collective Communication
	//all_reduce_gvt(&local_gvt);

	// Faster way
	gvt_send(&local_gvt, receive_from);
	gvt_receive(&global_gvt, receive_from);
	global_gvt = min(global_gvt, local_gvt);
}

void
gvt_send(double * gvt, int dest) 
{
	int rv = MPI_Send(gvt,
		    1,
		    MPI_DOUBLE,
		    dest, //(g_tw_pid + 1) % g_tw_machines, // Destination
		    3,
		    MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_SEND Failed");
}

void
gvt_receive(double * gvt, int source)
{
	//MPI_Status s;

	int rv = MPI_Recv(gvt, 
		    1, 
		    MPI_DOUBLE, 
		    source, // (g_tw_pid - 1) % g_tw_machines is not working somehow
	            3, 
		    MPI_COMM_WORLD, 
		    MPI_STATUS_IGNORE); //&s);

	//char str[MPI_MAX_ERROR_STRING];
	//int len;
	//MPI_Error_string(s.MPI_ERROR, str, &len);

	//if (!g_tw_pid) fprintf(stderr, "%s: %d\n", str, s.MPI_ERROR);
	//else fprintf(stderr, "	%s: %d\n", str, s.MPI_ERROR);
	
	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_RECV Failed");
}


void
exchange_cm(int receive_from)
{
	_control cm;
	
	// Collective Communication
	//all_reduce_cm(&cm);

	// Faster way
	cm_send(receive_from);	
	cm_receive(&cm, receive_from);
	sync_local_cm_with_other_cm(&cm);
}

void
cm_send(int dest) // sends local cm to the next machine in the ring
{
	int rv = MPI_Send(&control_message,
		    1,
		    mpi_control_message,
		    dest, //(g_tw_pid + 1) % g_tw_machines, // Destination
		    2,
		    MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_SEND Failed");
}

void
cm_receive(_control * cm ,int source) // receives local cm of the previous machine and writes it into global cm
{
	int rv = MPI_Recv(cm, 
		    1, 
		    mpi_control_message, 
		    source, // (g_tw_pid - 1) % g_tw_machines is not working somehow
	            2, 
		    MPI_COMM_WORLD, 
		    MPI_STATUS_IGNORE);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_RECV Failed");
}
