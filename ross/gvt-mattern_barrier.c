#include <ross.h>
#include <stddef.h>

#ifdef SL_BARRIER	
static pthread_barrier_t fossil_barrier;
#endif

// Statistics
static __thread tw_stime measure_fc = 0;
static __thread tw_stime measure_gvt = 0;
static __thread tw_stime mpi_b = 0;

// For imposing synchronization on Mattern`s GVT
static int node_sync_flag = 0;
static int global_sync_flag = 0;
//static int global_sync_flag_prev = 0;
static pthread_barrier_t sync_barrier;

static int c1_counter; 		// Counts how many threads reached the C 1
static int c1_completed;
static int c2_counter;		// Counts how many threads reached the C 2
static int c2_completed;

static int gvt_done_counter;
static double global_gvt;

#define num_nodes 2
#define eff_threshold 0.8 

typedef struct control // control block
{
	double t_min;
	double t_red;
	int msg_counters[256][num_nodes]; // [0][1] holds number of messages sent from all thread to the thread 0 in machine 1
} _control;

static _control control_message;
static pthread_mutex_t cm_lock;

static MPI_Datatype mpi_control_message;
static MPI_Op custom_op;

void 
tw_gvt_start()
{
	if (!g_tw_tid)
	{
		c1_completed = 0;
		c1_counter = 0;
		c2_counter = 0;
		c2_completed = 0;
		gvt_done_counter = 0;
		reset_control_message();

		MPI_Aint displacements[3] = {offsetof(_control, t_min), 
				     	     offsetof(_control, t_red),
				             offsetof(_control, msg_counters)};
		
		int block_lengths[3] = {1, 1, no_threads * g_tw_machines};
		MPI_Datatype types[3] = {MPI_DOUBLE, MPI_DOUBLE, MPI_INT};

		int rv = MPI_Type_create_struct(3, block_lengths, displacements, types, &mpi_control_message);
		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "mpi type creation failed!");
		
		rv = MPI_Type_commit(&mpi_control_message);
		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "mpi type commit failed!");	
		
		if (pthread_mutex_init(&cm_lock, NULL) != 0) tw_error(TW_LOC, "Mutex init failed\n");
	
		rv = MPI_Op_create(mpi_func, 1, &custom_op);
		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "mpi op create failed!");	
	}	
}	

void 
tw_gvt_step1(tw_pe * me)
{
	if (me -> color == white && !gvt_done_counter)
	{
		if (g_tw_tid == 1) compute_eff(me);

		// Each thread checks the C 1 and accumulate its message counters
		measure_gvt = tw_clock_read();
		me -> color = red;
		me -> t_red = g_tw_ts_end;			
			
		accumulate_msg_counters(me); // control block is updated 
			
		__sync_add_and_fetch(&c1_counter, 1);	
	}
	// Each thread is done with C 1, now accumulate CM between the machines
	else if (c1_counter == g_tw_n_threads && !g_tw_tid)
	{
        allreduce_barrier_flag(me);
		
        _control cm;

		mpi_b = tw_clock_read();		
		all_reduce_cm(&cm);
	    me -> stats.s_mpi_mattern_c2 += tw_clock_read() - mpi_b;

		reset_local_cm_with_other_cm(&cm);
		
		// At this point the message accumulation should be done
		// Local cm of both machines should be same and has all the message counts
		c1_counter = 0;
		c1_completed = 1;
	}
	// C 2 is being contructed
	else if (me -> color == red && c1_completed)
	{	 
        if (global_sync_flag)
        {
            synchronize(me);
            return;
		}

		if (!me -> c2_checked && 
		     me -> msg_counters[g_tw_tid][g_tw_pid] + control_message.msg_counters[g_tw_tid][g_tw_pid] == 0)
		{
			//if (global_sync_flag_prev) synchronize(me);

			me -> c2_checked = 1; 
			update_cm(me);			
			__sync_add_and_fetch(&c2_counter, 1);
		}
		
		// Each thread is done with C 2, now update CM between the machines
		if (c2_counter == g_tw_n_threads && !g_tw_tid)
		{		
			double local_gvt = min(control_message.t_min, control_message.t_red);
		    //global_gvt = min(control_message.t_min, control_message.t_red);

			mpi_b = tw_clock_read();
			all_reduce_gvt(&local_gvt);	
    	    me -> stats.s_mpi_mattern_c1 += tw_clock_read() - mpi_b;

			c2_counter = 0;
			c2_completed = 1;
		}

		if (c2_completed)
		{		
			//if (g_tw_tid == 0) allreduce_barrier_flag(me);

			fossil_collect(me);
			finish_gvt(me);
			
			//if (global_sync_flag_prev) synchronize(me);

			if (me -> id == 0) gvt_print(me);
			
			if (__sync_add_and_fetch(&gvt_done_counter, 1) == g_tw_n_threads)
			{	
				//compute_eff(me);
                //global_sync_flag_prev = global_sync_flag;

				reset_control_message();
				c1_completed = 0;
				c2_completed = 0;
				gvt_done_counter = 0;
			}
		}
	}
}

void
synchronize(tw_pe * me)
{
    tw_clock start_total = tw_clock_read();
	tw_stat local_white = 0;
	tw_stat total_white = 0;
	tw_stat node_total_white = 0;
			
	while (1)
	{
		tw_net_read(me);
		local_white = me -> s_nwhite_sent - me -> s_nwhite_recv;
		
		mt_allreduce_long_sum(&local_white, &total_white, &sum_barrier[sum_index]);
		sum_index = (sum_index + 1) % 2;

		if (g_tw_tid == 0)
		{
			//if (!g_tw_pid) fprintf(stderr, "%u come s\n", g_tw_tid);
			//else fprintf(stderr, "	%u come s\n", g_tw_tid);
	        	
			int rv = MPI_Allreduce(
				    &total_white, // reduce total_white accross machines with sum
		     		&node_total_white, // map it to node_total_white
		     		1,
		     		MPI_LONG_LONG,
		     		MPI_SUM,
		     		MPI_COMM_WORLD); 

			if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_Allreduce for GVT failed");
			
			//if (!g_tw_pid) fprintf(stderr, "%u go s\n", g_tw_tid);
			//else fprintf(stderr, "	%u go s\n", g_tw_tid);
		}
		else node_total_white = 0;
		
		mt_allreduce_long_sum_2(g_tw_tid, &node_total_white, &total_white, &sum_barrier[sum_index]);
		sum_index = (sum_index + 1) % 2;
		
		//if (!g_tw_pid) fprintf(stderr, "%u fin\n", g_tw_tid);
		//else fprintf(stderr, "	%u fin\n", g_tw_tid);
		
		if (total_white == 0) break;
	}

    tw_stime lvt = DBL_MAX;
    tw_stime gvt = DBL_MAX;
    tw_stime node_gvt = DBL_MAX;

    // Compute GVT
    if (g_tw_tid) lvt = min_pq_outq(me);

    mt_allreduce_double_min(&lvt, &gvt, &min_barrier[min_index]);
    min_index = (min_index + 1) % 2;

    if (g_tw_tid == 0)
    {
        if (MPI_Allreduce(
            &gvt,
            &node_gvt,
            1,
            MPI_DOUBLE,
            MPI_MIN,
            MPI_COMM_WORLD) != MPI_SUCCESS)
        tw_error(TW_LOC, "MPI_Allreduce for GVT failed");

    }
    else node_gvt = DBL_MAX;

    mt_allreduce_double_min_2(g_tw_tid, &node_gvt, &gvt, &min_barrier[min_index]);
    min_index = (min_index + 1) % 2; 

    me -> s_nwhite_sent = 0;
    me -> s_nwhite_recv = 0;
    me -> trans_msg_ts = DBL_MAX;
    me -> GVT = gvt;
	g_tw_gvt_done++;

    if (me -> id == 0) 
    {   
        fprintf(stderr, "Async ");
        gvt_print(me);
    }

    tw_clock start = tw_clock_read();
	if (g_tw_tid) tw_pe_fossil_collect(me);
    me -> stats.s_fossil_collect += tw_clock_read() - start;

	me -> interval_counter = g_tw_gvt_interval;
    me -> stats.s_gvt += tw_clock_read() - start_total;
}

void
sync_less(tw_pe * me)
{
	//if (!g_tw_tid && !g_tw_pid) fprintf(stderr, "%u come s\n", g_tw_tid);
	//else if (!g_tw_tid) fprintf(stderr, "	%u come s\n", g_tw_tid);
	
	pthread_barrier_wait(&sync_barrier);
	if (!g_tw_tid) MPI_Barrier(MPI_COMM_WORLD);
	pthread_barrier_wait(&sync_barrier);

	//if (!g_tw_tid && !g_tw_pid) fprintf(stderr, "%u go s\n", g_tw_tid);
	//else if (!g_tw_tid)fprintf(stderr, "	%u go s\n", g_tw_tid);
}

void
compute_eff(tw_pe * me)
{
	tw_kp * kp;
	tw_stat total_events_processed = 0; 
	tw_stat rollbacks = 0; 
	tw_stat events_committed = 0; 

	for (int i = 0; i < g_tw_nkp; i++)
        {
               	kp = tw_getkp(i);
                total_events_processed += kp -> s_nevent_processed;
                rollbacks += kp -> s_e_rbs;
	}

	events_committed = total_events_processed - rollbacks;
	double eff = (double) events_committed / (double) total_events_processed; 
	
	if (!total_events_processed)
	{
		//if (!g_tw_pid) fprintf(stderr, "No events processed\n");
		//else fprintf(stderr, "	No events processed\n");
	
		//if (g_tw_pid == 0) fprintf(stderr, "NO EVENTS\n");
		//if (g_tw_pid == 1) fprintf(stderr, "    NO EVENTS\n");

		//node_sync_flag = 1;
		node_sync_flag = 0;
	}
	else 
	{
		//if (!g_tw_pid) fprintf(stderr, "Eff: %f, E_processed: %llu, E_rollbacked: %llu \n", 
		//		       eff, total_events_processed, rollbacks);
		//else fprintf(stderr, "	Eff: %f, E_processed: %llu, E_rollbacked: %llu \n", 
		//		       eff, total_events_processed, rollbacks);
		
		//fprintf(stderr, "%d: Eff: %f, E_processed: %llu, E_rollbacked: %llu\n", 
		//		       g_tw_pid, eff, total_events_processed, rollbacks);

		if (eff < eff_threshold) 
        {
            //if (g_tw_pid == 0) fprintf(stderr, "GVT SYNC MODE\n");
            //if (g_tw_pid == 1) fprintf(stderr, "    GVT SYNC MODE\n");
            node_sync_flag = 1;
        }
		else 
        {  
            //if (g_tw_pid == 0) fprintf(stderr, "GVT NORMAL MODE\n");
            //if (g_tw_pid == 1) fprintf(stderr, "    GVT NORMAL MODE\n");
            node_sync_flag = 0;
        }
	}
}
void
allreduce_barrier_flag(tw_pe * me)
{
	int rv = MPI_Allreduce(
    			&node_sync_flag,
    			&global_sync_flag,
    			1,
    			MPI_INT,
    			MPI_LAND,
    			MPI_COMM_WORLD);
			
	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_Allreduce for GVT failed");
}

void 
mpi_func(_control * in, _control * inout, int * len, MPI_Datatype * dptr)
{
	if (*len != 1) MPI_Abort(MPI_COMM_WORLD, 1);	

	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++)
		{
			inout -> msg_counters[i][j] = inout -> msg_counters[i][j] + in -> msg_counters[i][j];		
		}
	}	
}

void
all_reduce_cm(void * cm)
{
	int rv = MPI_Allreduce(
		&control_message,
		cm,
		1,
		mpi_control_message,
		custom_op,
		MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_ALL_REDUCE Failed");	
}

void
all_reduce_gvt(void * local_gvt)
{
	int rv = MPI_Allreduce(
			local_gvt,
			&global_gvt,
			1,
			MPI_DOUBLE,
			MPI_MIN,
			MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_ALL_REDUCE Failed");	
}

void
update_cm(tw_pe * me)
{
	#ifdef SEPARATE_1
		if (g_tw_tid > 1) update_control(me);
	#elif defined(SEPARATE_0)
		if (g_tw_tid) update_control(me);
	#else
		update_control(me);
	#endif
}

void 
update_control(tw_pe * me)
{
	pthread_mutex_lock(&cm_lock);
	control_message.t_min = min(control_message.t_min, min_pq_outq(me));
	control_message.t_red = min(control_message.t_red, me -> t_red);	
	pthread_mutex_unlock(&cm_lock);
	
	me -> msg_counters[g_tw_tid][g_tw_pid] = 0;
}

void
reset_local_cm_with_other_cm(_control * cm)
{
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++)
		{
			control_message.msg_counters[i][j] = cm -> msg_counters[i][j];
		}
	}
}

void 
accumulate_msg_counters(tw_pe * me)
{
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++)
		{
			if (i != g_tw_tid || j != g_tw_pid)
			{
				__sync_add_and_fetch(&control_message.msg_counters[i][j], me -> msg_counters[i][j]); 
				me -> msg_counters[i][j] = 0;
			}
		}
	}
}

void
reset_control_message()
{
	control_message.t_min = g_tw_ts_end + 1;
	control_message.t_red = g_tw_ts_end + 1;
	for (int i = 0; i < g_tw_n_threads; i++) for (int j = 0; j < g_tw_machines; j++) 
	{
		control_message.msg_counters[i][j] = 0;
	}
}

void 
fossil_collect(tw_pe * me)
{
	me -> GVT = global_gvt - 1;
	
	measure_fc = tw_clock_read();

	#ifdef SL_LOCKS
		//if (!g_tw_pid) fprintf(stderr, "Node %d enter, GVT = %lf\n", g_tw_mynode, gvt);
		//else fprintf(stderr, "	Node %d enter, GVT = %lf\n", g_tw_mynode, gvt);
		if (g_tw_tid == 1) tw_pe_fossil_collect(me);
	#elif defined(SL_BARRIER)
		pthread_barrier_wait(&fossil_barrier);
		//if (g_tw_tid == 1)  tw_pe_fossil_collect(me);
		if (!g_tw_tid)  tw_pe_fossil_collect(me);
		pthread_barrier_wait(&fossil_barrier);
	#elif defined(SEPARATE_1)
		if (g_tw_tid > 1) tw_pe_fossil_collect(me);
	#elif defined(SEPARATE_0)
		if (g_tw_tid) tw_pe_fossil_collect(me);
	#else
		tw_pe_fossil_collect(me);
	#endif
	
	me -> stats.s_fossil_collect += tw_clock_read() - measure_fc;
}

void 
finish_gvt(tw_pe * me)
{
	g_tw_gvt_done++;

	me -> interval_counter = g_tw_gvt_interval;
	me -> trans_msg_ts = DBL_MAX;
	me -> c2_checked = 0;
	me -> color = white;
	me -> stats.s_gvt += tw_clock_read() - measure_gvt;
}


tw_stime
min_pq_outq(tw_pe * me)
{
	// tw_net_read(me); // might be neccessary

	// return the minimum of pq of PE	
	tw_stime pq_min = tw_pq_minimum(me -> pq);

	// return the minimum of outq and posted_sends of PE. Buffers for mpi.
	// populated during network_send, extracted during send_begin
	tw_stime net_min = tw_net_minimum(me);

	tw_stime lvt = me -> trans_msg_ts;
	
	if (lvt > pq_min) lvt = pq_min;
	if (lvt > net_min) lvt = net_min;
	
	if (lvt > g_tw_ts_end) return g_tw_ts_end + 2;
	else return lvt;
}

static const tw_optdef gvt_opts [] =
{
	TWOPT_GROUP("ROSS MPI GVT"),
	TWOPT_UINT("gvt-interval", g_tw_gvt_interval, "GVT Interval"),
	TWOPT_END()
};

const tw_optdef *
tw_gvt_setup(void) 
{
	#ifdef SL_BARRIER
	pthread_barrier_init(&fossil_barrier, NULL, no_threads);
	#endif

	pthread_barrier_init(&sync_barrier, NULL, no_threads);

	return gvt_opts;
}

void
tw_gvt_force_update(tw_pe * me) 
{
	me -> gvt_force++;
	//me -> interval_counter = 0;
}

void tw_gvt_step2(tw_pe * me) {}

void
tw_gvt_stats(FILE * f)
{
	fprintf(f, "\nTW GVT Statistics: Controlled Asynchronous GVT (CA-GVT)\n");
	fprintf(f, "\t%-50s %11d\n", "GVT Interval", g_tw_gvt_interval);
	fprintf(f, "\t%-50s %11d\n", "Batch Size", g_tw_mblock);
	//fprintf(f, "\t%-50s %11d\n", "Forced GVT", gvt_force);
	fprintf(f, "\t%-50s %11d\n", "Total GVT Computations", g_tw_gvt_done);
}
