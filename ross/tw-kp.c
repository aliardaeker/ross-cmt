#include <ross.h>

#define VERIFY_ROLLBACK 0

void
tw_kp_onpe(tw_kpid id, tw_pe * pe)
{
	if (id >= g_tw_nkp) tw_error(TW_LOC, "ID %d exceeded MAX KPs", id);

	if (g_tw_kp[id]) tw_error(TW_LOC, "KP already allocated: %lld\n", id);

	//if (!g_tw_pid) fprintf(stderr, "%d allocated g_tw_kp[%lld]\n", g_tw_tid, id);
	//else fprintf(stderr, "	%d allocated g_tw_kp[%lld]\n", g_tw_tid, id);

	g_tw_kp[id] = tw_calloc(TW_LOC, "Local KP", sizeof(tw_kp), 1);

	g_tw_kp[id] -> id = id;
	g_tw_kp[id] -> pe = pe;
}

void
#ifdef rollback_stats 
tw_kp_rollback_to(tw_kp * kp, tw_stime to,tw_event*test_cev)
#else
tw_kp_rollback_to(tw_kp * kp, tw_stime to)
#endif
{
        tw_event       *e;
        kp->s_rb_total++;

	
	// 02/23/2012
	#ifdef rollback_stats
  	int test=-1;
	if ((test_cev->send_pe/g_tw_n_threads)==(test_cev->recv_pe/g_tw_n_threads))
	{
		kp->s_rb_cause_regional++;
		test=0;
	}
	else
	{
		kp->s_rb_cause_remote++;
		test=1;
	}
	#endif


	#if VERIFY_ROLLBACK
        //printf("%d %d: rb_to %f, now = %f \n", kp->pe->id, kp->id, to, kp->last_time);
	
	if (!kp->pevent_q.size) tw_error(TW_LOC, "Attempting to rollback empty pevent_q 2!");
	#endif

	#ifdef SL_LOCKS
		if (!g_tw_tid) tw_error(TW_LOC, "Thread 0 in tw kp rollback to");		

		pthread_mutex_lock(&kp_locks[g_tw_tid]);
	#endif        

	while (kp->pevent_q.size && kp->pevent_q.head->recv_ts >= to)
        {
		e = tw_eventq_shift(&kp->pevent_q);

                /*
                 * rollback first
                 */
		#ifdef rollback_stats
		if (test == 0) kp->s_regional_rbs++;
		else if (test == 1) kp->s_remote_rbs++;
		#endif

		tw_event_rollback(e);
                /*
                 * reset kp pointers
                 */
                
		if (kp->pevent_q.size == 0) kp->last_time = kp->pe->GVT;
                else kp->last_time = kp->pevent_q.head->recv_ts;

                /*
                 * place event back into priority queue
                 */
                tw_pq_enqueue(kp->pe->pq, e);
        }

	#ifdef SL_LOCKS
		pthread_mutex_unlock(&kp_locks[g_tw_tid]);
	#endif        
}

void
tw_kp_rollback_event(tw_event * event)
{
        tw_event       *e = NULL;
        tw_kp          *kp;
        tw_pe          *pe;

        kp = event->dest_lp->kp;
        pe = kp->pe;

        kp->s_rb_total++;
	kp->s_rb_secondary++;

	//02/23/2012
	#ifdef rollback_stats
	int test=-1;
  	if((event->send_pe/g_tw_n_threads)==(event->recv_pe/g_tw_n_threads))
	{
		kp->s_rb_cause_regional++;
		test=0;
	}
	else
	{
		kp->s_rb_cause_remote++;
		test=1;
	}
	#endif


	#if VERIFY_ROLLBACK
        //printf("%d %d: rb_event: %f \n", pe->id, kp->id, event->recv_ts);

	if (!kp->pevent_q.size) tw_error(TW_LOC, "Attempting to rollback empty pevent_q!");
	#endif


	#ifdef rollback_stats 
	// 02/18/2012
	int rollback_count=0;
	#endif
	//Rollback all the processed events beyond this event
	
	#ifdef SL_LOCKS
		if (!g_tw_tid) tw_error(TW_LOC, "Thread 0 in tw kp rollback event");		
		
		pthread_mutex_lock(&kp_locks[g_tw_tid]);
	#endif
	
	e = tw_eventq_shift(&kp->pevent_q);

	while (e != event)
	{
		kp->last_time = kp->pevent_q.head->recv_ts;
	
		#ifdef rollback_stats
  		if(test==0) kp->s_regional_rbs++;
		else if(test==1) kp->s_remote_rbs++;
		#endif

		tw_event_rollback(e);

		tw_pq_enqueue(pe->pq, e);

		#ifdef rollback_stats 
		// 02/18/2012
   		rollback_count++;
		#endif

	
		e = tw_eventq_shift(&kp->pevent_q);
  	}

	#ifdef rollback_stats 
	// 02/18/2012
	if(rollback_count<10) kp->s_secrbs_small++;
	else if(rollback_count>30) kp->s_secrbs_large++;
	else kp->s_secrbs_middle++;		 
	#endif

	#ifdef rollback_stats
 	if(test==0) kp->s_regional_rbs++;
  	else if(test==1) kp->s_remote_rbs++;
	#endif
      
	tw_event_rollback(e);
        
	if (0 == kp->pevent_q.size) kp->last_time = kp->pe->GVT;
        else kp->last_time = kp->pevent_q.head->recv_ts;
	
	#ifdef SL_LOCKS
		pthread_mutex_unlock(&kp_locks[g_tw_tid]);
	#endif
}

void
tw_init_kps(tw_pe * me)
{
	#pragma GCC diagnostic push
	#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
	tw_kp *prev_kp = NULL;
	#pragma GCC diagnostic pop	

	tw_kpid i;

	for (i = 0; i < g_tw_nkp; i++)
	{
		tw_kp *kp = tw_getkp(i);

		if (kp->pe != me)
			continue;

		kp->id = i;
		kp->s_e_rbs = 0;

#ifdef rollback_stats
    kp->s_remote_rbs=0;
		kp->s_regional_rbs=0;
// 02/18/2012
		kp->s_secrbs_small=0;
		kp->s_secrbs_middle=0;
		kp->s_secrbs_large=0;
//02/23/2012    
		kp->s_rb_cause_regional=0;
    kp->s_rb_cause_remote=0; 
#endif


		kp->s_rb_total = 0;
		kp->s_rb_secondary = 0;
		prev_kp = kp;

#if ROSS_MEMORY
		kp->pmemory_q = tw_calloc(TW_LOC, "KP memory queues",
					sizeof(tw_memoryq), g_tw_memory_nqueues);
#endif
	}
}

void
tw_kp_fossil_memoryq(tw_kp * kp, tw_fd fd)
{
	#if ROSS_MEMORY
	tw_memoryq	*q;

	tw_memory      *b;
	tw_memory      *tail;

	tw_memory      *last;

	tw_stime	gvt = kp->pe->GVT;

	int             cnt;

	q = &kp->pmemory_q[fd];
	tail = q->tail;

	if(0 == q->size || tail->ts >= gvt)
		return;

	if(q->head->ts < gvt)
	{
		tw_memoryq_push_list(tw_pe_getqueue(kp->pe, fd), 
				     q->head, q->tail, q->size);

		q->head = q->tail = NULL;
		q->size = 0;

		return;
	}

	/*
	 * Start direct search.
	 */
	last = NULL;
	cnt = 0;

	b = q->head;
	while (b->ts >= gvt)
	{
		last = b;
		cnt++;

		b = b->next;
	}

	tw_memoryq_push_list(tw_pe_getqueue(kp->pe, fd), b, q->tail, q->size - cnt);

	/* fix what remains of our pmemory_q */
	q->tail = last;
	q->tail->next = NULL;
	q->size = cnt;

	#if VERIFY_PE_FC_MEM
	printf("%d: FC %d buf from FD %d \n", kp->id, cnt, fd);
	#endif
	#endif
}

void
tw_kp_fossil_memory(tw_kp * kp)
{
	int	 i;

	for(i = 0; i < g_tw_memory_nqueues; i++)
		tw_kp_fossil_memoryq(kp, i);
}
