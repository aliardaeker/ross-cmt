#include <ross.h>
#include <mpi.h>

//DJ:
#define MAX_NODES 256

struct act_q
{
	const char * name;

  	tw_event	** event_list;
  	MPI_Request	 * req_list;
  	int		 * idx_list;
	
	#ifdef OptMess  
		int		 *count_list;
	#endif  
	
	MPI_Status	 *status_list;

	#ifdef Message_Aggregate
  		char ** buffers;
	#endif

	#if ROSS_MEMORY
  		char ** buffers;
	#endif

  	unsigned int cur;
};

#define EVENT_TAG 1

#if ROSS_MEMORY
	#define BUFFER_SIZE 500
	#define EVENT_SIZE(e) BUFFER_SIZE
#else
	#define EVENT_SIZE(e) (g_tw_event_msg_sz)
#endif

__thread int mycount=0;
static __thread struct act_q posted_sends;
static __thread struct act_q posted_recvs;

#ifdef prob_opt 
	#ifdef dynamic_polling
		static __thread tw_stat  polling_frequency=4;
		static __thread tw_stat  poll_count=4;
		//static __thread tw_stat  poll_count=2;
	#else
		static __thread tw_stat  poll_count=poll_period;
	#endif
#endif

#ifdef Message_Aggregate
	static tw_eventq outq[Thread_Number][Machines];
	static pthread_mutex_t outq_mutex[Thread_Number][Machines];
	static __thread int queue_index=0;
	//print message
	static __thread tw_stat  tmp_message=0;
	static __thread tw_stat tmp_event=0;
	static __thread tw_stat tmp_message_aggre=0;
#else
	// For MPI Send
	static tw_eventq outq[Thread_Number];
	static pthread_mutex_t outq_mutex[Thread_Number];
#endif

static __thread int current_token = -1;

// For mt_receive
static __thread tw_eventq inq[MAX_NUMA_NODES]; // MAX_NUMA_NODES is same with MAX_NODES
					       // MAX_NUMA_NODES is 8, possibly the number of machines
typedef struct 
{
        tw_eventq * outq_ptr; // Pointer to thread specific output queue, not output
			      // This points to the thread specific inq location so that when this is
			      // written, inq is also updated.
        pthread_mutex_t mutex;
} g_mt_outq;

// For mt_send
static g_mt_outq mt_outq_ptr[MAX_NODES][MAX_NUMA_NODES];

static unsigned int read_buffer = 10;
static unsigned int send_buffer = 10;

static __thread int world_size = 1;
static __thread int numa_node_id;

static const tw_optdef mpi_opts[] = 
{
	TWOPT_GROUP("ROSS MPI Kernel"),
	TWOPT_UINT(
	     "read-buffer",
	     read_buffer,
	     "network read buffer size in # of events"),
  	TWOPT_UINT(
	     "send-buffer",
	     send_buffer,
	     "network send buffer size in # of events"),
	TWOPT_END()
};

void init_mt_ouq_ptr(int thread_id)
{
	int i, j;
	i = j = 0;
	
	for(i = 0; i < MAX_NUMA_NODES; i++)
	{
		inq[i].size = 0;
		inq[i].head = inq[i].tail = NULL;
		pthread_mutex_init(&(mt_outq_ptr[thread_id][i].mutex), NULL);
		
		mt_outq_ptr[thread_id][i].outq_ptr = &inq[i];
	}
	
	// jwang
	#ifdef Message_Aggregate
 		for(i = 0; i < g_tw_n_threads; i++) for(j=0;j<g_tw_machines;j++) pthread_mutex_init(&outq_mutex[i][j], NULL);
	#else
		// Outq mutex is initialized by every thread, BUG
 		for(i = 0; i < g_tw_n_threads; i++) pthread_mutex_init(&outq_mutex[i], NULL);	
	#endif
}

void destroy_mt_ouq_ptr()
{
	int i;
	
	for(i = 0; i < MAX_NUMA_NODES; i++) pthread_mutex_destroy(&(mt_outq_ptr[g_tw_tid][i].mutex));
	
        // mt_outq_ptr[thread_id].outq_ptr = &outq;
	#ifdef Message_Aggregate
		int j;
		for(i = 0; i < g_tw_n_threads; i++) for(j = 0; j < g_tw_machines; j++) pthread_mutex_destroy(&outq_mutex[i][j]);
	#else
		for(i = 0; i < g_tw_n_threads; i++) pthread_mutex_destroy(&outq_mutex[i]);
	#endif
}

//DJ:
void mt_mpi_init(int *argc, char ***argv)
{
	int provided;

  	if (MPI_Init_thread(argc, argv, MPI_THREAD_FUNNELED, &provided) != MPI_SUCCESS) tw_error(TW_LOC, "MPI_Init failed.");
  	//if (MPI_Init_thread(argc, argv, MPI_THREAD_SERIALIZED, &provided) != MPI_SUCCESS) tw_error(TW_LOC, "MPI_Init failed.");
  	//if (MPI_Init_thread(argc, argv, MPI_THREAD_MULTIPLE, &provided) != MPI_SUCCESS) tw_error(TW_LOC, "MPI_Init failed.");
}

const tw_optdef *
tw_net_init(int *argc, char ***argv,int thread_id)
{
	int my_rank;

	if (MPI_Comm_rank(MPI_COMM_WORLD, &my_rank) != MPI_SUCCESS) tw_error(TW_LOC, "Cannot get MPI_Comm_rank(MPI_COMM_WORLD)");

 	g_tw_masternode = 0;
  	g_tw_mynode = (my_rank * g_tw_n_threads) + thread_id;
 
 	g_tw_pid = my_rank; // 2 Computers: 0, 1
	numa_node_id = g_tw_pid;
  	
	g_tw_tid = thread_id; // 4 Threads in each node: 0, 1, 2, 3
  	//printf("Thread_id = %d, Numa_node_id = %d\n", thread_id, numa_node_id);

	init_mt_ouq_ptr(thread_id);
  	return mpi_opts;
}

static void
init_q(struct act_q *q, const char *name)
{
	unsigned int n;

	#ifdef Message_Aggregate
  		unsigned int i;
	#endif

	#if ROSS_MEMORY
  		unsigned int i;
	#endif
	
	if (q == &posted_sends) n = send_buffer;
  	else n = read_buffer;

  	q -> name = name;
  	q -> event_list = tw_calloc(TW_LOC, name, sizeof(*q->event_list), n);
  	q -> req_list = tw_calloc(TW_LOC, name, sizeof(*q->req_list), n);
  	q -> idx_list = tw_calloc(TW_LOC, name, sizeof(*q->idx_list), n);

	#ifdef OptMess  
  		q -> count_list = tw_calloc(TW_LOC, name, sizeof(*q->count_list), n);
	#endif 

	q -> status_list = tw_calloc(TW_LOC, name, sizeof(*q->status_list), n);

	#ifdef Message_Aggregate
 		q -> buffers = tw_calloc(TW_LOC, name, sizeof(*q->buffers), n); 
		for (i = 0; i < n; i++) q -> buffers[i] = tw_calloc(TW_LOC, "", buffer_size, 1);
	#endif

	#if ROSS_MEMORY
  		q -> buffers = tw_calloc(TW_LOC, name, sizeof(*q->buffers), n);
  		for (i = 0; i < n; i++) q->buffers[i] = tw_calloc(TW_LOC, "", BUFFER_SIZE, 1);
	#endif
}

unsigned int
tw_nnodes(void)
{
	return world_size;
}

void
tw_net_start(void)
{
  	if (MPI_Comm_size(MPI_COMM_WORLD, &world_size) != MPI_SUCCESS) tw_error(TW_LOC, "Cannot get MPI_Comm_size(MPI_COMM_WORLD)");
    
	// DJ
	world_size = world_size * no_threads; // world_size is total number of threads in global
  	if (g_tw_mynode == 0) printf("\nFound world size to be %d \n", world_size);

  	tw_pe_create(1);
  	tw_pe_init(0, g_tw_mynode);
  	g_tw_pe[0] -> hash_t = tw_hash_create();

  	if (send_buffer < 1) tw_error(TW_LOC, "network send buffer must be >= 1");
  	if (read_buffer < 1) tw_error(TW_LOC, "network read buffer must be >= 1");

  	init_q(&posted_sends, "MPI send queue");
  	init_q(&posted_recvs, "MPI recv queue");
  	g_tw_net_device_size = read_buffer;
}

void
tw_net_abort(void)
{
	MPI_Abort(MPI_COMM_WORLD, 1);
	exit(1);
}

void
tw_net_stop(void)
{
	destroy_mt_ouq_ptr();
}

void
tw_net_barrier(tw_pe * pe)
{
	barrier_sync(&g_barrier,g_tw_tid);

	//jwang
	if(g_tw_tid == 0)
	{
  		if (MPI_Barrier(MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Failed to wait for MPI_Barrier");
		//arg1: barrier struct pointer arg2: thread_id
	}

	barrier_sync(&g_barrier,g_tw_tid);
}

tw_stime
tw_net_minimum(tw_pe *me)
{
	tw_stime m = DBL_MAX;
	tw_event *e;
	int i;

	//jwang
	#ifdef Message_Aggregate
  		int j;
		for(j = 0; j < g_tw_machines; j++)
		{
			pthread_mutex_lock(&outq_mutex[g_tw_tid][j]);
  			e = outq[g_tw_tid][j].head;
			pthread_mutex_unlock(&outq_mutex[g_tw_tid][j]);
			// e = outq.head;

  			while (e) 
			{
    				if (m > e -> recv_ts) m = e->recv_ts;
    				e = e->next;
  			}
		}
	#else
  		pthread_mutex_lock(&outq_mutex[g_tw_tid]);
  		e = outq[g_tw_tid].head;
		pthread_mutex_unlock(&outq_mutex[g_tw_tid]);
		// e = outq.head;

  		while (e) 
		{
    			if (m > e -> recv_ts) m = e->recv_ts;
    			e = e -> next;
  		}
	#endif

	for (i = 0; i < posted_sends.cur; i++) 
	{
    		e = posted_sends.event_list[i];
    		if (m > e -> recv_ts) m = e->recv_ts;
  	}
	
  	return m;
}

#ifdef Message_Aggregate
static int
test_q(
       struct act_q *q,
       tw_pe *me,
       void (*finish)(tw_pe *, tw_event *, char *))
{
  	int ready, i, n;

	#if ROSS_MEMORY
  		char *tmp;
	#endif

	char*tmp;
	#ifdef OptMess
		int tmp_int;  	
	#endif 

  	if (!q->cur) return 0;
  	if (MPI_Testsome(
		   q->cur,
		   q->req_list,
		   &ready,
		   q->idx_list,
		   q->status_list) != MPI_SUCCESS) {
   
	 tw_error(
	     TW_LOC,
	     "MPI_testsome failed with %u items in %s",
	     q->cur,
	     q->name);
  	}

  	if (1 > ready) return 0;
  	for (i = 0; i < ready; i++)
    	{
      		tw_event *e;
      		tw_event*e_group[event_Group]; 
      
      		n = q->idx_list[i];	
		e = q->event_list[n];
		q->event_list[n] = NULL;
     
		int count=0;
		
		if(q == &posted_recvs)
      		{
			char*buffer=q->buffers[n];
			#ifdef OptMess
				count = q->count_list[n]/g_tw_event_msg_sz;
			#else
				memcpy(&count,&buffer[0],4);
			#endif
			
			int kk, recv_position;

					
			#ifdef OptMess
				for(kk=0,recv_position=0;kk<count;kk++,recv_position+=g_tw_event_msg_sz)
			#else
				for(kk=0,recv_position=4;kk<count;kk++,recv_position+=g_tw_event_msg_sz)
			#endif		
				{
				#ifdef MemoryPool
					e_group[kk]=tw_event_grab(me);							
				#else
					e_group[kk]=malloc(g_tw_event_msg_sz);
					e_group[kk]->cancel_next = NULL;
					e_group[kk]->caused_by_me = NULL;
					e_group[kk]->cause_next = NULL;
			    		e_group[kk]->prev = e_group[kk]->next = NULL;
					memset(&e_group[kk]->state, 0, sizeof(e_group[kk]->state));
					memset(&e_group[kk]->event_id, 0, sizeof(e_group[kk]->event_id));
				#endif
					memcpy(e_group[kk],&buffer[recv_position],g_tw_event_msg_sz);				
				}
		}
      		else
		{
			while (e != NULL)
			{
				e_group[count]=e;
				count++;
				e=e->next;
			}
		}

		int index=0;
		
		for(index=0;index<count;index++)
		{
			#if ROSS_MEMORY
      				finish(me, e_group[index], q->buffers[n]);
			#else
      				finish(me, e_group[index], NULL);
			#endif
		}
	}

  
	/* Collapse the lists to remove any holes we left. */
  	for (i = 0, n = 0; i < q->cur; i++) 
	{
    		if (q->event_list[i]) 
		{
      			if (i != n) 
			{
				// swap the event pointers
				q->event_list[n] = q->event_list[i];

				// copy the request handles
				memcpy(
	       			   &q->req_list[n],
	       			   &q->req_list[i],
	       			   sizeof(q->req_list[0]));

				#ifdef  Message_Aggregate
					// swap the buffers
					tmp = q->buffers[n];
					q->buffers[n] = q->buffers[i];
					q->buffers[i] = tmp;
				#endif

				#ifdef OptMess
 					tmp_int=q->count_list[n];
 					q->count_list[n]=q->count_list[i];
 					q->count_list[i]=tmp_int;
				#endif

				#if ROSS_MEMORY
					// swap the buffers
					tmp = q->buffers[n];
					q->buffers[n] = q->buffers[i];
					q->buffers[i] = tmp;
				#endif
      			}
      
			n++;
    		}
  	}
 
	q->cur -= ready;
  	return 1;
}
#else
static int
test_q(
       struct act_q * q, // thread spesific posted_sends / posted_receives
       tw_pe *me,
       void (*finish)(tw_pe *, tw_event *, char *)) // net_send_finish / net_receive_finish
{
	int ready, i, n;

	#ifdef blocking_MPI
		ready = 1;
	#endif
	
	#if ROSS_MEMORY
  		char *tmp;
	#endif
  
	// Posted receives is empty
	if (!q -> cur) return 0;
	
	// Tests for given requests to complete
	// q -> cur is length of requests, ready is number of completed requests
	
	#ifndef blocking_MPI	
	if (MPI_Testsome(
		   q->cur,
		   q->req_list,
		   &ready,
		   q->idx_list,
		   q->status_list) != MPI_SUCCESS) tw_error(TW_LOC, "MPI_testsome failed with %u items in %s", q->cur, q->name);
	#endif

	// Nothing is ready
  	if (1 > ready) return 0;

  	for (i = 0; i < ready; i++)
    	{
      		tw_event *e;
      		n = q -> idx_list[i];	
		e = q -> event_list[n];
      		q -> event_list[n] = NULL;

		#if ROSS_MEMORY
      			finish(me, e, q -> buffers[n]);
		#else
      			finish(me, e, NULL);
		#endif
    	}

  	/* Collapse the lists to remove any holes we left. */
  	for (i = 0, n = 0; i < q -> cur; i++) 
	{
    		if (q->event_list[i]) 
		{
      			if (i != n) 
			{
				// swap the event pointers
				q->event_list[n] = q->event_list[i];
				// copy the request handles
				memcpy(&q->req_list[n], &q->req_list[i], sizeof(q->req_list[0]));

				#if ROSS_MEMORY
					// swap the buffers
					tmp = q->buffers[n];
					q->buffers[n] = q->buffers[i];
					q->buffers[i] = tmp;
				#endif
      			}
      			n++;
    		}
  	}

  	q->cur -= ready;
 	return 1;
}
#endif

#ifdef Message_Aggregate
static int
recv_begin(tw_pe *me)
{
	// jwang
  	int changed = 0;
  	int flag=0;
	
	while (posted_recvs.cur < read_buffer)
    	{
		unsigned id = posted_recvs.cur;
      		MPI_Status status;
      		tw_event*e;
 
      		MPI_Iprobe(MPI_ANY_SOURCE,
			   EVENT_TAG,
		           MPI_COMM_WORLD,
		           &flag,
		           &status);
		
		//poll messages
		#ifdef prob_stats
			total_poll++;
		#endif 
  		
		if(flag)
		{
			#ifdef dynamic_polling
			if(polling_frequency>0)
			{
	 			//printf("poll_count is %ld\n",polling_frequency);
				polling_frequency /= 2;
			}
			#endif

			#ifdef prob_stats
			//poll messages
			suc_poll++;
			#endif

			#ifdef OptMess
   			MPI_Get_count(&status,MPI_BYTE,&posted_recvs.count_list[id]);    
			#endif

			#ifdef MemoryPool
			e = tw_event_grab(me); 
			#else
			e = malloc(EVENT_SIZE(e)); 
			#endif	
		}
		else
		{
			#ifdef dynamic_polling
			// poll_count++;
			polling_frequency += 2;
			#endif
	  		return changed;
		}

		#ifdef OptMess
		if(!flag || MPI_Irecv(
		 		posted_recvs.buffers[id],
				posted_recvs.count_list[id],
				MPI_BYTE,
				MPI_ANY_SOURCE,
		      		EVENT_TAG,
		     		MPI_COMM_WORLD,
		     		&posted_recvs.req_list[id]) != MPI_SUCCESS) return changed;
		#else
		
		if(!flag || MPI_Irecv(
		 		posted_recvs.buffers[id],
				buffer_size,	
				MPI_BYTE,
				MPI_ANY_SOURCE,
		      		EVENT_TAG,
		     		MPI_COMM_WORLD,
		     		&posted_recvs.req_list[id]) != MPI_SUCCESS) 
		{
			fprintf(stderr, "MPI_Irecv returns fail\n");	
			return changed;
		}		

		#endif
		posted_recvs.event_list[id]=e;
		posted_recvs.cur++;
		changed=1;
	}
  
	return changed;
}

#else
static int
recv_begin(tw_pe *me)
{
	MPI_Status status;

	// jwang
	// tw_event e ;
  	int flag = 0;
  	int changed = 0;
  
  	while (posted_recvs.cur < read_buffer)
  	{
      		unsigned id = posted_recvs.cur;
  		tw_event	*e = NULL;

      		MPI_Iprobe(MPI_ANY_SOURCE,
			EVENT_TAG,
		 	MPI_COMM_WORLD,
		 	&flag,
		 	&status);
  
		// poll messages
		#ifdef prob_stats
			total_poll++;
		#endif

		if (flag)
		{
			// jwang
			#ifdef prob_stats
				//poll messages
				suc_poll++;
			#endif
			
			#ifdef MemoryPool
				e = tw_event_grab(me); 
			#else
				e = malloc(EVENT_SIZE(e));
			#endif
				e -> cancel_next = NULL;
				e -> caused_by_me = NULL;
				e -> cause_next = NULL;
				e -> prev = e->next = NULL;
				memset(&e->state, 0, sizeof(e->state));
				memset(&e->event_id, 0, sizeof(e->event_id));
		} 
		else return changed;

	#if ROSS_MEMORY
      		if(MPI_Irecv(posted_recvs.buffers[id],
		   	EVENT_SIZE(e),
		   	MPI_BYTE,
		   	MPI_ANY_SOURCE,
		   	EVENT_TAG,
		   	MPI_COMM_WORLD,
		   	&posted_recvs.req_list[id]) != MPI_SUCCESS) return changed;
	#else
		
		/*
 		*
 		*
 		* 	Receiving remote
 		*
 		*
 		*/
		#ifdef blocking_MPI
		int rv = MPI_Recv(e,
		     		EVENT_SIZE(e),
		     		MPI_BYTE,
		     		MPI_ANY_SOURCE,
		     		EVENT_TAG,
		     		MPI_COMM_WORLD,
				MPI_STATUS_IGNORE);		

		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "Error: MPI Recv");				
		#else		
		int rv = MPI_Irecv(e,
		     	EVENT_SIZE(e),
		     	MPI_BYTE,
		     	MPI_ANY_SOURCE,
		     	EVENT_TAG,
		     	MPI_COMM_WORLD,
		     	&posted_recvs.req_list[id]);
	
		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "Error: MPI Recv");				
		#endif	
	#endif

		posted_recvs.event_list[id] = e;
      		posted_recvs.cur++;
      		changed = 1;
    	}
   
	return changed;
}
#endif

void
recv_finish(tw_pe *me, tw_event *e, char * buffer)
{
	tw_pe		*dest_pe;

	#if ROSS_MEMORY
  		tw_memory	*memory;
  		tw_memory	*last;

  		tw_fd		 mem_fd;
  		size_t		 mem_size;
  		unsigned	 position = 0;

  		memcpy(e, buffer, g_tw_event_msg_sz);
  		position += g_tw_event_msg_sz;
	#endif
	
	#ifdef ROSS_GVT_mpi_allreduce	
		me -> s_nwhite_recv++;	

		#ifdef DEBUG_ALI
			if (!g_tw_pid)
			{
				fprintf(stderr, "%u receives from %lld: %lld\n", 
					g_tw_tid, e -> send_pe, me -> s_nwhite_recv);
					// e -> event_id, e -> recv_ts);
			}
			else
			{
				fprintf(stderr, "	%u receives from %lld: %lld\n", 
					g_tw_tid, e -> send_pe, me -> s_nwhite_recv); 
					// e -> event_id, e -> recv_ts);
			}
		#endif
	#endif

	e -> dest_lp = tw_getlocal_lp((tw_lpid) e->dest_lp);
	dest_pe = e -> dest_lp -> pe;

  	if (e -> send_pe > tw_nnodes() - 1) tw_error(TW_LOC, "bad sendpe_id: %d", e->send_pe);

  	e -> cancel_next = NULL;
  	e -> caused_by_me = NULL;
  	e -> cause_next = NULL;

  	if (e -> recv_ts < me -> GVT) 
	{
		#ifdef ROSS_GVT_mattern_orig
			tw_error(TW_LOC, 
			"\n\n%d received straggler from %d\nRecv_ts: %lf (%d), gvt: (%f)\nE id: %d, E color: %d, me color: %d\n", 
			me -> id,  e -> send_pe, e -> recv_ts, e -> state.cancel_q, 
			me -> GVT, e -> event_id, e -> color, me -> color);
		#else	
			tw_error(TW_LOC, 
			"\n\n%d received straggler from %d\nRecv_ts: %lf (%d), gvt: (%f)\nE id: %d\n", 
			me -> id,  e -> send_pe, e -> recv_ts, e -> state.cancel_q, 
			me -> GVT, e -> event_id);
		#endif
	}

  	if (tw_gvt_inprogress(me)) me->trans_msg_ts = min(me->trans_msg_ts, e->recv_ts);

	// if cancel event, retrieve and flush
  	// else, store in hash table
  	if (e -> state.cancel_q)
    	{
		//DJ: find an event in the hash table for which this cancel event is made 
		tw_event *cancel = tw_hash_remove(me->hash_t, e, e->send_pe);

      		// NOTE: it is possible to cancel the event we
      		// are currently processing at this PE since this
      		// MPI module lets me read cancel events during 
      		// event sends over the network.

      		cancel->state.cancel_q = 1;
      		cancel->state.remote = 0;
		//Put orig event in the cancelq
      		cancel->cancel_next = dest_pe->cancel_q;
      		dest_pe->cancel_q = cancel;

		//DJ:cancel event done it's job now we can free it 
		tw_event_free(me, e);
      
      		return;
    	}

	tw_hash_insert(me->hash_t, e, e->send_pe);
  	e -> state.remote = 1;

	#if ROSS_MEMORY
  		mem_size = (size_t) e->memory;
  		mem_fd = (tw_fd) e->prev;

  		last = NULL;
  		while(mem_size)
    		{
      			memory = tw_memory_alloc(e->dest_lp, mem_fd);

      			if (last) last->next = memory;
      			else e->memory = memory;

      			memcpy(memory, &buffer[position], mem_size);
      			position += mem_size;

      			memory->fd = mem_fd;
      			memory->nrefs = 1;

      			mem_size = (size_t) memory->next;
      			mem_fd = memory->fd;

      			last = memory;
    		}
	#endif

  	if(me == dest_pe && e->dest_lp->kp->last_time <= e->recv_ts) 
	{
    		/* Fast case, we are sending to our own PE and
     		* there is no rollback caused by this send.
     		*/
    		tw_pq_enqueue(dest_pe->pq, e);
    		return;
  	}
  
  	if(tw_node_eq(&me->node, &dest_pe->node)) 
	{
    		/* Slower, but still local send, so put into top
     		* of dest_pe->event_q. 
     		*/
    		e->state.owner = TW_pe_event_q;
    		tw_eventq_push(&dest_pe->event_q, e);
    		return;
  	}

  	/* Never should happen; MPI should have gotten the
  	 * message to the correct node without needing us
   	 * to redirect the message there for it.  This is
   	 * probably a serious bug with the event headers
   	 * not being formatted right.
   	 */
  	
	tw_error(
	   TW_LOC,
	   "Event recived by PE %u but meant for PE %u",
	   me->id,
	   dest_pe->id);
}

#ifdef Message_Aggregate
static int
send_begin(tw_pe *me)
{
	int changed = 0;
	// int count=0;
	int kk;
  	
	while (posted_sends.cur < send_buffer)
    	{
		int send_position=0;
  		int count=0;

		tw_event * e[event_Group];
		//jwang

		// Defined nowhere
		#ifdef dynamic_Message_Aggregate 
	 	int thread_index=0;
	 	for(thread_index=current_token;thread_index<g_tw_n_threads;thread_index++)
		{
			pthread_mutex_lock(&outq_mutex[thread_index][queue_index]);
	   		while(count<event_Group)
			{
				e[count]= tw_eventq_pop(&outq[thread_index][queue_index]);
				if(!e[count]) break;    
				
				count++;
			}  

			pthread_mutex_unlock(&outq_mutex[thread_index][queue_index]);
			if(count==event_Group) break;
		}
		#else
		pthread_mutex_lock(&outq_mutex[current_token][queue_index]);
    		for (count = 0; count < event_Group; count++)
		{
			e[count] = tw_eventq_pop(&outq[current_token][queue_index]);
			if (!e[count]) break;
		}
		pthread_mutex_unlock(&outq_mutex[current_token][queue_index]);
		#endif

		if (count == 0) break; 

		//printf message
   		if (count > 1) tmp_message_aggre++;

		tmp_message++;
		tmp_event += count;
	 
		 
		unsigned id = posted_sends.cur;
   		char *buffer = NULL;
		buffer=posted_sends.buffers[id];

		#ifdef OptMess
			for(kk=0, send_position=0; kk<count; kk++, send_position += g_tw_event_msg_sz) 
				memcpy(&buffer[send_position],e[kk],g_tw_event_msg_sz);		

			if (MPI_Isend(buffer,
			     count*g_tw_event_msg_sz,
			     MPI_BYTE,
                	     (e[0] -> recv_pe) / g_tw_n_threads,
                	     EVENT_TAG,
                	     MPI_COMM_WORLD,
               		     &posted_sends.req_list[id]) != MPI_SUCCESS) return changed;
		#else
			memcpy(&buffer[0],&count,4);
			for(kk=0, send_position = 4; kk < count; kk++, send_position += g_tw_event_msg_sz)
       			memcpy(&buffer[send_position],e[kk],g_tw_event_msg_sz);
		
			if (MPI_Isend(buffer,
		      		buffer_size,
		      		MPI_BYTE,
                      		(e[0] -> recv_pe) / g_tw_n_threads, // dest -> 0 or 1 (For 2 machines, each has 4 threads)
                      		EVENT_TAG,
                      		MPI_COMM_WORLD,
                      		&posted_sends.req_list[id]) != MPI_SUCCESS) 
			{
				fprintf(stderr, "MPI_Isend returns fail\n");	
				return changed;
			}
		#endif
 
		tw_event*head=NULL;
		for (kk = 0; kk < count; kk++)
		{	  
			if (kk == 0)
			{					  
				posted_sends.event_list[id] = e[kk];
				head = e[kk];
      				posted_sends.cur++;
			}
			else
			{
				head->next=e[kk];
				head=e[kk];
			}
      		
			me->s_nwhite_sent++;
		}

		head->next=NULL; 		
		changed = 1;
	}
  
  	return changed;
}

#else
static int
send_begin(tw_pe *me)
{
	int changed = 0;

	while (posted_sends.cur < send_buffer)
   	{
      
	//jwang
	#ifdef Message_Aggregate
		pthread_mutex_lock(&outq_mutex[current_token][queue_index]);
    		tw_event *e = tw_eventq_peek(&outq[current_token][queue_index]);
		printf("start : send pe  is %d,id is %d\n",e->send_pe, e->event_id);
	  	pthread_mutex_unlock(&outq_mutex[current_token][queue_index]);
 		tw_event *e = tw_eventq_peek(&outq);
	#else
		pthread_mutex_lock(&outq_mutex[current_token]);
      		tw_event *e = tw_eventq_peek(&outq[current_token]);
		pthread_mutex_unlock(&outq_mutex[current_token]);
	#endif

		if (!e) break;

		//printf message 
   		me->stats.s_message_number++,
		me->stats.s_event_number++;

      		unsigned id = posted_sends.cur;
	
	#if ROSS_MEMORY
		tw_event *tmp_prev = NULL;
      		tw_lp *tmp_lp = NULL;
      		tw_memory *memory = NULL;
      		tw_memory *m = NULL;
      		char *buffer = NULL;
      		size_t mem_size = 0;
      		unsigned position = 0;
	#endif

	#if ROSS_MEMORY
      		// pack pointers
      		tmp_prev = e->prev;
      		tmp_lp = e->src_lp;
      		// delete when working
      		e->src_lp = NULL;

     	 	memory = NULL;
      		if(e->memory)
		{
	  		memory = e->memory;
	  		e->memory = (tw_memory *) tw_memory_getsize(me, memory->fd);
	  		e->prev = (tw_event *) memory->fd;
	  		mem_size = (size_t) e->memory;
		}

      		buffer = posted_sends.buffers[id];
      		memcpy(&buffer[position], e, g_tw_event_msg_sz);
      		position += g_tw_event_msg_sz;

      		// restore pointers
      		e->prev = tmp_prev;
      		e->src_lp = tmp_lp;

      		m = NULL;
      		while(memory)
		{
	 		m = memory->next;

	  		if(m)
	    		{
	      			memory->next = (tw_memory *)
				tw_memory_getsize(me, m->fd);
	      			memory->fd = m->fd;
	    		}

	  		if(position + mem_size > BUFFER_SIZE) tw_error(TW_LOC, "Out of buffer space!");

	  		memcpy(&buffer[position], memory, mem_size);
	  		position += mem_size;

	  		memory->nrefs--;
	  		tw_memory_unshift(e->src_lp, memory, memory->fd);

	  		if(NULL != (memory = m)) mem_size = tw_memory_getsize(me, memory->fd);
		}

      		e->memory = NULL;

      		if (MPI_Isend(buffer,
		    	EVENT_SIZE(e),
		    	MPI_BYTE,
		    	(e -> recv_pe) / g_tw_n_threads, //*dest_node,
		    	EVENT_TAG,
		    	MPI_COMM_WORLD,
		    	&posted_sends.req_list[id]) != MPI_SUCCESS) return changed;
	#else
		/*
 		*
 		*	Sending remote
 		*
 		*	Important: although it looks like sender is e -> send_pe, 
 		*	actual sender is g_tw_tid == 0. This is not important at the
 		*	receiving side since we just count regional receives. Because 
 		*	every remote receive will execute regional receive as well but
 		*	sends are either regional or remote.
 		*/

		#ifdef ROSS_GVT_mattern_orig
		int _thread_id = (e -> recv_pe) % g_tw_n_threads;
		int _machine_id = (e -> recv_pe) / g_tw_n_threads;

		if (me -> color == white) 
		//if (e -> src_lp_ptr -> pe -> color == white)
		{
			e -> color = white;
			me -> msg_counters[_thread_id][_machine_id]++;			
			//e -> src_lp_ptr -> pe -> msg_counters[_thread_id][_machine_id]++;			

			#ifdef DEBUG_ALI
			if (!g_tw_pid)
			{
				fprintf(stderr, "%lld sends remote white to %d: %d\n", 
					e -> send_pe, e -> recv_pe, me -> msg_counters[_thread_id][_machine_id]);
					// e -> event_id, e -> recv_ts);
			}
			else
			{
				fprintf(stderr, "	%lld sends remote white to %d: %d\n", 
					e -> send_pe, e -> recv_pe, me -> msg_counters[_thread_id][_machine_id]);
					// e -> event_id, e -> recv_ts);
			}
			#endif

		}
		else if (me -> color == red)
		//if (e -> src_lp_ptr -> pe -> color == red)
		{
			e -> color = red;
			me -> t_red = min(me -> t_red, e -> recv_ts);
			//e -> src_lp_ptr -> pe -> t_red = min(me -> t_red, e -> recv_ts);
			
			#ifdef DEBUG_ALI
			if (!g_tw_pid)
			{
				fprintf(stderr, "%lld sends remote red to %d: %d\n", 
					e -> send_pe, e -> recv_pe, me -> msg_counters[_thread_id][_machine_id]);
					// e -> event_id, e -> recv_ts);
			}
			else
			{
				fprintf(stderr, "	%lld sends remote red to %d: %d\n", 
					e -> send_pe, e -> recv_pe, me -> msg_counters[_thread_id][_machine_id]);
					// e -> event_id, e -> recv_ts);
			}
			#endif
		}
		else tw_error(TW_LOC, "Error in mattern orig send"); 	
		#endif

		#ifdef blocking_MPI	
		int rv = MPI_Send(e,
                    		EVENT_SIZE(e),
                    		MPI_BYTE,
                    		(e -> recv_pe) / g_tw_n_threads,
                    		EVENT_TAG,
                    		MPI_COMM_WORLD);
		
		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "Error in MPI_Send"); 	
		#else
		int rv = MPI_Isend(e,
                    	EVENT_SIZE(e),
                    	MPI_BYTE,
                    	(e -> recv_pe) / g_tw_n_threads,
                    	EVENT_TAG,
                    	MPI_COMM_WORLD,
                    	&posted_sends.req_list[id]);

		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "Error in MPI_Isend");
		#endif
	
	#endif

	#ifdef Message_Aggregate
 		pthread_mutex_lock(&outq_mutex[current_token][queue_index]);
     		tw_eventq_pop(&outq[current_token][queue_index]);
		pthread_mutex_unlock(&outq_mutex[current_token][queue_index]);
	#else 
 		pthread_mutex_lock(&outq_mutex[current_token]);
      		tw_eventq_pop(&outq[current_token]);
		pthread_mutex_unlock(&outq_mutex[current_token]);
	#endif
		// net_send_finish here
      		posted_sends.event_list[id] = e;      
		posted_sends.cur++;
      		
	#ifdef ROSS_GVT_mpi_allreduce
		me->s_nwhite_sent++;
		
		#ifdef DEBUG_ALI
			if (!g_tw_pid)
			{
				fprintf(stderr, "%lld sends remote to %d: %lld\n", 
					e -> send_pe, e -> recv_pe, me -> s_nwhite_sent);
					// e -> event_id, e -> recv_ts);
			}
			else
			{
				fprintf(stderr, "	%lld sends remote to %d: %lld\n", 
					e -> send_pe, e -> recv_pe, me -> s_nwhite_sent); 
					// e -> event_id, e -> recv_ts);
			}
		#endif
	#endif
      		
		changed = 1;
    	}

  	return changed;
}
#endif

static void
send_finish(tw_pe *me, tw_event *e, char * buffer)
{
	if (e -> state.owner == TW_net_asend) 
	{
    		if (e->state.cancel_asend) 
		{
      			/* Event was cancelled during transmission.  We must
       			* send another message to pass the cancel flag to
       			* the other node.
       			*/
		
			e -> state.cancel_asend = 0;
      			e -> state.cancel_q = 1;
		
			#ifdef Message_Aggregate
				pthread_mutex_lock(&outq_mutex[g_tw_tid][(e->recv_pe)/g_tw_n_threads]);
      				tw_eventq_push(&outq[g_tw_tid][(e->recv_pe)/g_tw_n_threads], e);				
				pthread_mutex_unlock(&outq_mutex[g_tw_tid][(e->recv_pe)/g_tw_n_threads]);
			#else
				pthread_mutex_lock(&outq_mutex[g_tw_tid]);
      				tw_eventq_push(&outq[g_tw_tid], e);				
				pthread_mutex_unlock(&outq_mutex[g_tw_tid]);
			#endif
    		} 
		else 
		{
      			/* Event finished transmission and was not cancelled.
       			* Add to our sent event queue so we can retain the
       			* event in case we need to cancel it later.  Note it
       			* is currently in remote format and must be converted
       			* back to local format for fossil collection.
       			*/
		
      			e->state.owner = TW_pe_sevent_q;
			if (g_tw_synchronization_protocol == CONSERVATIVE) tw_event_free(me, e);
    		}

    		return;
	}

	#ifndef MemCopyRem 
		if (e -> state.owner == TW_net_acancel) 
		{
			/* We just finished sending the cancellation message
     			* for this event.  We need to free the buffer and
     			* make it available for reuse.
     			*/
			tw_event_free(me, e);
    			return;
  		}

  		/* Never should happen, not unless we somehow broke this
   		* module's other functions related to sending an event.
   		*/
  		tw_error(
	   		TW_LOC,
	   		"Don't know how to finish send of owner=%u, cancel_q=%d",
	   		e->state.owner,
	   		e->state.cancel_q);
	#endif
}

void net_send_finish(tw_pe *me, tw_event *e, char * buffer)
{
	#ifdef SL_LOCKS 
  		e -> state.owner = TW_pe_free_q;
	#else
		tw_event_free(me,e);
	#endif
}

//Moves events from outq to appropriate PE queue
//This will involve invoking receive_finish for each event in the outq
static void mt_receive(tw_pe * me)
{
	int status,i;
	
	// fprintf(stderr, "%d\n", g_tw_machines); -> 2
	for (i = 0; i < g_tw_machines; i++)
	{
		while (inq[i].size > 0)
		{
			status = pthread_mutex_lock(&(mt_outq_ptr[g_tw_tid][i].mutex));
			if(status!=0) printf("In mt_receive: Lock failed error-code=%d\n",status);	
			tw_event *e = tw_eventq_pop(&inq[i]);

			if (e == NULL)
			{ 
				status = pthread_mutex_unlock(&(mt_outq_ptr[g_tw_tid][i].mutex));	
				continue;
			}

			recv_finish(me,e,NULL);
			
			status = pthread_mutex_unlock(&(mt_outq_ptr[g_tw_tid][i].mutex));	
			if(status!=0) printf("In mt_receive: UnLock failed error-code=%d\n",status);

			/*
 			*
 			*
 			*	Receiving regional
 			*
			*
 			*/
			me -> stats.s_nread_network++;
			#ifdef ROSS_GVT_mattern_orig
				if (e -> color == white) 
				{
					me -> msg_counters[g_tw_tid][g_tw_pid]--;	
					
					#ifdef DEBUG_ALI
					if (!g_tw_pid)
					{
						fprintf(stderr, "%u receives white from %lld: %d (%d)\n", 
							g_tw_tid, e -> send_pe, me -> msg_counters[g_tw_tid][g_tw_pid],
							e -> event_id);
					}
					else
					{
						fprintf(stderr, "	%u receives white from %lld: %d (%d)\n", 
							g_tw_tid, e -> send_pe, me -> msg_counters[g_tw_tid][g_tw_pid],
							e -> event_id);
					}
					#endif
				}
				else if (e -> color == red) 
				{
					#ifdef DEBUG_ALI
					if (g_tw_pid)
					{
						//fprintf(stderr, "	%llu receives red from %lld: %d - %.0f\n", 
						//	me -> id, e -> send_pe, e -> event_id, e -> recv_ts);
					}
					else
					{
						//fprintf(stderr, "%llu receives red from %lld: %d - %.0f\n", 
						//	me -> id, e -> send_pe, e -> event_id, e -> recv_ts);
					}
					#endif
				}
				else tw_error(TW_LOC, "Error: mattern orig receive");				
			#endif
		}
	}
}

void net_recv_finish(tw_pe *me, tw_event *e, char * buffer)
{
	int status;
	
	status = pthread_mutex_lock(&(mt_outq_ptr[((e->recv_pe)%g_tw_n_threads)][numa_node_id].mutex));
	if (status) printf("Error\n");
     	
	tw_eventq_unshift(mt_outq_ptr[((e -> recv_pe) % g_tw_n_threads)][numa_node_id].outq_ptr, e);  
	
	status = pthread_mutex_unlock(&(mt_outq_ptr[((e->recv_pe)%g_tw_n_threads)][numa_node_id].mutex));
	if (status) printf("Error\n");
}

static void mt_send(tw_pe *me,tw_event *e, int dest_node_id)
{
	int status;
	tw_event * cp_e = NULL;
	
	if (!e -> state.cancel_q) e -> event_id = (tw_eventid) ++me->seq_num;

	e -> send_pe = (tw_peid) g_tw_mynode;
	e -> state.owner = e->state.cancel_q ? TW_net_acancel : TW_net_asend;
	
	//Make a copy of the event message
	#ifdef MemCopyRem 
		if (e->state.owner == TW_net_asend) 
		{
			/* Event finished transmission and was not cancelled.
			 * Add to our sent event queue so we can retain the
			 * event in case we need to cancel it later.  Note it
			 * is currently in remote format and must be converted
			 * back to local format for fossil collection.
			 */
			cp_e = mt_tw_event_new(dest_node_id,e->recv_ts,e->src_lp); 	
		    	memcpy(cp_e,e,g_tw_event_msg_sz); 
		}
		else cp_e = e;

	#else	
		cp_e = mt_tw_event_new(dest_node_id, e->recv_ts, e->src_lp); //dummy event parameters doesn't have any significance
		memcpy(cp_e, e, g_tw_event_msg_sz); //make cp_e identical to e
	#endif

	/*
 	*
	* 
	*	 Sending regional
 	*
 	*
 	*/
	#ifdef ROSS_GVT_mattern_orig
		if (me -> color == white) 
		{
			me -> msg_counters[dest_node_id][numa_node_id]++;
			cp_e -> color = white;
			
			#ifdef DEBUG_ALI
			if (!g_tw_pid)
			{
				fprintf(stderr, "%d sends white to %d: %d\n", 
					g_tw_tid, dest_node_id, //e -> event_id);
					me -> msg_counters[dest_node_id][numa_node_id]);
			}
			else
			{
				fprintf(stderr, "	%d sends white to %d: %d\n", 
					g_tw_tid, dest_node_id, //e -> event_id);
					me -> msg_counters[dest_node_id][numa_node_id]);
			}
			#endif
		}
		else if (me -> color == red)
		{
			#ifdef DEBUG_ALI
			if (!g_tw_pid)
			{
				fprintf(stderr, "%d sends red to %d: %d\n", 
					g_tw_tid, dest_node_id, //e -> event_id);
					me -> msg_counters[dest_node_id][numa_node_id]);
			}
			else
			{
				fprintf(stderr, "	%d sends red to %d: %d\n", 
					g_tw_tid, dest_node_id, //e -> event_id);
					me -> msg_counters[dest_node_id][numa_node_id]);
			}
			#endif
			
			cp_e -> color = red;
			me -> t_red = min(me -> t_red, e -> recv_ts);
		}
		else tw_error(TW_LOC, "Error in mattern orig send"); 	
	#endif

	status = pthread_mutex_lock(&(mt_outq_ptr[dest_node_id][numa_node_id].mutex));	
	if (status!=0) printf("mt_send lock failed! error-code=%d\n",status);

	// dest_node_id: thread id in a machine, numa_node_id: machine rank
	tw_eventq_unshift(mt_outq_ptr[dest_node_id][numa_node_id].outq_ptr, cp_e);
	
	status = pthread_mutex_unlock(&(mt_outq_ptr[dest_node_id][numa_node_id].mutex));
	if (status!=0) printf("mt_send unlock failed! error-code=%d\n",status);

	#ifdef ROSS_GVT_mpi_allreduce
		me -> s_nwhite_sent++;
	
		#ifdef DEBUG_ALI
			if (!g_tw_pid)
			{
				fprintf(stderr, "%d sends regional to %d: %lld\n", 
					g_tw_tid, dest_node_id, me -> s_nwhite_sent);
					// e -> event_id, e -> recv_ts);
			}
			else
			{
				fprintf(stderr, "	%d sends regional to %d: %lld\n", 
					g_tw_tid, dest_node_id, me -> s_nwhite_sent); 
					// e -> event_id, e -> recv_ts);
			}
		#endif
	#endif

	//me -> stats.s_nsend_loc_remote++;
	// Send finish part

	#ifndef MemCopyRem 
	if (e->state.owner == TW_net_asend) 
	{
		/* Event finished transmission and was not cancelled.
		 * Add to our sent event queue so we can retain the
		 * event in case we need to cancel it later.  Note it
		 * is currently in remote format and must be converted
		 * back to local format for fossil collection.
		 */
		e->state.owner = TW_pe_sevent_q;
		
		if (g_tw_synchronization_protocol == CONSERVATIVE) tw_event_free(me, e);
		return;
	}

	if (e->state.owner == TW_net_acancel) 
	{
		/* We just finished sending the cancellation message
		 * for this event.  We need to free the buffer and
		 * make it available for reuse.
		 */
		
		//DJ: It means this is a cancelation message we can free this event immidiately  
		tw_event_free(me, e);
		return;
	}

  	/* Never should happen, not unless we somehow broke this
   	* module's other functions related to sending an event.
   	*/

  	tw_error(
           	TW_LOC,
           	"Don't know how to finish send of owner=%u, cancel_q=%d",
           	e->state.owner,
           	e->state.cancel_q);
	#else
	if (e->state.owner == TW_net_asend) 
	{
		/* Event finished transmission and was not cancelled.
		 * Add to our sent event queue so we can retain the
		 * event in case we need to cancel it later.  Note it
		 * is currently in remote format and must be converted
		 * back to local format for fossil collection.
		 */
	
		me -> stats.s_nsend_regional++;
			
		e->state.owner = TW_pe_sevent_q;
		if (g_tw_synchronization_protocol == CONSERVATIVE) tw_event_free(me, e);
		return;
	}
	#endif
}

/*
 * NOTE: Chris believes that this network layer is too aggressive at
 * reading events out of the network.. so we are modifying the algorithm
 * to only send events when tw_net_send it called, and only read events
 * when tw_net_read is called.
 */
void
tw_net_read(tw_pe *me)
{
	tw_stime r = tw_clock_read(); 
	#ifdef SEPARATE_1
		if (g_tw_tid > 1) mt_receive(me);
	#elif defined(SEPARATE_0)
		if (g_tw_tid) mt_receive(me);
	#else	
		mt_receive(me);
	#endif
	me -> stats.s_regional_read += tw_clock_read() - r;

	if (!g_tw_tid)
	{
		tw_stime rr = tw_clock_read(); 
		int i = 0;

		#ifdef Message_Aggregate
			#ifdef prob_opt
	  			while(i < g_tw_n_threads)
				{
					current_token = (current_token + 1) % g_tw_n_threads;
	
					int changed;
					do {
					
						// for(i=0;i<g_tw_n_threads;i++)
						// {
							// current_token=(current_token+1)%g_tw_n_threads;

						for (queue_index = 0; queue_index < g_tw_machines; queue_index++)
						{
							changed |= test_q(&posted_sends, me, net_send_finish);
						 	changed |= send_begin(me);
						}

						// } 
						changed  = test_q(&posted_recvs, me, net_recv_finish);
				
						if((++poll_count)%4==0)
						// if((++poll_count)%2==0)
						// if((++poll_count)%20==0)
						{
							changed |= recv_begin(me);
						}
			 		} while (changed);
			
					i++;
				}
			#else
				// old
				while(i<g_tw_n_threads)
				{
					current_token=(current_token+1)%g_tw_n_threads;

					for(queue_index=0;queue_index<g_tw_machines;queue_index++)
					{
						service_queues(me);
					}
					i++;
				}
			#endif
		#else
			while (i < g_tw_n_threads)
			{
				current_token = (current_token + 1) % g_tw_n_threads;
	  			int changed;
	
				//if (!g_tw_pid) fprintf(stderr, "%llu in while %.0f\n", me -> id, me -> GVT);		
				//else fprintf(stderr, "	%llu in while %.0f\n", me -> id, me -> GVT);	
				
				//do {
					// Returns 0 if any irecv is not completed or posted_recv is empty
					// otherwise returns 1
					tw_clock test_recv = tw_clock_read();	
					changed  = test_q(&posted_recvs, me, net_recv_finish);
					me -> stats.s_remote_read_test_recv += tw_clock_read() - test_recv;
					
					tw_clock test_send = tw_clock_read();	
					changed |= test_q(&posted_sends, me, net_send_finish);  
					me -> stats.s_remote_read_test_send += tw_clock_read() - test_send;
					
					#ifdef prob_opt	
						if((++poll_count) % 4 == 0) changed |= recv_begin(me);
					#else	
						// Scans posted_recvs and returns 0 if irecv fails or iprobe 
						// returns 0 otherwise performs irecv and returns 1
						tw_clock irecv = tw_clock_read();	
						changed |= recv_begin(me);
						me -> stats.s_remote_read_irecv += tw_clock_read() - irecv;
					#endif
					
					// Scans posted_sends and returns 0 if isend fails or outq -> peek is 
					// null otherwise perfroms isend and returns 1
					tw_clock isend = tw_clock_read();	
					changed |= send_begin(me);
					me -> stats.s_remote_read_isend += tw_clock_read() - isend;
				//} while (changed);

				// service_queues(me);
				i++;  			  
			}
		#endif
		current_token = -1;
		me -> stats.s_remote_read += tw_clock_read() - rr;
	}	
}

void network_send(tw_pe *me, tw_event *e)
{
	e -> state.owner = TW_net_outq;
	tw_node * dest_node = NULL;
		 
     	tw_event * cp_e = NULL;
      	e -> recv_pe = 0;  

	dest_node = tw_net_onnode((*e->src_lp->type.map)((tw_lpid) e->dest_lp));
		 
	if (!e -> state.cancel_q) e -> event_id = (tw_eventid) ++me->seq_num;
		
	e -> send_pe = (tw_peid) g_tw_mynode;	
	e -> state.owner = e -> state.cancel_q ? TW_net_acancel : TW_net_asend;
	e -> recv_pe = (tw_peid)(*dest_node);

	#ifdef MemCopyRem
  		if (e->state.owner == TW_net_asend) 
		{
   			cp_e = mt_tw_event_new(e -> recv_pe, e -> recv_ts, e -> src_lp);
	 		memcpy(cp_e, e , g_tw_event_msg_sz);	 					
		}
		else cp_e=e;
	#else
		cp_e = mt_tw_event_new(e -> recv_pe, e -> recv_ts, e -> src_lp);
		memcpy(cp_e,e,g_tw_event_msg_sz);
	#endif


	#ifdef Message_Aggregate
		pthread_mutex_lock(&outq_mutex[g_tw_tid][(e->recv_pe)/g_tw_n_threads]);
		tw_eventq_unshift(&outq[g_tw_tid][(e -> recv_pe) / g_tw_n_threads], cp_e);
		pthread_mutex_unlock(&outq_mutex[g_tw_tid][(e->recv_pe)/g_tw_n_threads]);
	#else
		
		/*
 		*
 		*
 		*
 		* Sending Remote
 		*
 		*
 		*
		
		#ifdef ROSS_GVT_mattern_orig
		int _thread_id = (e -> recv_pe) % g_tw_n_threads;
		int _machine_id = (e -> recv_pe) / g_tw_n_threads;

		if (me -> color == white) 
		//if (e -> src_lp_ptr -> pe -> color == white)
		{
			cp_e -> color = white;
			me -> msg_counters[_thread_id][_machine_id]++;			
			//e -> src_lp_ptr -> pe -> msg_counters[_thread_id][_machine_id]++;			

			#ifdef DEBUG_ALI
			if (!g_tw_pid)
			{
				fprintf(stderr, "%lld sends remote white to %d: %d (%d)\n", 
					e -> send_pe, e -> recv_pe, me -> msg_counters[_thread_id][_machine_id],
					e -> event_id); //, e -> recv_ts);
			}
			else
			{
				fprintf(stderr, "	%lld sends remote white to %d: %d (%d)\n", 
					e -> send_pe, e -> recv_pe, me -> msg_counters[_thread_id][_machine_id],
					e -> event_id); //, e -> recv_ts);
			}
			#endif

		}
		else if (me -> color == red)
		//if (e -> src_lp_ptr -> pe -> color == red)
		{
			cp_e -> color = red;
			me -> t_red = min(me -> t_red, e -> recv_ts);
			//e -> src_lp_ptr -> pe -> t_red = min(me -> t_red, e -> recv_ts);
			
			#ifdef DEBUG_ALI
			if (!g_tw_pid)
			{
				//fprintf(stderr, "%lld sends remote red to %d: %d (%d)\n", 
				//	e -> send_pe, e -> recv_pe, me -> msg_counters[_thread_id][_machine_id],
				//	e -> event_id);
			}
			else
			{
				//fprintf(stderr, "	%lld sends remote red to %d: %d (%d)\n", 
				//	e -> send_pe, e -> recv_pe, me -> msg_counters[_thread_id][_machine_id],
				//	e -> event_id);
			}
			#endif
		}
		else tw_error(TW_LOC, "Error in mattern orig send"); 	
		#endif
 		*/

		pthread_mutex_lock(&outq_mutex[g_tw_tid]);
		tw_eventq_unshift(&outq[g_tw_tid], cp_e);
		pthread_mutex_unlock(&outq_mutex[g_tw_tid]);
	#endif

	send_finish(me,e,NULL);
}

void
tw_net_send(tw_event *e, int cancel_msg)
{
	tw_node * dest_node = NULL;
	int dest_node_id = 0;
	tw_pe * me = e -> src_lp -> pe;

	e -> state.remote = 0;
        if (e == me -> abort_event) tw_error(TW_LOC, "sending abort event!");
        
	dest_node = tw_net_onnode((*e->src_lp->type.map)((tw_lpid) e->dest_lp));
        dest_node_id = *dest_node;

	if (dest_node_id / g_tw_n_threads == g_tw_pid)
	{
		// to local thread
		#ifdef rollback_stats   
	 		e->recv_pe = (tw_peid)(*dest_node);
		#endif
		
		if (cancel_msg) me -> stats.s_cancel_regional++;	
		me -> stats.s_nsend_regional++;	
		
		tw_stime s = tw_clock_read();
		mt_send(me, e, (dest_node_id % g_tw_n_threads));
		me -> stats.s_regional_send += tw_clock_read() - s;
   	}
	else 
	{
		if (cancel_msg) me -> stats.s_cancel_remote++;	

		me -> stats.s_nsend_network++;
		tw_stime s = tw_clock_read();
		network_send(me,e);
		me -> stats.s_remote_send += tw_clock_read() - s;
	}
}

void
tw_net_cancel(tw_event *e)
{
	tw_pe *src_pe = e->src_lp->pe;
  
	switch (e->state.owner) 
	{
  		case TW_net_outq:
    			/* Cancelled before we could transmit it.  Do not
     			* transmit the event and instead just release the
     			* buffer back into our own free list.
     			*/

			//jwang
			#ifdef Message_Aggregate
				pthread_mutex_lock(&outq_mutex[g_tw_tid][(e->recv_pe)/g_tw_n_threads]);
    				tw_eventq_delete_any(&outq[g_tw_tid][(e->recv_pe)/g_tw_n_threads], e);
	  			pthread_mutex_unlock(&outq_mutex[g_tw_tid][(e->recv_pe)/g_tw_n_threads]);
			#else
				pthread_mutex_lock(&outq_mutex[g_tw_tid]);
    				tw_eventq_delete_any(&outq[g_tw_tid], e);
	  			pthread_mutex_unlock(&outq_mutex[g_tw_tid]);
			#endif    
			// tw_eventq_delete_any(&outq, e);
			tw_event_free(src_pe, e);
    			return;
    			break;
 		
		case TW_net_asend:
    			/* Too late.  We've already let MPI start to send
     			* this event over the network.  We can't pull it
     			* back now without sending another message to do
     			* the cancel.
     			*
     			* Setting the cancel_q flag will signal us to do
     			* another message send once the current send of
     			* this message is completed.
     			*/
			// printf("XXX  !!Khatra DANGER ZONE!! XXX\n");
    			e->state.cancel_asend = 1;
    			break;

  		case TW_pe_sevent_q:
    			/* Way late; the event was already sent and is in
     			* our sent event queue.  Mark it as a cancel and
     			* place it at the front of the outq.
     			*/
 
			//DJ: This is the only case for muitithreaded implementation
			//Because above two are not present in this case
			//Whatever events are generated during processing are sent immidiately
			//So they must be cancelled by sending explicit cancel message

    			e->state.cancel_q = 1;

			//Change this
    			//tw_eventq_unshift(&outq, e);
			//DJ:
			src_pe -> stats.s_cancel++;
			tw_net_send(e, 1);	
    			break;

  		default:
    			/* Huh?  Where did you come from?  Why are we being
     			* told about you?  We did not send you so we cannot
     			* cancel you!
     			*/
    
			tw_error(
	     			TW_LOC,
	     			"Don't know how to cancel event owned by %u",
	     			e->state.owner);
 	}
	tw_net_read(src_pe);
}

tw_statistics *
tw_net_statistics(tw_pe * me, tw_statistics * s)
{
	tw_statistics node_s;
	bzero(&node_s, sizeof(node_s));
	mt_allreduce(&(s->s_max_run_time), &(node_s.s_max_run_time), 1, MT_DOUBLE, MT_MAX, &max_barrier);

	if(g_tw_tid==0)
	{
		if(MPI_Reduce(&(node_s.s_max_run_time), 
                	&me->stats.s_max_run_time,
                	1,
	                 MPI_DOUBLE,
        	        MPI_MAX,
	                g_tw_masternode,
        	        MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}
	
	#ifdef rollback_stats
	mt_allreduce(&(s->s_net_events), &(node_s.s_net_events), 22, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
        sum_index=(sum_index+1)%2;
  	
	if( g_tw_tid==0)
	{
      		if(MPI_Reduce(&(node_s.s_net_events),
                  &me->stats.s_net_events,
                  22,
                  MPI_LONG_LONG,
                  MPI_SUM,
                  g_tw_masternode,
                  MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
  	}
	#else
	
	mt_allreduce(&(s->s_net_events), &(node_s.s_net_events), 16, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
        sum_index=(sum_index+1)%2;
	
	if(g_tw_tid==0)
	{
		  if(MPI_Reduce(&(node_s.s_net_events), 
        	        &me->stats.s_net_events,
                	16,
	                MPI_LONG_LONG,
                	MPI_SUM,
	       	        g_tw_masternode,
        	        MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}
	#endif

	mt_allreduce(&(s->s_nsend_loc), &(node_s.s_nsend_loc), 16, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
        sum_index=(sum_index+1)%2;
	if(g_tw_tid==0)
	{
		  if(MPI_Reduce(&(node_s.s_nsend_loc), 
        	        &me->stats.s_nsend_loc,
                	16,
	                MPI_LONG_LONG,
                	MPI_SUM,
	       	        g_tw_masternode,
        	        MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	mt_allreduce(&(s->s_nsend_loc_remote), &(node_s.s_nsend_loc_remote), 16, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
        sum_index=(sum_index+1)%2;
	if(g_tw_tid==0)
	{
		  if(MPI_Reduce(&(node_s.s_nsend_loc_remote), 
        	        &me->stats.s_nsend_loc_remote,
                	16,
	                MPI_LONG_LONG,
                	MPI_SUM,
	       	        g_tw_masternode,
        	        MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}


	mt_allreduce(&s->s_total, &(node_s.s_total), 8, MT_LONG_LONG, MT_MAX, &max_barrier);
	if (g_tw_tid==0)
	{
		if(MPI_Reduce(&(node_s.s_total), 
	                &me->stats.s_total,
        	        8,
	                MPI_LONG_LONG,
        	        MPI_MAX,
	                g_tw_masternode,
        	        MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");	
	}

	#ifdef Message_Aggregate
		if(g_tw_tid==0)
		{
			if(MPI_Reduce(&tmp_message, 
        	        	&total_message_number,
                		1,
	                	MPI_LONG_LONG,
                		MPI_SUM,
	       	        	g_tw_masternode,
        	        	MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
			
			if(MPI_Reduce(&tmp_event, 
        	        	&total_event_number,
                		1,
	                	MPI_LONG_LONG,
                		MPI_SUM,
	       	        	g_tw_masternode,
        	        	MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
       
      			if(MPI_Reduce(&tmp_message_aggre, 
        		        &total_message_aggre_number,
                		1,
	        	        MPI_LONG_LONG,
                		MPI_SUM,
	       		        g_tw_masternode,
        		        MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
		}
	#endif

	#ifdef prob_stats
		//polling messages	
		if (g_tw_tid==0)
		{
			if(MPI_Reduce(&suc_poll, 
        	        	&total_sucessful_poll_messages,
                		1,
	                	MPI_LONG_LONG,
                		MPI_SUM,
	       	        	g_tw_masternode,
        	        	MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
  
			if (MPI_Reduce(&total_poll, 
        	        	&total_poll_messages,
                		1,
	                	MPI_LONG_LONG,
                		MPI_SUM,
	       	        	g_tw_masternode,
        	        	MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!"); 
 		}
	#endif

	return &me->stats;
}
