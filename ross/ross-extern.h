#ifndef INC_ross_extern_h
#define	INC_ross_extern_h
#include "mt_barrier_sync.h"
//DJ
extern int no_threads;
extern void	tw_rand_init_streams(tw_lp * lp, unsigned int nstreams);
extern __thread int g_tw_machines;

/*
 * tw-stats.c
 */
extern void tw_stats(tw_pe * me, int n);

/*
 * queue-*.c
 */
extern tw_pq *tw_pq_create(void);
extern void tw_pq_enqueue(tw_pq *, tw_event *);
extern tw_event *tw_pq_dequeue(tw_pq *);
extern tw_stime tw_pq_minimum(tw_pq *);
extern void tw_pq_delete_any(tw_pq *, tw_event *);
extern unsigned int tw_pq_get_size(tw_pq *);
extern unsigned int tw_pq_max_size(tw_pq *);

/*
 * tw-timer.c
 */

/*
 * ross-global.c
 */




#ifdef Message_Aggregate
extern __thread tw_stat total_event_number;
extern __thread tw_stat total_message_number;
extern __thread tw_stat total_message_aggre_number;
#endif

extern __thread  tw_stat total_sucessful_poll_messages;
extern __thread  tw_stat total_poll_messages;


//#ifdef partition 
extern __thread unsigned int local_nodes;
extern __thread unsigned int my_region;
//#endif 

//print message
//poll messages
extern tw_synch g_tw_synchronization_protocol;
extern __thread map_local_f g_tw_custom_lp_global_to_local_map;
extern __thread map_custom_f g_tw_custom_initial_mapping;
extern __thread tw_lp_map g_tw_mapping;   
extern __thread tw_lpid  g_tw_nlp;
extern __thread tw_lpid	g_tw_lp_offset;
extern __thread tw_kpid  g_tw_nkp;
extern __thread tw_lp	**g_tw_lp;
extern __thread tw_kp	**g_tw_kp;

#ifdef SL_GVT
	#ifdef ROSS_GVT_mpi_allreduce
		tw_stime ** lvt_list;
		tw_stat ** white_sent_list;
		tw_stat ** white_received_list;
	#elif ROSS_GVT_mattern_orig
		extern int *** msg_counter_array;
		double ** t_min_list;
		double ** t_red_list;
	#endif
#endif

#if defined(SL_LOCKS) || defined(SL_BARRIER) 
extern tw_kp ** kp_list;
extern tw_eventq ** freeq_list;
extern tw_stime ** gvt_list;
#endif

#ifdef SL_LOCKS
static inline void tw_event_free_from_fossil_collect(tw_pe *, tw_event *);
extern pthread_mutex_t 	*  kp_locks;
extern pthread_mutex_t * freeq_locks; 
#endif

extern __thread int      g_tw_sv_growcnt;
extern __thread int      g_tw_fossil_attempts;
extern __thread unsigned int	g_tw_nRNG_per_lp;
extern __thread tw_lpid		g_tw_rng_default;
extern __thread size_t		g_tw_rng_max;
extern __thread tw_seed		*g_tw_rng_seed;
extern unsigned int	g_tw_mblock;

//#ifndef gvt7clock
extern unsigned int g_tw_gvt_interval;
//#endif

extern tw_stime		g_tw_ts_end;
extern __thread unsigned int	g_tw_sim_started;
extern __thread unsigned int	g_tw_master;
extern __thread size_t		g_tw_msg_sz;
extern __thread size_t		g_tw_event_msg_sz;

extern __thread unsigned int	g_tw_memory_nqueues;
extern __thread size_t		g_tw_memory_sz;

extern __thread unsigned int     g_tw_periodicity;

extern __thread tw_stime         g_tw_lookahead;

extern __thread tw_peid  g_tw_npe;
extern __thread tw_pe **g_tw_pe;
//extern int      g_tw_events_per_pe;
extern __thread int      g_tw_events_per_pe;

extern __thread unsigned int	    g_tw_gvt_threshold;
extern __thread unsigned int	    g_tw_gvt_done;

extern __thread unsigned int	g_tw_net_device_size;
extern __thread tw_node g_tw_mynode;
extern __thread tw_node g_tw_masternode;
//DJ
extern __thread int g_tw_n_threads;
extern __thread int g_tw_pid;
extern __thread int g_tw_tid;


extern __thread FILE		*g_tw_csv;
//DJ
extern __thread int local_numa_id;
        /*
	 * Cycle Counter variables
	 */

extern __thread tw_clock g_tw_cycles_gvt;
extern __thread tw_clock g_tw_cycles_ev_abort;
extern __thread tw_clock g_tw_cycles_ev_proc;
extern __thread tw_clock g_tw_cycles_ev_queue;
extern __thread tw_clock g_tw_cycles_rbs;
extern __thread tw_clock g_tw_cycles_cancel;
/*
* mt_barrier_sync.[hc]
*/
extern barrier_t g_barrier;
extern barrier_t sum_barrier[2];
extern barrier_t min_barrier[2];
extern barrier_t max_barrier;
extern __thread int sum_index;
extern __thread int min_index;
extern int barrier_dbg;
extern void init_barrier(barrier_t *barrier,int n);
extern void barrier_sync(barrier_t *barrier,long tid);
extern void destroy_barrier(barrier_t *barrier);

/*
 * clock-*
 */
extern const tw_optdef *tw_clock_setup();
extern void tw_clock_init(tw_pe * me);
extern tw_clock tw_clock_now(tw_pe * me);
extern tw_clock tw_clock_read();
extern tw_stime g_tw_clock_rate;

/*
 * signal-*.c
 */
extern void     tw_register_signals(void);

/*
 * tw-event.c
 */
extern void		 tw_event_send(tw_event * event);

extern void		 tw_event_rollback(tw_event * event);
/*
 * ross-inline.h
 */
static inline void 		 tw_event_free(tw_pe *, tw_event *);

/*
 * tw-lp.c
 */
extern tw_lp		*tw_lp_next_onpe(tw_lp * last, tw_pe * pe);
extern void		 tw_lp_settype(tw_lpid lp, const tw_lptype * type);
extern void		 tw_lp_onpe(tw_lpid index, tw_pe * pe, tw_lpid id);
extern void		 tw_lp_onkp(tw_lp * lp, tw_kp * kp);
extern void		 tw_init_lps(tw_pe * me);

/*
 * tw-kp.c
 */
extern void	 tw_kp_onpe(tw_kpid id, tw_pe * pe);
extern void	 kp_fossil_remote(tw_kp * kp);
extern tw_kp	*tw_kp_next_onpe(tw_kp * last, tw_pe * pe);
extern void	 tw_init_kps(tw_pe * me);

extern void	 tw_kp_rollback_event(tw_event *event);
#ifdef rollback_stats 
extern void	 tw_kp_rollback_to(tw_kp * kp, tw_stime to,tw_event*event);
#else
extern void	 tw_kp_rollback_to(tw_kp * kp, tw_stime to);
#endif

extern void	 tw_kp_fossil_memoryq(tw_kp * me, tw_fd);
extern void	 tw_kp_fossil_memory(tw_kp * me);

/*
 * tw-pe.c
 */
extern tw_pe		*tw_pe_next(tw_pe * last);
extern void		 tw_pe_settype(tw_pe *, const tw_petype * type);
extern void		 tw_pe_create(tw_peid npe);
extern void		 tw_pe_init(tw_peid id, tw_peid global);
extern void		 tw_pe_fossil_collect(tw_pe * me);
extern tw_fd		 tw_pe_memory_init(tw_pe * pe, size_t n_mem, 
					   size_t d_sz, tw_stime mult);

/*
 * tw-setup.c
 */
//DJ
extern void tw_init(int *argc, char ***argv,int thread_id);
//jwang
#ifdef New_map
extern void tw_define_lps(tw_lpid nlp, size_t msg_sz, tw_seed * seed,unsigned int offset);
#else
extern void tw_define_lps(tw_lpid nlp, size_t msg_sz, tw_seed * seed);
#endif
extern void tw_run(int n, int gvt_interval, int * epg, double * reg, double * rem);
extern void tw_end(void);

/*
 * tw-sched.c
 */
extern void tw_scheduler_sequential(tw_pe * me, int n);
extern void tw_scheduler_conservative(tw_pe * me, int n);
extern void tw_scheduler_optimistic(tw_pe * me, int n, int * epg, double * reg, double * rem);
extern void sched_event();
extern void batch_event();

/*
 * tw-signal.c
 */
extern void     tw_sigsegv(int sig);
extern void     tw_sigterm(int sig);

/*
 * tw-state.c
 */
extern void tw_state_save(tw_lp * lp, tw_event * cevent);
extern void tw_state_rollback(tw_lp * lp, tw_event * revent);
extern void tw_state_alloc(tw_lp * lp, int nvect);

/*
 * tw-timing.c
 */
extern   void     tw_wall_now(tw_wtime * t);
extern   void     tw_wall_sub(tw_wtime * r, tw_wtime * a, tw_wtime * b);
extern   double   tw_wall_to_double(tw_wtime * t);

/*
 * tw-memory.c
 */
#ifdef ROSS_MEMORY
extern size_t	tw_memory_allocate(tw_memoryq *);
#endif

/*
 * tw-util.c
 */

#define	TW_LOC	__FILE__,__LINE__
extern void tw_error(
	const char *file,
	int line,
	const char *fmt,
	...) NORETURN;
extern void tw_printf(
	const char *file,
	int line,
	const char *fmt,
	...);
extern void tw_exit(int rv);
extern void tw_calloc_stats(size_t *alloc, size_t *waste);
extern void* tw_calloc(
	const char *file,
	int line,
	const char *for_who,
	size_t e_sz,
	size_t n);
extern void* tw_unsafe_realloc(
	const char *file,
	int line,
	const char *for_who,
	void *addr,
	size_t len);


#endif
