#include <ross.h>
#include <stddef.h>

#ifdef SL_BARRIER	
static pthread_barrier_t fossil_barrier;
#endif

static pthread_barrier_t barrier;

// Statistics
static __thread unsigned int gvt_force = 0;
static __thread tw_stime measure_fc = 0;
static __thread tw_stime measure_gvt = 0;
static __thread tw_stime mpi_b = 0;
static __thread tw_stime delay = 0;

static int c1_counter; 		// Counts how many threads reached the C 1
static int c1_completed;
static int c2_counter;		// Counts how many threads reached the C 2
static int c2_completed;

static int gvt_done_counter;
static double global_gvt;

#define num_nodes 2
typedef struct control // control block
{
	double t_min;
	double t_red;
	int msg_counters[256][num_nodes]; // [0][1] holds number of messages sent from all thread to the thread 0 in machine 1
} _control;

static _control control_message;
static pthread_mutex_t cm_lock;

static MPI_Datatype mpi_control_message;
static MPI_Op custom_op;

void 
tw_gvt_start()
{
	if (!g_tw_tid)
	{
		c1_completed = 0;
		c1_counter = 0;
		c2_counter = 0;
		c2_completed = 0;
		gvt_done_counter = 0;
		reset_control_message();

		MPI_Aint displacements[3] = {offsetof(_control, t_min), 
				     	     offsetof(_control, t_red),
				             offsetof(_control, msg_counters)};
		
		int block_lengths[3] = {1, 1, no_threads * g_tw_machines};
		MPI_Datatype types[3] = {MPI_DOUBLE, MPI_DOUBLE, MPI_INT};

		int rv = MPI_Type_create_struct(3, block_lengths, displacements, types, &mpi_control_message);
		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "mpi type creation failed!");
		
		rv = MPI_Type_commit(&mpi_control_message);
		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "mpi type commit failed!");	
		
		if (pthread_mutex_init(&cm_lock, NULL) != 0) tw_error(TW_LOC, "Mutex init failed\n");
	
		rv = MPI_Op_create(mpi_func, 1, &custom_op);
		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "mpi op create failed!");	
	}	
}	

void 
tw_gvt_step1(tw_pe * me)
{
	//if (g_tw_tid && delay > 0.01) 
	//{
		//volatile double result = 0;
		//volatile int i;
		//for (i = 0; i < 200000; i++)
		//{
                //	result = result + (i * 315789) / 12345;
                //	result += 30000;
        	//}
		//pthread_barrier_wait(&barrier);
	//}

	// C 1 is being constructed
	if (g_tw_tid && me -> color == white && !gvt_done_counter)
	{	
		// Each thread checks the C 1 and accumulate its message counters
		measure_gvt = tw_clock_read();
		me -> color = red;
		me -> t_red = g_tw_ts_end;			
			
		accumulate_msg_counters(me); // control block is updated 
			
		__sync_add_and_fetch(&c1_counter, 1);	
		
		#ifdef DEBUG_M
		if (c1_counter == g_tw_n_threads - 1)
		{
			if (!g_tw_pid) fprintf(stderr, "%u becomes red (%.0f)\n", g_tw_tid, me -> GVT);
			else fprintf(stderr, "	%u becomes red (%.0f)\n", g_tw_tid, me -> GVT);
		}
		#endif
	}
	// Each thread is done with C 1, now accumulate CM between the machines
	else if (c1_counter == g_tw_n_threads - 1 && !g_tw_tid)
	{
		#ifdef DEBUG_M
		if (!g_tw_pid) fprintf(stderr, "%u exchanges cm (%.0f)\n", g_tw_tid, me -> GVT);
		else fprintf(stderr, "	%u exchanges cm (%.0f)\n", g_tw_tid, me -> GVT);
		#endif
		
		mpi_b = tw_clock_read();
		if (!me -> id) exchange_cm(1);
		else exchange_cm(0);
		
		//_control cm;
                //all_reduce_cm(&cm);
                //reset_local_cm_with_other_cm(&cm);
		me -> stats.s_mpi_barrier += tw_clock_read() - mpi_b;

		// At this point the message accumulation should be done
		// Local cm of both machines should be same and has all the message counts
		me -> color = red;
		c1_counter = 0;
		c1_completed = 1;
	}
	// C 2 is being contructed
	else if (me -> color == red && c1_completed)
	{
		if (!me -> c2_checked && g_tw_tid &&
		     me -> msg_counters[g_tw_tid][g_tw_pid] + control_message.msg_counters[g_tw_tid][g_tw_pid] == 0)
		{
			me -> c2_checked = 1; 
			update_control(me);
			__sync_add_and_fetch(&c2_counter, 1);
			
			#ifdef DEBUG_M
			if (c2_counter == g_tw_n_threads - 1) 
			{
				if (!g_tw_pid) fprintf(stderr, "%u updates cm (%.0f)\n", g_tw_tid, me -> GVT);
				else fprintf(stderr, "	%u updates cm (%.0f)\n", g_tw_tid, me -> GVT);
			}
			#endif
		}
		else if (!me -> c2_checked && g_tw_tid)
		{
			#ifdef DEBUG_M
			//if (!g_tw_pid) fprintf(stderr, "%u received: %d, sent to it: %d\n", 
			//	g_tw_tid, me -> msg_counters[g_tw_tid][g_tw_pid], control_message.msg_counters[g_tw_tid][g_tw_pid]);
			//else fprintf(stderr, "	%u received: %d, sent to it: %d\n", 
			//	g_tw_tid, me -> msg_counters[g_tw_tid][g_tw_pid], control_message.msg_counters[g_tw_tid][g_tw_pid]);
			#endif
		}

		// Each thread is done with C 2, now update CM between the machines
		if (c2_counter == g_tw_n_threads - 1 && !g_tw_tid)
		{		
			#ifdef DEBUG_M
			if (!g_tw_pid) fprintf(stderr, "%u exchanges gvt (%.0f)\n", g_tw_tid, me -> GVT);
			else fprintf(stderr, "	%u exchanges gvt (%.0f)\n", g_tw_tid, me -> GVT);
			#endif
	
			mpi_b = tw_clock_read();
			if (!me -> id) exchange_gvt(1);
			else exchange_gvt(0);
			
			//double local_gvt = min(control_message.t_min, control_message.t_red);
                        //all_reduce_gvt(&local_gvt);	
			me -> stats.s_mpi_barrier += tw_clock_read() - mpi_b;
	
			g_tw_gvt_done++;
			me -> color = white;
			c2_counter = 0;
			c2_completed = 1;
		}

		if (c2_completed && g_tw_tid)
		{
			fossil_collect(me);
			finish_gvt(me);
		
			g_tw_gvt_done++;
			if (me -> id == 1 && me -> GVT / g_tw_ts_end > percent_complete) gvt_print(me -> GVT);
			
			if (__sync_add_and_fetch(&gvt_done_counter, 1) == g_tw_n_threads - 1)
			{
				#ifdef DEBUG_M
				if (!g_tw_pid) fprintf(stderr, "%u fossil (%.0f)\n", g_tw_tid, me -> GVT);
				else fprintf(stderr, "	%u fossil (%.0f)\n", g_tw_tid, me -> GVT);
				#endif

				interval_counter = g_tw_gvt_interval;
				reset_control_message();
				c1_completed = 0;
				c2_completed = 0;
				gvt_done_counter = 0;
			} 
			delay = tw_clock_read() - measure_gvt;
			//if (!g_tw_pid) fprintf(stderr, "%u delay: (%.5f)\n", g_tw_tid, delay / g_tw_clock_rate);
			//else fprintf(stderr, "	%u delay: (%.5f)\n", g_tw_tid, delay / g_tw_clock_rate);
			
			me -> stats.s_gvt += delay;
		}
	}
}

void 
mpi_func(_control * in, _control * inout, int * len, MPI_Datatype * dptr)
{
	if (*len != 1) MPI_Abort(MPI_COMM_WORLD, 1);	

	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++)
		{
			inout -> msg_counters[i][j] = inout -> msg_counters[i][j] + in -> msg_counters[i][j];		
		}
	}	
}

void
all_reduce_cm(void * cm)
{
	int rv = MPI_Allreduce(
		&control_message,
		cm,
		1,
		mpi_control_message,
		custom_op,
		MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_ALL_REDUCE Failed");	
}

void
all_reduce_gvt(void * local_gvt)
{
	int rv = MPI_Allreduce(
			local_gvt,
			&global_gvt,
			1,
			MPI_DOUBLE,
			MPI_MIN,
			MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_ALL_REDUCE Failed");	
}

void 
update_control(tw_pe * me)
{
	pthread_mutex_lock(&cm_lock);
	control_message.t_min = min(control_message.t_min, min_pq_outq(me));
	control_message.t_red = min(control_message.t_red, me -> t_red);	
	pthread_mutex_unlock(&cm_lock);
	
	me -> msg_counters[g_tw_tid][g_tw_pid] = 0;
}

void
reset_local_cm_with_other_cm(_control * cm)
{
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++)
		{
			control_message.msg_counters[i][j] = cm -> msg_counters[i][j];
		}
	}
}

void 
accumulate_msg_counters(tw_pe * me)
{
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++)
		{
			if (i != g_tw_tid || j != g_tw_pid)
			{
				__sync_add_and_fetch(&control_message.msg_counters[i][j], me -> msg_counters[i][j]); 
				me -> msg_counters[i][j] = 0;
			}
		}
	}
}

void
reset_control_message()
{
	control_message.t_min = g_tw_ts_end + 1;
	control_message.t_red = g_tw_ts_end + 1;
	for (int i = 0; i < g_tw_n_threads; i++) for (int j = 0; j < g_tw_machines; j++) 
	{
		control_message.msg_counters[i][j] = 0;
	}
}

void 
fossil_collect(tw_pe * me)
{
	me -> GVT = global_gvt;
	
	measure_fc = tw_clock_read();

	#ifdef SL_LOCKS
		if (g_tw_tid == 1) tw_pe_fossil_collect(me);
	#elif defined(SL_BARRIER)
		pthread_barrier_wait(&fossil_barrier);
		if (g_tw_tid == 1)  tw_pe_fossil_collect(me);
		pthread_barrier_wait(&fossil_barrier);
	#else
		tw_pe_fossil_collect(me);
	#endif
	
	me -> stats.s_fossil_collect += tw_clock_read() - measure_fc;
}

void 
finish_gvt(tw_pe * me)
{	
	me -> trans_msg_ts = DBL_MAX + 3;
	me -> c2_checked = 0;
	me -> color = white;
}


tw_stime
min_pq_outq(tw_pe * me)
{
	// tw_net_read(me); // might be neccessary

	// return the minimum of pq of PE	
	tw_stime pq_min = tw_pq_minimum(me -> pq);

	// return the minimum of outq and posted_sends of PE. Buffers for mpi.
	// populated during network_send, extracted during send_begin
	tw_stime net_min = tw_net_minimum(me);

	tw_stime lvt = me -> trans_msg_ts;
	
	if (lvt > pq_min) lvt = pq_min;
	if (lvt > net_min) lvt = net_min;
	
	if (lvt > g_tw_ts_end) return g_tw_ts_end + 2;
	else return lvt;
}

void
exchange_cm(int receive_from)
{
	_control cm;
	
	// Collective Communication
	//all_reduce_cm(&cm);

	// Faster way
	cm_send(receive_from);	
	cm_receive(&cm, receive_from);
	sync_local_cm_with_other_cm(&cm);
}

void
sync_local_cm_with_other_cm(_control * cm)
{
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++)
		{
			control_message.msg_counters[i][j] += cm -> msg_counters[i][j];
		}
	}
}

void
cm_send(int dest) // sends local cm to the next machine in the ring
{
	int rv = MPI_Send(&control_message,
		    1,
		    mpi_control_message,
		    dest, //(g_tw_pid + 1) % g_tw_machines, // Destination
		    2,
		    MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_SEND Failed");
}

void
cm_receive(_control * cm ,int source) // receives local cm of the previous machine and writes it into global cm
{
	int rv = MPI_Recv(cm, 
		    1, 
		    mpi_control_message, 
		    source, // (g_tw_pid - 1) % g_tw_machines is not working somehow
	            2, 
		    MPI_COMM_WORLD, 
		    MPI_STATUS_IGNORE);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_RECV Failed");
}

void
exchange_gvt(int receive_from)
{
	double local_gvt = min(control_message.t_min, control_message.t_red);
	
	// Collective Communication
	//all_reduce_gvt(&local_gvt);

	// Faster way
	gvt_send(&local_gvt, receive_from);
	gvt_receive(&global_gvt, receive_from);
	global_gvt = min(global_gvt, local_gvt);
}

void
gvt_send(double * gvt, int dest) 
{
	int rv = MPI_Send(gvt,
		    1,
		    MPI_DOUBLE,
		    dest, //(g_tw_pid + 1) % g_tw_machines, // Destination
		    3,
		    MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_SEND Failed");
}

void
gvt_receive(double * gvt, int source)
{
	//MPI_Status s;

	int rv = MPI_Recv(gvt, 
		    1, 
		    MPI_DOUBLE, 
		    source, // (g_tw_pid - 1) % g_tw_machines is not working somehow
	            3, 
		    MPI_COMM_WORLD, 
		    MPI_STATUS_IGNORE); //&s);

	//char str[MPI_MAX_ERROR_STRING];
	//int len;
	//MPI_Error_string(s.MPI_ERROR, str, &len);

	//if (!g_tw_pid) fprintf(stderr, "%s: %d\n", str, s.MPI_ERROR);
	//else fprintf(stderr, "	%s: %d\n", str, s.MPI_ERROR);
	
	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_RECV Failed");
}


static const tw_optdef gvt_opts [] =
{
	TWOPT_GROUP("ROSS MPI GVT"),
	TWOPT_UINT("gvt-interval", g_tw_gvt_interval, "GVT Interval"),
	TWOPT_END()
};

const tw_optdef *
tw_gvt_setup(void) 
{
	pthread_barrier_init(&barrier, NULL, no_threads - 1);
	
	#ifdef SL_BARRIER
	pthread_barrier_init(&fossil_barrier, NULL, no_threads);
	#endif
	
	return gvt_opts;
}

void
tw_gvt_force_update(tw_pe * me) 
{
	gvt_force++;
	// interval_counter = 0;
}

void tw_gvt_step2(tw_pe * me) {}

void
tw_gvt_stats(FILE * f)
{
	fprintf(f, "\nTW GVT Statistics: Mattern\n");
	fprintf(f, "\t%-50s %11d\n", "GVT Interval", g_tw_gvt_interval);
	fprintf(f, "\t%-50s %11d\n", "Batch Size", g_tw_mblock);
	fprintf(f, "\t%-50s %11d\n", "Forced GVT", gvt_force);
	fprintf(f, "\t%-50s %11d\n", "Total GVT Computations", g_tw_gvt_done);
}
