#include <ross.h>
#include <stddef.h>

#ifdef SL_BARRIER	
static pthread_barrier_t fossil_barrier;
#endif

// Statistics
static __thread unsigned int gvt_force = 0;
static __thread tw_stime measure_fc = 0;
static __thread tw_stime measure_gvt = 0;
static __thread tw_stime mpi_b = 0;

static int c1_counter; 		// Counts how many threads reached the C 1
static int c1_completed;
static int c2_counter;		// Counts how many threads reached the C 2
static int c2_completed;

static int gvt_done_counter;
static double global_gvt;

int ** accumulated_msg = NULL;
int ** recv_accumulated_msg = NULL;
static double node_gvt;

void 
tw_gvt_start() 
{
	c1_counter = 0;
	c1_completed = 0;
	c2_counter = 0;
	c2_completed = 0;
	gvt_done_counter = 0;

	accumulated_msg = (int **) calloc(sizeof(int *), no_threads);
	recv_accumulated_msg = (int **) calloc(sizeof(int *), no_threads);
	
	// Allocates non contiguous
	//for (int k = 0; k < no_threads; k++) accumulated_msg[k] = (int *) calloc(sizeof(int), g_tw_machines);

	// Allocates contiguous
	int * contiguous_block = (int *) calloc(sizeof(int), no_threads * g_tw_machines);
	for (int k = 0; k < no_threads; k++) accumulated_msg[k] = &(contiguous_block[g_tw_machines * k]);
	
	int * r_contiguous_block = (int *) calloc(sizeof(int), no_threads * g_tw_machines);
	for (int k = 0; k < no_threads; k++) recv_accumulated_msg[k] = &(r_contiguous_block[g_tw_machines * k]);
}

void
tw_gvt_step1(tw_pe * me)
{
	// C 1 is being constructed
	if (me -> color == white && !gvt_done_counter)  
	{
		#ifdef DEBUG_M
		if (!g_tw_pid) fprintf(stderr, "%u becomes red\n", g_tw_tid);
		else fprintf(stderr, "	%u becomes red\n", g_tw_tid);
		#endif

		// Each worker thread checks the C 1
		measure_gvt = tw_clock_read();
		me -> color = red;
		me -> t_red = g_tw_ts_end;			
			
		__sync_add_and_fetch(&c1_counter, 1);
	}
	else if (c1_counter == g_tw_n_threads && !g_tw_tid && !c1_completed)
	{
		#ifdef DEBUG_M
		//if (!g_tw_pid) fprintf(stderr, "%u accums: ", g_tw_tid);
		//else fprintf(stderr, "	%u accums: ", g_tw_tid);
		#endif
		// Compute accumulated message counter out of msg counter array
	
		// Thread 0 and 1 are separated
		for (int k = 0; k < no_threads; k++)
		{
			for (int i = 0; i < no_threads; i++)
			{
				for (int j = 0; j < g_tw_machines; j++)
				{
					#ifdef DEBUG_M
					//if (!g_tw_pid) fprintf(stderr, "%d ", msg_counter_array[k][i][j]);
					//else fprintf(stderr, "	%d ", msg_counter_array[k][i][j]);	
					#endif
					
					if (i != k || j != g_tw_pid)
					{
						accumulated_msg[i][j] += msg_counter_array[k][i][j];
						msg_counter_array[k][i][j] = 0;
					}					
				}
			}
		}
		#ifdef DEBUG_M
		//fprintf(stderr, "\n");
		#endif
		
		// exchange accumulated msg counter
		// turn c1 completed flag on
		if (!me -> id) exchange_msg(1);
		else exchange_msg(0);

		c1_completed = 1;
	}
	else if (c1_completed && g_tw_tid && !me -> c2_checked && me -> color == red)
	{
		#ifdef DEBUG_M
		//if (!g_tw_pid) fprintf(stderr, "%u received %d, accumulated %d\n", g_tw_tid,
		//		me -> msg_counters[g_tw_tid][g_tw_pid],
		//		accumulated_msg[g_tw_tid][g_tw_pid]);
		
		//else fprintf(stderr, "	%u received %d, accumulated %d\n", g_tw_tid,
		//		me -> msg_counters[g_tw_tid][g_tw_pid],
		//		accumulated_msg[g_tw_tid][g_tw_pid]);
		#endif

		if (me -> msg_counters[g_tw_tid][g_tw_pid] + accumulated_msg[g_tw_tid][g_tw_pid] == 0)
		{
			// update gvt array with local lvt
			me -> msg_counters[g_tw_tid][g_tw_pid] = 0;
			me -> c2_checked = 1; 
		
			me -> t_min = min_pq_outq(me);
			// me -> t_red will be updated after here also. Might be a problem 
				
			__sync_add_and_fetch(&c2_counter, 1);
		}
	}
	else if (c2_counter == g_tw_n_threads - 1 && !g_tw_tid && !c2_completed)
	{	
		#ifdef DEBUG_M
		//if (!g_tw_pid) fprintf(stderr, "%u gvt\n", g_tw_tid);
		//else fprintf(stderr, "	%u gvt\n", g_tw_tid);
		#endif

		// compute gvt out of lvt arrays
		double min = DBL_MAX;

		
		for (int i = 0; i < no_threads; i++)
		{
			double t_min = *t_min_list[i];
			double t_red = *t_red_list[i];

			if (t_min == 0 || t_red == 0) continue;

			if (t_min < min) min = t_min;
			if (t_red < min) min = t_red;	
		}

		node_gvt = min;		
	
		if (!me -> id) exchange_gvt(1);
		else exchange_gvt(0);
		
		c2_completed = 1;
	}
	else if (me -> color == red && c2_completed) 
	{
		#ifdef DEBUG_M
		if (!g_tw_pid) fprintf(stderr, "%u finishes (%.0f)\n", g_tw_tid, global_gvt);
		else fprintf(stderr, "	%u finishes (%.0f)\n", g_tw_tid, global_gvt);
		#endif

		fossil_collect(me);
		finish_gvt(me);
	
		if (!(me -> id) && me -> GVT / g_tw_ts_end > percent_complete) gvt_print(me -> GVT);
			
		if (__sync_add_and_fetch(&gvt_done_counter, 1) == g_tw_n_threads)
		{
			interval_counter = g_tw_gvt_interval;
			for (int i = 0; i < g_tw_n_threads; i++) for (int j = 0; j < g_tw_machines; j++) accumulated_msg[i][j] = 0;

			c1_completed = 0;
			c2_completed = 0;
			c1_counter = 0;
			c2_counter = 0;			
			gvt_done_counter = 0;
		} 
	}
}

void
exchange_gvt(int receive_from)
{
	if (receive_from)
	{
		gvt_send();
		gvt_receive(1);
		global_gvt = min(node_gvt, global_gvt);
	}
	else
	{
		gvt_send();
		gvt_receive(0);
		global_gvt = min(node_gvt, global_gvt);
	}
}

void
gvt_send() 
{
	int rv = MPI_Send(&node_gvt,
		    1,
		    MPI_DOUBLE,
		    (g_tw_pid + 1) % g_tw_machines, // Destination
		    3,
		    MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_SEND Failed");
}

void
gvt_receive(int source)
{
	int rv = MPI_Recv(&global_gvt, 
		    1, 
		    MPI_DOUBLE, 
		    source, // (g_tw_pid - 1) % g_tw_machines is not working somehow
	            3, 
		    MPI_COMM_WORLD, 
		    MPI_STATUS_IGNORE); //&s);
	
	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_RECV Failed");
}

void
exchange_msg(int receive_from)
{
	if (receive_from)
	{
		msg_send();
		msg_receive(1);
		sync_msg_with_received_msg();
	}
	else
	{
		msg_send();
		msg_receive(0);
		sync_msg_with_received_msg();
	}
}

void
msg_send() 
{
	int rv = MPI_Send(&(accumulated_msg[0][0]),
		    no_threads * g_tw_machines,
		    MPI_INT,
		    (g_tw_pid + 1) % g_tw_machines, // Destination
		    2,
		    MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_SEND Failed");
}

void
msg_receive(int source)
{
	int rv = MPI_Recv(&(recv_accumulated_msg[0][0]), 
		    no_threads * g_tw_machines, 
		    MPI_INT, 
		    source, // (g_tw_pid - 1) % g_tw_machines is not working somehow
	            2, 
		    MPI_COMM_WORLD, 
		    MPI_STATUS_IGNORE);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_RECV Failed");
}

void
sync_msg_with_received_msg()
{
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++)
		{
			accumulated_msg[i][j] += recv_accumulated_msg[i][j];
		}
	}
}

void 
fossil_collect(tw_pe * me)
{
	me -> GVT = global_gvt;
	
	measure_fc = tw_clock_read();

	#ifdef SL_LOCKS
		//if (!g_tw_pid) fprintf(stderr, "Node %d enter, GVT = %lf\n", g_tw_mynode, gvt);
		//else fprintf(stderr, "	Node %d enter, GVT = %lf\n", g_tw_mynode, gvt);
		if (g_tw_tid == 1) tw_pe_fossil_collect(me);
	#elif defined(SL_BARRIER)
		pthread_barrier_wait(&fossil_barrier);
		//if (g_tw_tid == 1)  tw_pe_fossil_collect(me);
		if (!g_tw_tid)  tw_pe_fossil_collect(me);
		pthread_barrier_wait(&fossil_barrier);
	#elif defined(SEPARATE_1)
		if (g_tw_tid > 1) tw_pe_fossil_collect(me);
	#elif defined(SEPARATE_0)
		if (g_tw_tid) tw_pe_fossil_collect(me);
	#else
		tw_pe_fossil_collect(me);
	#endif
	
	me -> stats.s_fossil_collect += tw_clock_read() - measure_fc;
}

void 
finish_gvt(tw_pe * me)
{
	g_tw_gvt_done++;
	
	me -> trans_msg_ts = DBL_MAX + 3;
	me -> c2_checked = 0;
	me -> color = white;
	me -> stats.s_gvt += tw_clock_read() - measure_gvt;
}


tw_stime
min_pq_outq(tw_pe * me)
{
	// tw_net_read(me); // might be neccessary

	// return the minimum of pq of PE	
	tw_stime pq_min = tw_pq_minimum(me -> pq);

	// return the minimum of outq and posted_sends of PE. Buffers for mpi.
	// populated during network_send, extracted during send_begin
	tw_stime net_min = tw_net_minimum(me);

	tw_stime lvt = me -> trans_msg_ts;
	
	if (lvt > pq_min) lvt = pq_min;
	if (lvt > net_min) lvt = net_min;
	
	if (lvt > g_tw_ts_end) return g_tw_ts_end + 2;
	else return lvt;
}

static const tw_optdef gvt_opts [] =
{
	TWOPT_GROUP("ROSS MPI GVT"),
	TWOPT_UINT("gvt-interval", g_tw_gvt_interval, "GVT Interval"),
	TWOPT_END()
};

const tw_optdef *
tw_gvt_setup(void) 
{
	#ifdef SL_BARRIER
	pthread_barrier_init(&fossil_barrier, NULL, no_threads);
	#endif
	
	return gvt_opts;
}

void
tw_gvt_force_update(tw_pe * me) 
{
	gvt_force++;
	// interval_counter = 0;
}

void tw_gvt_step2(tw_pe * me) {}

void
tw_gvt_stats(FILE * f)
{
	fprintf(f, "\nTW GVT Statistics: Mattern\n");
	fprintf(f, "\t%-50s %11d\n", "GVT Interval", g_tw_gvt_interval);
	fprintf(f, "\t%-50s %11d\n", "Batch Size", g_tw_mblock);
	fprintf(f, "\t%-50s %11d\n", "Forced GVT", gvt_force);
	fprintf(f, "\t%-50s %11d\n", "Total GVT Computations", g_tw_gvt_done);
}
