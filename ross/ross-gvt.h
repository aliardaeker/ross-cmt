#ifndef INC_ross_gvt_h
#define INC_ross_gvt_h

/*
 * Initialize the GVT library and parse options.
 */

/* setup the GVT library (config cmd line args, etc) */
extern const tw_optdef *tw_gvt_setup(void);

/* start the GVT library (init vars, etc) */
extern void tw_gvt_start(void);

/* 
 * GVT computation is broken into two stages:
 * stage 1: determine if GVT computation should be started
 * stage 2: compute GVT
 */
extern void tw_gvt_step1(tw_pe *);
extern void tw_gvt_step2(tw_pe *);

/*
 * Provide a mechanism to force a GVT computation outside of the 
 * GVT interval (optional)
 */
extern void tw_gvt_force_update(tw_pe *);

/* Set the PE GVT value */
extern int tw_gvt_set(tw_pe * pe, tw_stime LVT);

/* Returns true if GVT in progress, false otherwise */
static inline int  tw_gvt_inprogress(tw_pe * pe);

/* Statistics collection and printing function */
extern void tw_gvt_stats(FILE * F);

extern void send_white_count(int dest);
extern void recv_white_count(int source);

extern void update_control();

extern void send_gvt();
extern void recv_gvt();

extern tw_stime min_pq_outq(tw_pe * me);
extern void accumulate_msg_counters(tw_pe * me);

extern void update_cm(tw_pe * me);
extern void cm_send();
extern void cm_receive();

extern void mpi_func();
extern void all_reduce_gvt();
extern void all_reduce_cm();

extern void finish_gvt(tw_pe * me);
extern void fossil_collect(tw_pe * me);
extern void reset_control_message();

extern void sync_local_cm_with_other_cm();
extern void reset_local_cm_with_other_cm();
extern void print_local_cm();

extern void exchange_gvt();
extern void exchange_cm();

extern void sync_msg_with_received_msg();
extern void exchange_msg();
extern void msg_send();
extern void msg_receive();

extern void exchange_gvt();
extern void gvt_send();
extern void gvt_receive();

extern void compute_eff();
extern void synchronize();
extern void allreduce_barrier_flag();
extern void sync_less();

extern void report_lvts();
extern void mark_faster_lps();

extern void first_cut();
extern void second_cut();

extern void gvt_info();
#endif
