#include <ross.h>
#include <mpi.h>

#define MAX_NODES 256

struct act_q
{
	const char * name;

  	tw_event	** event_list;
  	MPI_Request	 * req_list;
  	int		 * idx_list;
	
	MPI_Status	 *status_list;
	unsigned int cur;
};

#define EVENT_TAG 1

#define EVENT_SIZE(e) (g_tw_event_msg_sz)

__thread int mycount=0;
static __thread struct act_q posted_sends;
static __thread struct act_q posted_recvs;

// For MPI Send
static tw_eventq outq[Thread_Number];
static pthread_mutex_t outq_mutex[Thread_Number];

static __thread int current_token = -1;

// For mt_receive
static __thread tw_eventq inq[MAX_NUMA_NODES];
 
typedef struct 
{
        tw_eventq * outq_ptr; // Pointer to thread specific output queue, not output
			      // This points to the thread specific inq location so that when this is
			      // written, inq is also updated.
        pthread_mutex_t mutex;
} g_mt_outq;

// For mt_send
static g_mt_outq mt_outq_ptr[MAX_NODES][MAX_NUMA_NODES];

static unsigned int send_buffer = 50000;
static unsigned int read_buffer = 50000;

//static unsigned int send_buffer = 10;
//static unsigned int read_buffer = 10;

//static unsigned int send_buffer = 1;
//static unsigned int read_buffer = 1;

static __thread int world_size = 1;
static __thread int numa_node_id;

static const tw_optdef mpi_opts[] = 
{
	TWOPT_GROUP("ROSS MPI Kernel"),
	TWOPT_UINT(
	     "read-buffer",
	     read_buffer,
	     "network read buffer size in # of events"),
  	TWOPT_UINT(
	     "send-buffer",
	     send_buffer,
	     "network send buffer size in # of events"),
	TWOPT_END()
};

void init_mt_ouq_ptr(int thread_id)
{
	int i, j;
	i = j = 0;
	
	for(i = 0; i < MAX_NUMA_NODES; i++)
	{
		inq[i].size = 0;
		inq[i].head = inq[i].tail = NULL;
		pthread_mutex_init(&(mt_outq_ptr[thread_id][i].mutex), NULL);
		
		mt_outq_ptr[thread_id][i].outq_ptr = &inq[i];
	}
	
	// Outq mutex is initialized by every thread, BUG
 	for(i = 0; i < g_tw_n_threads; i++) pthread_mutex_init(&outq_mutex[i], NULL);	
}

void destroy_mt_ouq_ptr()
{
	int i;
	
	for(i = 0; i < MAX_NUMA_NODES; i++) pthread_mutex_destroy(&(mt_outq_ptr[g_tw_tid][i].mutex));
	
	for(i = 0; i < g_tw_n_threads; i++) pthread_mutex_destroy(&outq_mutex[i]);
}

void mt_mpi_init(int *argc, char ***argv)
{
	int provided;

  	if (MPI_Init_thread(argc, argv, MPI_THREAD_FUNNELED, &provided) != MPI_SUCCESS) tw_error(TW_LOC, "MPI_Init failed.");
  	//if (MPI_Init_thread(argc, argv, MPI_THREAD_SERIALIZED, &provided) != MPI_SUCCESS) tw_error(TW_LOC, "MPI_Init failed.");
  	//if (MPI_Init_thread(argc, argv, MPI_THREAD_MULTIPLE, &provided) != MPI_SUCCESS) tw_error(TW_LOC, "MPI_Init failed.");
}

const tw_optdef *
tw_net_init(int *argc, char ***argv,int thread_id)
{
	int my_rank;

	if (MPI_Comm_rank(MPI_COMM_WORLD, &my_rank) != MPI_SUCCESS) tw_error(TW_LOC, "Cannot get MPI_Comm_rank(MPI_COMM_WORLD)");

 	g_tw_masternode = 0;
  	g_tw_mynode = (my_rank * g_tw_n_threads) + thread_id;
 
 	g_tw_pid = my_rank; // 2 Computers: 0, 1
	numa_node_id = g_tw_pid;
  	
	g_tw_tid = thread_id; // 4 Threads in each node: 0, 1, 2, 3
  	//printf("Thread_id = %d, Numa_node_id = %d\n", thread_id, numa_node_id);

	init_mt_ouq_ptr(thread_id);
  	return mpi_opts;
}

static void
init_q(struct act_q *q, const char *name)
{
	unsigned int n;
	
	if (q == &posted_sends) n = send_buffer;
  	else n = read_buffer;

  	q -> name = name;
  	q -> event_list = tw_calloc(TW_LOC, name, sizeof(*q->event_list), n);
  	q -> req_list = tw_calloc(TW_LOC, name, sizeof(*q->req_list), n);
  	q -> idx_list = tw_calloc(TW_LOC, name, sizeof(*q->idx_list), n);

	q -> status_list = tw_calloc(TW_LOC, name, sizeof(*q->status_list), n);
}

unsigned int
tw_nnodes(void)
{
	return world_size;
}

void
tw_net_start(void)
{
  	if (MPI_Comm_size(MPI_COMM_WORLD, &world_size) != MPI_SUCCESS) tw_error(TW_LOC, "Cannot get MPI_Comm_size(MPI_COMM_WORLD)");
    
	world_size = world_size * no_threads; // world_size is total number of threads in global
  	if (g_tw_mynode == 0) printf("\nFound world size to be %d \n", world_size);

  	tw_pe_create(1);
  	tw_pe_init(0, g_tw_mynode);
  	g_tw_pe[0] -> hash_t = tw_hash_create();

  	if (send_buffer < 1) tw_error(TW_LOC, "network send buffer must be >= 1");
  	if (read_buffer < 1) tw_error(TW_LOC, "network read buffer must be >= 1");

  	init_q(&posted_sends, "MPI send queue");
  	init_q(&posted_recvs, "MPI recv queue");
  	g_tw_net_device_size = read_buffer;
}

void
tw_net_abort(void)
{
	MPI_Abort(MPI_COMM_WORLD, 1);
	exit(1);
}

void
tw_net_stop(void)
{
	destroy_mt_ouq_ptr();
}

void
tw_net_barrier(tw_pe * pe)
{
	barrier_sync(&g_barrier,g_tw_tid);

	if(g_tw_tid == 0)
	{
  		if (MPI_Barrier(MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Failed to wait for MPI_Barrier");
		//arg1: barrier struct pointer arg2: thread_id
	}

	barrier_sync(&g_barrier,g_tw_tid);
}

tw_stime
tw_net_minimum(tw_pe *me)
{
	tw_stime m = DBL_MAX;
	tw_event *e;
	int i;

	pthread_mutex_lock(&outq_mutex[g_tw_tid]);
  	e = outq[g_tw_tid].head;
	pthread_mutex_unlock(&outq_mutex[g_tw_tid]);

  	while (e) 
	{
    		if (m > e -> recv_ts) m = e->recv_ts;
    		e = e -> next;
  	}

	for (i = 0; i < posted_sends.cur; i++) 
	{
    		e = posted_sends.event_list[i];
    		if (m > e -> recv_ts) m = e->recv_ts;
  	}
	
  	return m;
}

static int
test_q(
       struct act_q * q, // thread spesific posted_sends / posted_receives
       tw_pe *me,
       void (*finish)(tw_pe *, tw_event *, char *)) // net_send_finish / net_receive_finish
{
	int ready, i, n;

	#ifdef blocking_MPI
		ready = 1;
	#endif
	
	// Posted receives is empty
	if (!q -> cur) return 0;
	
	// Tests for given requests to complete
	// q -> cur is length of requests, ready is number of completed requests
	
	#ifndef blocking_MPI	
	if (MPI_Testsome(
		   q->cur,
		   q->req_list,
		   &ready,
		   q->idx_list,
		   q->status_list) != MPI_SUCCESS) tw_error(TW_LOC, "MPI_testsome failed with %u items in %s", q->cur, q->name);
	#endif

	// Nothing is ready
  	if (1 > ready) return 0;

  	for (i = 0; i < ready; i++)
    	{
      		tw_event *e;
      		n = q -> idx_list[i];	
		e = q -> event_list[n];
      		q -> event_list[n] = NULL;

      		finish(me, e, NULL);
    	}

  	/* Collapse the lists to remove any holes we left. */
  	for (i = 0, n = 0; i < q -> cur; i++) 
	{
    		if (q->event_list[i]) 
		{
      			if (i != n) 
			{
				// swap the event pointers
				q->event_list[n] = q->event_list[i];
				// copy the request handles
				memcpy(&q->req_list[n], &q->req_list[i], sizeof(q->req_list[0]));

	  		}
      			n++;
    		}
  	}

  	q->cur -= ready;
 	return 1;
}

static int
recv_begin(tw_pe *me)
{
	MPI_Status status;

	// jwang
	// tw_event e ;
  	int flag = 0;
  	int changed = 0;
  
  	while (posted_recvs.cur < read_buffer)
  	{
      		unsigned id = posted_recvs.cur;
  		tw_event	*e = NULL;

      		MPI_Iprobe(MPI_ANY_SOURCE,
			EVENT_TAG,
		 	MPI_COMM_WORLD,
		 	&flag,
		 	&status);
  
		if (flag)
		{
				e = malloc(EVENT_SIZE(e));
				e -> cancel_next = NULL;
				e -> caused_by_me = NULL;
				e -> cause_next = NULL;
				e -> prev = e->next = NULL;
				memset(&e->state, 0, sizeof(e->state));
				memset(&e->event_id, 0, sizeof(e->event_id));
		} 
		else return changed;
	
		/*
 		*
 		*
 		* 	Receiving remote
 		*
 		*
 		*/
		#ifdef blocking_MPI
			int rv = MPI_Recv(e,
			     		EVENT_SIZE(e),
			     		MPI_BYTE,
			     		MPI_ANY_SOURCE,
			     		EVENT_TAG,
			     		MPI_COMM_WORLD,
					MPI_STATUS_IGNORE);		

			if (rv != MPI_SUCCESS) tw_error(TW_LOC, "Error: MPI Recv");				
		#else		
			int rv = MPI_Irecv(e,
		     		EVENT_SIZE(e),
		     		MPI_BYTE,
		     		MPI_ANY_SOURCE,
		     		EVENT_TAG,
		     		MPI_COMM_WORLD,
			     	&posted_recvs.req_list[id]);
	
			if (rv != MPI_SUCCESS) tw_error(TW_LOC, "Error: MPI Recv");				
		#endif	

		posted_recvs.event_list[id] = e;
      		posted_recvs.cur++;
      		changed = 1;
    	}
   
	return changed;
}

void
recv_finish(tw_pe *me, tw_event *e, char * buffer)
{
	tw_pe		*dest_pe;
	
	me -> s_nwhite_recv++;	
	
	#ifdef ROSS_GVT_mpi_allreduce	
		#ifdef DEBUG_ALI
			if (!g_tw_pid)
			{
				fprintf(stderr, "%u receives from %lld: %lld\n", 
					g_tw_tid, e -> send_pe, me -> s_nwhite_recv);
					// e -> event_id, e -> recv_ts);
			}
			else
			{
				fprintf(stderr, "	%u receives from %lld: %lld\n", 
					g_tw_tid, e -> send_pe, me -> s_nwhite_recv); 
					// e -> event_id, e -> recv_ts);
			}
		#endif
	#endif

	e -> dest_lp = tw_getlocal_lp((tw_lpid) e->dest_lp);
	dest_pe = e -> dest_lp -> pe;

  	if (e -> send_pe > tw_nnodes() - 1) tw_error(TW_LOC, "bad sendpe_id: %d", e->send_pe);

  	e -> cancel_next = NULL;
  	e -> caused_by_me = NULL;
  	e -> cause_next = NULL;

  	if (e -> recv_ts < me -> GVT) 
	{
		#ifdef ROSS_GVT_mattern_orig
			tw_error(TW_LOC, 
			"\n\n%d received straggler from %d\nRecv_ts: %lf (%d), gvt: (%f)\nE id: %d, E color: %d, me color: %d\n", 
			me -> id,  e -> send_pe, e -> recv_ts, e -> state.cancel_q, 
			me -> GVT, e -> event_id, e -> color, me -> color);
		#else	
			tw_error(TW_LOC, 
			"\n\n%d received straggler from %d\nRecv_ts: %lf (%d), gvt: (%f)\nE id: %d\n", 
			me -> id,  e -> send_pe, e -> recv_ts, e -> state.cancel_q, 
			me -> GVT, e -> event_id);
		#endif
	}

  	if (tw_gvt_inprogress(me)) me->trans_msg_ts = min(me->trans_msg_ts, e->recv_ts);

	// if cancel event, retrieve and flush
  	// else, store in hash table
  	if (e -> state.cancel_q)
    	{
		//DJ: find an event in the hash table for which this cancel event is made 
		tw_event *cancel = tw_hash_remove(me->hash_t, e, e->send_pe);

      		// NOTE: it is possible to cancel the event we
      		// are currently processing at this PE since this
      		// MPI module lets me read cancel events during 
      		// event sends over the network.

      		cancel->state.cancel_q = 1;
      		cancel->state.remote = 0;
		//Put orig event in the cancelq
      		cancel->cancel_next = dest_pe->cancel_q;
      		dest_pe->cancel_q = cancel;

		//DJ:cancel event done it's job now we can free it 
		tw_event_free(me, e);
      
      		return;
    	}

	tw_hash_insert(me->hash_t, e, e->send_pe);
  	e -> state.remote = 1;

  	if(me == dest_pe && e->dest_lp->kp->last_time <= e->recv_ts) 
	{
    		/* Fast case, we are sending to our own PE and
     		* there is no rollback caused by this send.
     		*/
    		tw_pq_enqueue(dest_pe->pq, e);
    		return;
  	}
  
  	if(tw_node_eq(&me->node, &dest_pe->node)) 
	{
    		/* Slower, but still local send, so put into top
     		* of dest_pe->event_q. 
     		*/
    		e->state.owner = TW_pe_event_q;
    		tw_eventq_push(&dest_pe->event_q, e);
    		return;
  	}

  	/* Never should happen; MPI should have gotten the
  	 * message to the correct node without needing us
   	 * to redirect the message there for it.  This is
   	 * probably a serious bug with the event headers
   	 * not being formatted right.
   	 */
  	
	tw_error(
	   TW_LOC,
	   "Event recived by PE %u but meant for PE %u",
	   me->id,
	   dest_pe->id);
}

static int
send_begin(tw_pe *me)
{
	int changed = 0;

	while (posted_sends.cur < send_buffer)
   	{
		pthread_mutex_lock(&outq_mutex[current_token]);
      		tw_event *e = tw_eventq_peek(&outq[current_token]);
		pthread_mutex_unlock(&outq_mutex[current_token]);

		if (!e) break;

		//printf message 
   		me->stats.s_message_number++,
		me->stats.s_event_number++;

      		unsigned id = posted_sends.cur;
	
		/*
 		*
 		*	Sending Remote
 		*
 		*	Important: although it looks like sender is e -> send_pe, 
 		*	actual sender is g_tw_tid == 0. This is not important at the
 		*	receiving side since we just count regional receives. Because 
 		*	every remote receive will execute regional receive as well but
 		*	sends are either regional or remote.
 		*/

		#ifdef ROSS_GVT_mattern_orig
			int _thread_id = (e -> recv_pe) % g_tw_n_threads;
			int _machine_id = (e -> recv_pe) / g_tw_n_threads;

			if (me -> color == white) 
			//if (e -> src_lp_ptr -> pe -> color == white)
			{
				e -> color = white;
				me -> msg_counters[_thread_id][_machine_id]++;			
				//e -> src_lp_ptr -> pe -> msg_counters[_thread_id][_machine_id]++;			

				#ifdef DEBUG_ALI
				if (!g_tw_pid)
				{
					fprintf(stderr, "%lld sends remote white to %d: %d\n", 
						e -> send_pe, e -> recv_pe, me -> msg_counters[_thread_id][_machine_id]);
						// e -> event_id, e -> recv_ts);
				}
				else
				{
					fprintf(stderr, "	%lld sends remote white to %d: %d\n", 
						e -> send_pe, e -> recv_pe, me -> msg_counters[_thread_id][_machine_id]);
						// e -> event_id, e -> recv_ts);
				}
				#endif
			}
			else if (me -> color == red)
			//if (e -> src_lp_ptr -> pe -> color == red)
			{
				e -> color = red;
				me -> t_red = min(me -> t_red, e -> recv_ts);
				//e -> src_lp_ptr -> pe -> t_red = min(me -> t_red, e -> recv_ts);
			
				#ifdef DEBUG_ALI
				if (!g_tw_pid)
				{
					fprintf(stderr, "%lld sends remote red to %d: %d\n", 
						e -> send_pe, e -> recv_pe, me -> msg_counters[_thread_id][_machine_id]);
						// e -> event_id, e -> recv_ts);
				}
				else
				{
					fprintf(stderr, "	%lld sends remote red to %d: %d\n", 
						e -> send_pe, e -> recv_pe, me -> msg_counters[_thread_id][_machine_id]);
						// e -> event_id, e -> recv_ts);
				}
				#endif
			}
			else tw_error(TW_LOC, "Error in mattern orig send"); 	
		#endif

		#ifdef blocking_MPI	
			int rv = MPI_Send(e,
                   		EVENT_SIZE(e),
                    		MPI_BYTE,
                    		(e -> recv_pe) / g_tw_n_threads,
                    		EVENT_TAG,
                    		MPI_COMM_WORLD);
		
			if (rv != MPI_SUCCESS) tw_error(TW_LOC, "Error in MPI_Send"); 	
		#else
			int rv = MPI_Isend(e,
                	   	EVENT_SIZE(e),
                	    	MPI_BYTE,
                	   	(e -> recv_pe) / g_tw_n_threads,
                	    	EVENT_TAG,
               		     	MPI_COMM_WORLD,
               		     	&posted_sends.req_list[id]);

			if (rv != MPI_SUCCESS) tw_error(TW_LOC, "Error in MPI_Isend");
		#endif

		pthread_mutex_lock(&outq_mutex[current_token]);
      		tw_eventq_pop(&outq[current_token]);
		pthread_mutex_unlock(&outq_mutex[current_token]);
		
      		posted_sends.event_list[id] = e;      
		posted_sends.cur++;
      		
		me->s_nwhite_sent++;
		
		#ifdef ROSS_GVT_mpi_allreduce		
			#ifdef DEBUG_ALI
				if (!g_tw_pid)
				{
					fprintf(stderr, "%lld sends remote to %d: %lld\n", 
						e -> send_pe, e -> recv_pe, me -> s_nwhite_sent);
						// e -> event_id, e -> recv_ts);
				}
				else
				{
					fprintf(stderr, "	%lld sends remote to %d: %lld\n", 
						e -> send_pe, e -> recv_pe, me -> s_nwhite_sent); 
						// e -> event_id, e -> recv_ts);
				}
			#endif
		#endif
      		
		changed = 1;
    	}

  	return changed;
}

static void
send_finish(tw_pe *me, tw_event *e, char * buffer)
{
	if (e -> state.owner == TW_net_asend) 
	{
    		if (e->state.cancel_asend) 
		{
      			/* Event was cancelled during transmission.  We must
       			* send another message to pass the cancel flag to
       			* the other node.
       			*/
		
			e -> state.cancel_asend = 0;
      			e -> state.cancel_q = 1;
		
			pthread_mutex_lock(&outq_mutex[g_tw_tid]);
      			tw_eventq_push(&outq[g_tw_tid], e);				
			pthread_mutex_unlock(&outq_mutex[g_tw_tid]);
    		} 
		else 
		{
      			/* Event finished transmission and was not cancelled.
       			* Add to our sent event queue so we can retain the
       			* event in case we need to cancel it later.  Note it
       			* is currently in remote format and must be converted
       			* back to local format for fossil collection.
       			*/
		
      			e->state.owner = TW_pe_sevent_q;
			if (g_tw_synchronization_protocol == CONSERVATIVE) tw_event_free(me, e);
    		}

    		return;
	}

	if (e -> state.owner == TW_net_acancel) 
	{
		/* We just finished sending the cancellation message
     		* for this event.  We need to free the buffer and
     		* make it available for reuse.
     		*/
		tw_event_free(me, e);
   		return;
  	}

  	/* Never should happen, not unless we somehow broke this
   	* module's other functions related to sending an event.
   	*/
  	tw_error(
	  	TW_LOC,
	   	"Don't know how to finish send of owner=%u, cancel_q=%d",
	   	e->state.owner,
	   	e->state.cancel_q);
}

void net_send_finish(tw_pe *me, tw_event *e, char * buffer)
{
	#ifdef SL_LOCKS 
  		e -> state.owner = TW_pe_free_q;
	#else
		tw_event_free(me,e);
	#endif
}

//Moves events from outq to appropriate PE queue
//This will involve invoking receive_finish for each event in the outq
static void mt_receive(tw_pe * me)
{
	int status,i;
	
	// fprintf(stderr, "%d\n", g_tw_machines); -> 2
	for (i = 0; i < g_tw_machines; i++)
	{
		while (inq[i].size > 0)
		{
			status = pthread_mutex_lock(&(mt_outq_ptr[g_tw_tid][i].mutex));
			if(status!=0) printf("In mt_receive: Lock failed error-code=%d\n",status);	
			tw_event *e = tw_eventq_pop(&inq[i]);

			if (e == NULL)
			{ 
				status = pthread_mutex_unlock(&(mt_outq_ptr[g_tw_tid][i].mutex));	
				continue;
			}

			recv_finish(me,e,NULL);
			
			status = pthread_mutex_unlock(&(mt_outq_ptr[g_tw_tid][i].mutex));	
			if(status!=0) printf("In mt_receive: UnLock failed error-code=%d\n",status);

			/*
 			*
 			*
 			*	Receiving Regional
 			*
			*
 			*/
			me -> stats.s_nread_network++;
			#ifdef ROSS_GVT_mattern_orig
				if (e -> color == white) 
				{
					me -> msg_counters[g_tw_tid][g_tw_pid]--;	
					
					#ifdef DEBUG_ALI
					if (!g_tw_pid)
					{
						fprintf(stderr, "%u receives white from %lld: %d (%d)\n", 
							g_tw_tid, e -> send_pe, me -> msg_counters[g_tw_tid][g_tw_pid],
							e -> event_id);
					}
					else
					{
						fprintf(stderr, "	%u receives white from %lld: %d (%d)\n", 
							g_tw_tid, e -> send_pe, me -> msg_counters[g_tw_tid][g_tw_pid],
							e -> event_id);
					}
					#endif
				}
				else if (e -> color == red) 
				{
					#ifdef DEBUG_ALI
					if (g_tw_pid)
					{
						//fprintf(stderr, "	%llu receives red from %lld: %d - %.0f\n", 
						//	me -> id, e -> send_pe, e -> event_id, e -> recv_ts);
					}
					else
					{
						//fprintf(stderr, "%llu receives red from %lld: %d - %.0f\n", 
						//	me -> id, e -> send_pe, e -> event_id, e -> recv_ts);
					}
					#endif
				}
				//else tw_error(TW_LOC, "Error: mattern orig receive");				
			#endif
		}
	}
}

void net_recv_finish(tw_pe *me, tw_event *e, char * buffer)
{
	int status;
	
	status = pthread_mutex_lock(&(mt_outq_ptr[((e->recv_pe)%g_tw_n_threads)][numa_node_id].mutex));
	if (status) printf("Error\n");
     	
	tw_eventq_unshift(mt_outq_ptr[((e -> recv_pe) % g_tw_n_threads)][numa_node_id].outq_ptr, e);  
	
	status = pthread_mutex_unlock(&(mt_outq_ptr[((e->recv_pe)%g_tw_n_threads)][numa_node_id].mutex));
	if (status) printf("Error\n");
}

static void mt_send(tw_pe *me,tw_event *e, int dest_node_id)
{
	int status;
	tw_event * cp_e = NULL;
	
	if (!e -> state.cancel_q) e -> event_id = (tw_eventid) ++me->seq_num;

	e -> send_pe = (tw_peid) g_tw_mynode;
	e -> state.owner = e->state.cancel_q ? TW_net_acancel : TW_net_asend;
	
	
	cp_e = mt_tw_event_new(dest_node_id, e->recv_ts, e->src_lp); //dummy event parameters doesn't have any significance
	memcpy(cp_e, e, g_tw_event_msg_sz); //make cp_e identical to e

	/*
 	*
	* 
	*	 Sending Regional
 	*
 	*
 	*/
	#ifdef ROSS_GVT_mattern_orig
		if (me -> color == white) 
		{
			me -> msg_counters[dest_node_id][numa_node_id]++;
			cp_e -> color = white;
			
			#ifdef DEBUG_ALI
			if (!g_tw_pid)
			{
				fprintf(stderr, "%d sends white to %d: %d\n", 
					g_tw_tid, dest_node_id, //e -> event_id);
					me -> msg_counters[dest_node_id][numa_node_id]);
			}
			else
			{
				fprintf(stderr, "	%d sends white to %d: %d\n", 
					g_tw_tid, dest_node_id, //e -> event_id);
					me -> msg_counters[dest_node_id][numa_node_id]);
			}
			#endif
		}
		else if (me -> color == red)
		{
			#ifdef DEBUG_ALI
			if (!g_tw_pid)
			{
				fprintf(stderr, "%d sends red to %d: %d\n", 
					g_tw_tid, dest_node_id, //e -> event_id);
					me -> msg_counters[dest_node_id][numa_node_id]);
			}
			else
			{
				fprintf(stderr, "	%d sends red to %d: %d\n", 
					g_tw_tid, dest_node_id, //e -> event_id);
					me -> msg_counters[dest_node_id][numa_node_id]);
			}
			#endif
			
			cp_e -> color = red;
			me -> t_red = min(me -> t_red, e -> recv_ts);
		}
		else tw_error(TW_LOC, "Error in mattern orig send"); 	
	#endif

	status = pthread_mutex_lock(&(mt_outq_ptr[dest_node_id][numa_node_id].mutex));	
	if (status!=0) printf("mt_send lock failed! error-code=%d\n",status);

	// dest_node_id: thread id in a machine, numa_node_id: machine rank
	tw_eventq_unshift(mt_outq_ptr[dest_node_id][numa_node_id].outq_ptr, cp_e);
	
	status = pthread_mutex_unlock(&(mt_outq_ptr[dest_node_id][numa_node_id].mutex));
	if (status!=0) printf("mt_send unlock failed! error-code=%d\n",status);

	me -> s_nwhite_sent++;
		
	#ifdef ROSS_GVT_mpi_allreduce
		#ifdef DEBUG_ALI
			if (!g_tw_pid)
			{
				fprintf(stderr, "%d sends regional to %d: %lld\n", 
					g_tw_tid, dest_node_id, me -> s_nwhite_sent);
					// e -> event_id, e -> recv_ts);
			}
			else
			{
				fprintf(stderr, "	%d sends regional to %d: %lld\n", 
					g_tw_tid, dest_node_id, me -> s_nwhite_sent); 
					// e -> event_id, e -> recv_ts);
			}
		#endif
	#endif

	if (e->state.owner == TW_net_asend) 
	{
		/* Event finished transmission and was not cancelled.
		 * Add to our sent event queue so we can retain the
		 * event in case we need to cancel it later.  Note it
		 * is currently in remote format and must be converted
		 * back to local format for fossil collection.
		 */
		e->state.owner = TW_pe_sevent_q;
		
		if (g_tw_synchronization_protocol == CONSERVATIVE) tw_event_free(me, e);
		return;
	}

	if (e->state.owner == TW_net_acancel) 
	{
		/* We just finished sending the cancellation message
		 * for this event.  We need to free the buffer and
		 * make it available for reuse.
		 */
		
		//DJ: It means this is a cancelation message we can free this event immidiately  
		tw_event_free(me, e);
		return;
	}

  	/* Never should happen, not unless we somehow broke this
   	* module's other functions related to sending an event.
   	*/

  	tw_error(
           	TW_LOC,
           	"Don't know how to finish send of owner=%u, cancel_q=%d",
           	e->state.owner,
           	e->state.cancel_q);
}

/*
 * NOTE: Chris believes that this network layer is too aggressive at
 * reading events out of the network.. so we are modifying the algorithm
 * to only send events when tw_net_send it called, and only read events
 * when tw_net_read is called.
 */
void
tw_net_read(tw_pe *me)
{
	//tw_stime r = tw_clock_read(); 
	#ifdef SEPARATE_1
		if (g_tw_tid > 1) mt_receive(me);
	#elif defined(SEPARATE_0)
		if (g_tw_tid) 
        {
            //fprintf(stderr, "   %llu mt reads\n", me -> id);
            mt_receive(me);
            //fprintf(stderr, "   %llu mt read done\n", me -> id);
        }
	#else	
		mt_receive(me);
	#endif
	//me -> stats.s_regional_read += tw_clock_read() - r;

	if (!g_tw_tid)
	{
        //fprintf(stderr, "%llu mpi reads\n", me -> id);
		//tw_stime rr = tw_clock_read(); 
		int i = 0;
		
		while (i < g_tw_n_threads)
		{
			current_token = (current_token + 1) % g_tw_n_threads;
	  		int changed;
	
			//if (!g_tw_pid) fprintf(stderr, "%llu in while %.0f\n", me -> id, me -> GVT);		
			//else fprintf(stderr, "	%llu in while %.0f\n", me -> id, me -> GVT);	
				
			do {
				// Returns 0 if any irecv is not completed or posted_recv is empty
				// otherwise returns 1
				//tw_clock test_recv = tw_clock_read();	
				changed  = test_q(&posted_recvs, me, net_recv_finish);
				//me -> stats.s_remote_read_test_recv += tw_clock_read() - test_recv;
					
				//if (g_tw_pid == 1)
				{
				//	struct timeval timecheck;
				//	long s;		

				//	gettimeofday(&timecheck, NULL);
	 			//	s = (long) timecheck.tv_sec * 1000 + (long) timecheck.tv_usec / 1000;	
				//	printf("--> %ld milliseconds\n", s);	
				}

				//tw_clock test_send = tw_clock_read();	
				changed |= test_q(&posted_sends, me, net_send_finish);  
				//me -> stats.s_remote_read_test_send += tw_clock_read() - test_send;
					
				// Scans posted_recvs and returns 0 if irecv fails or iprobe 
				// returns 0 otherwise performs irecv and returns 1
				//tw_clock irecv = tw_clock_read();	
				changed |= recv_begin(me);
				//me -> stats.s_remote_read_irecv += tw_clock_read() - irecv;
				
				//if (!g_tw_pid)
				{
				//	struct timeval timecheck;
				//	long s;		

				//	gettimeofday(&timecheck, NULL);
	 			//	s = (long) timecheck.tv_sec * 1000 + (long) timecheck.tv_usec / 1000;	
				//	printf("%ld milliseconds\n", s);	
				}
			
				// Scans posted_sends and returns 0 if isend fails or outq -> peek is 
				// null otherwise perfroms isend and returns 1
				//tw_clock isend = tw_clock_read();	
				changed |= send_begin(me);
				//me -> stats.s_remote_read_isend += tw_clock_read() - isend;
			} while (changed);

			// service_queues(me);
			i++;  			  
		}
	
		current_token = -1;
		//me -> stats.s_remote_read += tw_clock_read() - rr;
    
        //fprintf(stderr, "%llu mpi read done\n", me -> id);
	}	
}

void network_send(tw_pe *me, tw_event *e)
{
	e -> state.owner = TW_net_outq;
	tw_node * dest_node = NULL;
		 
     	tw_event * cp_e = NULL;
      	e -> recv_pe = 0;  

	dest_node = tw_net_onnode((*e->src_lp->type.map)((tw_lpid) e->dest_lp));
		 
	if (!e -> state.cancel_q) e -> event_id = (tw_eventid) ++me->seq_num;
		
	e -> send_pe = (tw_peid) g_tw_mynode;	
	e -> state.owner = e -> state.cancel_q ? TW_net_acancel : TW_net_asend;
	e -> recv_pe = (tw_peid)(*dest_node);
	
	cp_e = mt_tw_event_new(e -> recv_pe, e -> recv_ts, e -> src_lp);
	memcpy(cp_e,e,g_tw_event_msg_sz);
		
 	// Sending Remote
	pthread_mutex_lock(&outq_mutex[g_tw_tid]);
	tw_eventq_unshift(&outq[g_tw_tid], cp_e);
	pthread_mutex_unlock(&outq_mutex[g_tw_tid]);

	send_finish(me,e,NULL);
}

void
tw_net_send(tw_event *e, int cancel_msg)
{
	tw_node * dest_node = NULL;
	int dest_node_id = 0;
	tw_pe * me = e -> src_lp -> pe;

	e -> state.remote = 0;
        if (e == me -> abort_event) tw_error(TW_LOC, "sending abort event!");
        
	dest_node = tw_net_onnode((*e->src_lp->type.map)((tw_lpid) e->dest_lp));
        dest_node_id = *dest_node;

	if (dest_node_id / g_tw_n_threads == g_tw_pid)
	{
		// to local thread
		#ifdef rollback_stats   
	 		e -> recv_pe = (tw_peid)(*dest_node);
		#endif
		
		if (cancel_msg) me -> stats.s_cancel_regional++;	
		me -> stats.s_nsend_regional++;	
	
		//if (!g_tw_pid && g_tw_tid == 1) fprintf(stderr, "Regional Sent\n");
	
		//struct timeval timecheck;
		//long start, end;		

		//if (me -> id == 1)
		//{
		//	gettimeofday(&timecheck, NULL);
	 	//	start = (long) timecheck.tv_sec * 1000 + (long) timecheck.tv_usec / 1000;	
		//}

		//time_t start = time(NULL);
		//fprintf(stderr, "%.2f\n", start);
	
		//tw_stime s = tw_clock_read();
		mt_send(me, e, (dest_node_id % g_tw_n_threads));
		//me -> stats.s_regional_send += tw_clock_read() - s;

		//if (me -> id == 1) printf("%.6f seconds elapsed\n", (double)  me -> stats.s_regional_send / g_tw_clock_rate);	
		
		//if (me -> id == 1)
		//{
		//	gettimeofday(&timecheck, NULL);
	 	//	end = (long) timecheck.tv_sec * 1000 + (long) timecheck.tv_usec / 1000;	
		//	printf("%ld milliseconds elapsed\n", (end - start));	
		//}
   	}
	else 
	{
		if (cancel_msg) me -> stats.s_cancel_remote++;	

		me -> stats.s_nsend_network++;
		
		//tw_stime s = tw_clock_read();	
		network_send(me,e);
		//me -> stats.s_remote_send += tw_clock_read() - s;
	}
}

void
tw_net_cancel(tw_event *e)
{
	tw_pe *src_pe = e->src_lp->pe;
  
	switch (e->state.owner) 
	{
  		case TW_net_outq:
    			/* Cancelled before we could transmit it.  Do not
     			* transmit the event and instead just release the
     			* buffer back into our own free list.
     			*/

			pthread_mutex_lock(&outq_mutex[g_tw_tid]);
   			tw_eventq_delete_any(&outq[g_tw_tid], e);
  			pthread_mutex_unlock(&outq_mutex[g_tw_tid]);

			// tw_eventq_delete_any(&outq, e);
			tw_event_free(src_pe, e);
    			return;
    			break;
 		
		case TW_net_asend:
    			/* Too late.  We've already let MPI start to send
     			* this event over the network.  We can't pull it
     			* back now without sending another message to do
     			* the cancel.
     			*
     			* Setting the cancel_q flag will signal us to do
     			* another message send once the current send of
     			* this message is completed.
     			*/
			// printf("XXX  !!Khatra DANGER ZONE!! XXX\n");
    			e->state.cancel_asend = 1;
    			break;

  		case TW_pe_sevent_q:
    			/* Way late; the event was already sent and is in
     			* our sent event queue.  Mark it as a cancel and
     			* place it at the front of the outq.
     			*/
 
			//DJ: This is the only case for muitithreaded implementation
			//Because above two are not present in this case
			//Whatever events are generated during processing are sent immidiately
			//So they must be cancelled by sending explicit cancel message

    			e->state.cancel_q = 1;

			//Change this
    			//tw_eventq_unshift(&outq, e);
			//DJ:
			src_pe -> stats.s_cancel++;
			tw_net_send(e, 1);	
    			break;

  		default:
    			/* Huh?  Where did you come from?  Why are we being
     			* told about you?  We did not send you so we cannot
     			* cancel you!
     			*/
    
			tw_error(
	     			TW_LOC,
	     			"Don't know how to cancel event owned by %u",
	     			e->state.owner);
 	}
	tw_net_read(src_pe);
}

tw_statistics *
tw_net_statistics(tw_pe * me, tw_statistics * s)
{
	tw_statistics node_s;
	bzero(&node_s, sizeof(node_s));

	// Wall clock run time	
	mt_allreduce(&(s -> s_max_run_time), &(node_s.s_max_run_time), 1, MT_DOUBLE, MT_MAX, &max_barrier); 
	if (g_tw_tid == 0) // Allreduce wall clock run time for whole system
	{
		if(MPI_Reduce(&(node_s.s_max_run_time), 
                	&me->stats.s_max_run_time,
                	1,
	                MPI_DOUBLE,
        	        MPI_MAX,
	                g_tw_masternode,
        	        MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}
	
	// CPU run time
	mt_allreduce(&s -> s_total, &(node_s.s_total), 1, MT_LONG_LONG, MT_MAX, &max_barrier);
	if (g_tw_tid == 0) // Allreduce cpu run time for whole system
	{
		if(MPI_Reduce(&(node_s.s_total), 
	                &me->stats.s_total,
        	        1,
	                MPI_LONG_LONG,
        	        MPI_MAX,
	                g_tw_masternode,
        	        MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");	
	}


	// Total CPU run time
	mt_allreduce(&s -> s_total_all, &(node_s.s_total_all), 1, MT_LONG_LONG, MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;

	if (g_tw_tid == 0) // Allreduce cpu run time for whole system
	{
		if(MPI_Reduce(&(node_s.s_total_all), 
	                &me -> stats.s_total_all,
        	        1,
	                MPI_LONG_LONG,
        	        MPI_SUM,
	                g_tw_masternode,
        	        MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");	
	}

	// Number of Rollbacks
	mt_allreduce(&(s->s_e_rbs), &(node_s.s_e_rbs), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_e_rbs), 
			&me -> stats.s_e_rbs,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	// Number of total events processed
	mt_allreduce(&(s->s_nevent_processed), &(node_s.s_nevent_processed), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_nevent_processed), 
			&me -> stats.s_nevent_processed,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	// Number of committed events
	mt_allreduce(&(s->s_net_events), &(node_s.s_net_events), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_net_events), 
			&me -> stats.s_net_events,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	// Total cpu time for event processing 
	mt_allreduce(&(s->s_event_process), &(node_s.s_event_process), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_event_process), 
			&me -> stats.s_event_process,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	// Total cpu time for priority queue
	mt_allreduce(&(s->s_pq), &(node_s.s_pq), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_pq), 
			&me -> stats.s_pq,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	// Total cpu time for event cancel 
	mt_allreduce(&(s->s_cancel_q), &(node_s.s_cancel_q), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_cancel_q), 
			&me -> stats.s_cancel_q,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	// Total cpu time for rollbacks
	mt_allreduce(&(s->s_rollback), &(node_s.s_rollback), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_rollback), 
			&me -> stats.s_rollback,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	// Total cpu time for event abort 
	mt_allreduce(&(s->s_event_abort), &(node_s.s_event_abort), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_event_abort), 
			&me -> stats.s_event_abort,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	// Total cpu time for net_read function
	mt_allreduce(&(s->s_net_read), &(node_s.s_net_read), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_net_read), 
			&me -> stats.s_net_read,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	// Total cpu time for gvt function
	mt_allreduce(&(s -> s_gvt_func), &(node_s.s_gvt_func), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_gvt_func), 
			&me -> stats.s_gvt_func,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}	
	
	// Total cpu time for sched event function
	mt_allreduce(&(s -> s_sched_event), &(node_s.s_sched_event), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_sched_event), 
			&me -> stats.s_sched_event,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	// Total cpu time for batch event function 
	mt_allreduce(&(s -> s_batch_event), &(node_s.s_batch_event), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_batch_event), 
			&me -> stats.s_batch_event,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	// Total cpu time for gvt round 
	mt_allreduce(&(s -> s_gvt), &(node_s.s_gvt), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_gvt), 
			&me -> stats.s_gvt,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	// Total cpu time for fossil collect 
	mt_allreduce(&(s -> s_fossil_collect), &(node_s.s_fossil_collect), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_fossil_collect), 
			&me -> stats.s_fossil_collect,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	// Forced GVT 
	mt_allreduce(&(s -> force_gvt), &(node_s.force_gvt), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.force_gvt), 
			&me -> stats.force_gvt,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	#ifdef ROSS_GVT_mattern_orig
	// Total cpu time for cut 1 in GVT
	mt_allreduce(&(s -> s_mpi_mattern_c1), &(node_s.s_mpi_mattern_c1), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_mpi_mattern_c1), 
			&me -> stats.s_mpi_mattern_c1,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	// Total cpu time for cut 2 in GVT 
	mt_allreduce(&(s -> s_mpi_mattern_c2), &(node_s.s_mpi_mattern_c2), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.s_mpi_mattern_c2), 
			&me -> stats.s_mpi_mattern_c2,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}		

	// GVT Mattern try lock fails
	mt_allreduce(&(s -> try_lock_fails), &(node_s.try_lock_fails), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.try_lock_fails), 
			&me -> stats.try_lock_fails,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	// GVT Mattern cm check fails
	mt_allreduce(&(s -> cm_check_fails), &(node_s.cm_check_fails), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
	
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.cm_check_fails), 
			&me -> stats.cm_check_fails,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}
	#endif

	// GVT Mattern std dev counter
	//if (me -> id == 0) printf("\n---> %f\n", s -> std_dev);

	mt_allreduce(&(s -> std_dev), &(node_s.std_dev), 1, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
	sum_index = (sum_index + 1) % 2;
		
	if (g_tw_tid == 0)
	{
		if(MPI_Reduce(&(node_s.std_dev), 
			&me -> stats.std_dev,
			1,
			MPI_LONG_LONG,
			MPI_SUM,
			g_tw_masternode,
			MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}
	

	return &me -> stats;

	/*
	#ifdef rollback_stats
		mt_allreduce(&(s -> s_net_events), &(node_s.s_net_events), 22, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
        	sum_index=(sum_index+1)%2;
  	
		if (g_tw_tid == 0)
		{
      			if (MPI_Reduce(&(node_s.s_net_events),
                		&me->stats.s_net_events,
                  		22,
                  		MPI_LONG_LONG,
                  		MPI_SUM,
                  		g_tw_masternode,
                  		MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
  		}
	#else
		mt_allreduce(&(s->s_net_events), &(node_s.s_net_events), 16, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
        	sum_index=(sum_index+1)%2;
		
		if (g_tw_tid == 0)
		{
			if(MPI_Reduce(&(node_s.s_net_events), 
        	        	&me -> stats.s_net_events,
                		16,
	                	MPI_LONG_LONG,
                		MPI_SUM,
	       	        	g_tw_masternode,
        	        	MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
		}
	#endif
	*/

	/*
	mt_allreduce(&(s->s_nsend_loc), &(node_s.s_nsend_loc), 16, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
        sum_index=(sum_index+1)%2;

	if(g_tw_tid==0)
	{
		  if(MPI_Reduce(&(node_s.s_nsend_loc), 
        	        &me->stats.s_nsend_loc,
                	16,
	                MPI_LONG_LONG,
                	MPI_SUM,
	       	        g_tw_masternode,
        	        MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}

	mt_allreduce(&(s->s_nsend_loc_remote), &(node_s.s_nsend_loc_remote), 16, MT_LONG_LONG,  MT_SUM, &sum_barrier[sum_index]);
        sum_index=(sum_index+1)%2;
	if(g_tw_tid==0)
	{
		  if(MPI_Reduce(&(node_s.s_nsend_loc_remote), 
        	        &me->stats.s_nsend_loc_remote,
                	16,
	                MPI_LONG_LONG,
                	MPI_SUM,
	       	        g_tw_masternode,
        	        MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "Unable to reduce statistics!");
	}
	*/	
}
