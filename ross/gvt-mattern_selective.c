#include <ross.h>
#include <stddef.h>

#ifdef SL_BARRIER	
static pthread_barrier_t fossil_barrier;
#endif

// Statistics
//static __thread unsigned int gvt_force = 0;
static __thread tw_stime measure_fc = 0;
static __thread tw_stime measure_gvt = 0;
static __thread tw_stime mpi_b = 0;

static int c1_counter; 		// Counts how many threads reached the C 1
static int c1_completed;
static int c2_counter;		// Counts how many threads reached the C 2
static int c2_completed;

static int gvt_done_counter;
static double global_gvt;
//static double gvt_prev;

#define num_nodes 2
typedef struct control // control block
{
	double t_min;
	double t_red;
	int msg_counters[256][num_nodes]; // [0][1] holds number of messages sent from all thread to the thread 0 in machine 1
} _control;

static _control control_message;
static pthread_mutex_t cm_lock;

static MPI_Datatype mpi_control_message;
static MPI_Op custom_op;

void 
tw_gvt_start()
{
	if (!g_tw_tid)
	{
		for (int i = 0; i < no_threads; i++) 
		{
			spin_analysis[i].should_spin = 0;
			spin_array[i].should_spin = 0;
		}

		c1_completed = 0;
		c1_counter = 0;
		c2_counter = 0;
		c2_completed = 0;
		gvt_done_counter = 0;
		reset_control_message();

		MPI_Aint displacements[3] = {offsetof(_control, t_min), 
				     	     offsetof(_control, t_red),
				             offsetof(_control, msg_counters)};
		
		int block_lengths[3] = {1, 1, no_threads * g_tw_machines};
		MPI_Datatype types[3] = {MPI_DOUBLE, MPI_DOUBLE, MPI_INT};

		int rv = MPI_Type_create_struct(3, block_lengths, displacements, types, &mpi_control_message);
		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "mpi type creation failed!");
		
		rv = MPI_Type_commit(&mpi_control_message);
		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "mpi type commit failed!");	
		
		if (pthread_mutex_init(&cm_lock, NULL) != 0) tw_error(TW_LOC, "Mutex init failed\n");
	
		rv = MPI_Op_create(mpi_func, 1, &custom_op);
		if (rv != MPI_SUCCESS) tw_error(TW_LOC, "mpi op create failed!");	
	}	
}	

void 
tw_gvt_step1(tw_pe * me)
{
	// C 1 is being constructed
	if (me -> color == white && !gvt_done_counter)
	{
		//if (spin_array[g_tw_tid].should_spin) 
		//{
			//fprintf(stderr, "%d sleeps\n", g_tw_tid);
			//usleep(100);
		//}

		#ifdef DEBUG_M
		if (!g_tw_pid) fprintf(stderr, "%u becomes red (%.0f)\n", g_tw_tid, me -> GVT);
		else fprintf(stderr, "	%u becomes red (%.0f)\n", g_tw_tid, me -> GVT);
		#endif
		
		// Each thread checks the C 1 and accumulate its message counters
		measure_gvt = tw_clock_read();
		me -> color = red;
		me -> t_red = g_tw_ts_end;			
			
		accumulate_msg_counters(me); // control block is updated 
			
		__sync_add_and_fetch(&c1_counter, 1);	
	}
	// Each thread is done with C 1, now accumulate CM between the machines
	else if (c1_counter == g_tw_n_threads && !g_tw_tid)
	{
		#ifdef DEBUG_M
		if (!g_tw_pid) fprintf(stderr, "%u exchanges cm (%.0f)\n", g_tw_tid, me -> GVT);
		else fprintf(stderr, "	%u exchanges cm (%.0f)\n", g_tw_tid, me -> GVT);
		#endif
		
		// First machine is responsible to send cm
		_control cm;
		
		mpi_b = tw_clock_read();
                all_reduce_cm(&cm);
		me -> stats.s_mpi_mattern_c1 += tw_clock_read() - mpi_b;
                
		reset_local_cm_with_other_cm(&cm);

		// At this point the message accumulation should be done
		// Local cm of both machines should be same and has all the message counts
		c1_counter = 0;
		c1_completed = 1;
	}
	// C 2 is being contructed
	else if (me -> color == red && c1_completed)
	{
		if (!me -> c2_checked && 
		     me -> msg_counters[g_tw_tid][g_tw_pid] + control_message.msg_counters[g_tw_tid][g_tw_pid] == 0)
		{
			#ifdef DEBUG_M
			if (!g_tw_pid) fprintf(stderr, "%u updates cm (%.0f)\n", g_tw_tid, me -> GVT);
			else fprintf(stderr, "	%u updates cm (%.0f)\n", g_tw_tid, me -> GVT);
			#endif

			me -> c2_checked = 1; 
			update_cm(me);			
			__sync_add_and_fetch(&c2_counter, 1);
		}
		else if (!me -> c2_checked)
		{
			__sync_add_and_fetch(&cm_check_fails, 1);
			me -> cm_check_fails++;
		}
		
		// Each thread is done with C 2, now update CM between the machines
		if (c2_counter == g_tw_n_threads && !g_tw_tid)
		{		
			#ifdef DEBUG_M
			if (!g_tw_pid) fprintf(stderr, "%u exchanges gvt (%.0f)\n", g_tw_tid, me -> GVT);
			else fprintf(stderr, "	%u exchanges gvt (%.0f)\n", g_tw_tid, me -> GVT);
			#endif
	
			// First machine is responsible to send cm
			double local_gvt = min(control_message.t_min, control_message.t_red);
			
			mpi_b = tw_clock_read();
                        all_reduce_gvt(&local_gvt);	
			me -> stats.s_mpi_mattern_c2 += tw_clock_read() - mpi_b;

			c2_counter = 0;
			c2_completed = 1;
		}

		if (c2_completed)
		{
			fossil_collect(me);
			finish_gvt(me);
	
			#ifdef DEBUG_M
			if (!g_tw_pid) fprintf(stderr, "%u fossil (%.0f)\n", g_tw_tid, me -> GVT);
			else fprintf(stderr, "	%u fossil (%.0f)\n", g_tw_tid, me -> GVT);
			#endif

			if (me -> id == 0) gvt_print(me);
			if (g_tw_tid == 0) gvt_info(me);
			

			if (__sync_add_and_fetch(&gvt_done_counter, 1) == g_tw_n_threads)
			{
				reset_control_message();
				c1_completed = 0;
				c2_completed = 0;
				gvt_done_counter = 0;
			} 
			
			me -> stats.s_gvt += tw_clock_read() - measure_gvt;
		}
	}
}

void
gvt_info(tw_pe * me)
{
	//for (int i = 1; i < g_tw_n_threads; i++) fprintf(stderr, "%.0f ", lvts[i].lvt); fprintf(stderr, "\n");

	double std_dev = 0, avg = 0;

	if ((int) min(100, floor(100 * (me -> GVT / g_tw_ts_end))) < 100)
	{
		double total = 0, dist_to_mean = 0;

		for (int i = 1; i < g_tw_n_threads; i++) total += lvts[i].lvt;	
		avg = total / (g_tw_n_threads - 1);		
		
		for (int i = 1; i < g_tw_n_threads; i++) dist_to_mean += (lvts[i].lvt - avg) * (lvts[i].lvt - avg);
		std_dev = sqrt(dist_to_mean / (g_tw_n_threads - 1));	
		
		me -> std_dev += std_dev;

		if (me -> id == 0) fprintf(stderr, " Std Dev: %.2f\n", std_dev);
	}
	//else fprintf(stderr, "\nAvg Std Dev: %.2f", me -> std_dev / g_tw_gvt_done);
	
	if (me -> id == 0) fprintf(stderr, "Spinning: ");
	for (int i = 1; i < no_threads; i++)
	{
		if (lvts[i].lvt > 1.0 * (avg + std_dev)) 
		{
			if (me -> id == 0) fprintf(stderr, "%d ", i);

			spin_array[i].should_spin = 1;
			spin_analysis[i].should_spin++;
		}
		else spin_array[i].should_spin = 0;
	}
	if (me -> id == 0) fprintf(stderr, "\n");
}

void
mark_faster_lps()
{
	double avg, std_dev_lvt, total = 0, distance_to_mean = 0;
	int i;	
					
	for (i = 1; i < no_threads; i++) total += lvts[i].lvt;
			
	avg = total / (no_threads - 1);		
		
	for (i = 1; i < no_threads; i++) distance_to_mean += (lvts[i].lvt - avg) * (lvts[i].lvt - avg);

	std_dev_lvt = sqrt(distance_to_mean / (no_threads - 1));
	
	//if (!g_tw_pid) fprintf(stderr,"avg: %f, std dev: %f\n", avg, std_dev_lvt);		
	for (i = 1; i < no_threads; i++)
	{
		//if (!g_tw_pid) fprintf(stderr,"%d LVT: %f\n", i, lvts[i].lvt);		
	
		if (lvts[i].lvt > 5 * (avg + std_dev_lvt)) 
		{
			spin_array[i].should_spin = 1;
			spin_analysis[i].should_spin += 1;
		}
		else spin_array[i].should_spin = 0;
	}
	//if (!g_tw_pid) fprintf(stderr, "\n");
}

void
report_lvts(tw_pe * me)
{
	/*
	tw_stime t_msg, n_min, pq_min;

	if (me -> trans_msg > g_tw_ts_end) t_msg = g_tw_ts_end;
	else t_msg = me -> trans_msg;

	if (me -> net_min > g_tw_ts_end) n_min = g_tw_ts_end;
	else n_min = me -> net_min;

	if (me -> pq_min > g_tw_ts_end) pq_min = g_tw_ts_end;
	else pq_min = me -> pq_min;

	fprintf(stderr, "%u: LVT: %.0f, trans_msg: %.0f, net_min: %.0f, pq_min: %.0f\n", g_tw_tid, me -> LVT, t_msg, n_min, pq_min);
	*/

	// For disparity calculation
	#ifdef disparity_check
	lvt_array[g_tw_gvt_done].lvt[g_tw_tid] = me -> LVT;
	#endif

	// For throttling
	lvts[g_tw_tid].lvt = me -> LVT;
}

void 
mpi_func(_control * in, _control * inout, int * len, MPI_Datatype * dptr)
{
	if (*len != 1) MPI_Abort(MPI_COMM_WORLD, 1);	

	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++)
		{
			inout -> msg_counters[i][j] = inout -> msg_counters[i][j] + in -> msg_counters[i][j];		
		}
	}	
}

void
all_reduce_cm(void * cm)
{
	int rv = MPI_Allreduce(
		&control_message,
		cm,
		1,
		mpi_control_message,
		custom_op,
		MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_ALL_REDUCE Failed");	
}

void
all_reduce_gvt(void * local_gvt)
{
	int rv = MPI_Allreduce(
			local_gvt,
			&global_gvt,
			1,
			MPI_DOUBLE,
			MPI_MIN,
			MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_ALL_REDUCE Failed");	
}

void
update_cm(tw_pe * me)
{
	#ifdef SEPARATE_1
		if (g_tw_tid > 1) update_control(me);
	#elif defined(SEPARATE_0)
		if (g_tw_tid) update_control(me);
	#else
		update_control(me);
	#endif
}

void 
update_control(tw_pe * me)
{
	pthread_mutex_lock(&cm_lock);
	control_message.t_min = min(control_message.t_min, min_pq_outq(me));
	control_message.t_red = min(control_message.t_red, me -> t_red);	
	pthread_mutex_unlock(&cm_lock);
	
	me -> msg_counters[g_tw_tid][g_tw_pid] = 0;
}

void
reset_local_cm_with_other_cm(_control * cm)
{
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++)
		{
			control_message.msg_counters[i][j] = cm -> msg_counters[i][j];
		}
	}
}

void 
accumulate_msg_counters(tw_pe * me)
{
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		for (int j = 0; j < g_tw_machines; j++)
		{
			if (i != g_tw_tid || j != g_tw_pid)
			{
				__sync_add_and_fetch(&control_message.msg_counters[i][j], me -> msg_counters[i][j]); 
				me -> msg_counters[i][j] = 0;
			}
		}
	}
}

void
reset_control_message()
{
	control_message.t_min = g_tw_ts_end + 1;
	control_message.t_red = g_tw_ts_end + 1;
	for (int i = 0; i < g_tw_n_threads; i++) for (int j = 0; j < g_tw_machines; j++) 
	{
		control_message.msg_counters[i][j] = 0;
	}
}

void 
fossil_collect(tw_pe * me)
{
	me -> GVT = global_gvt - 1;
	
	measure_fc = tw_clock_read();

	#ifdef SL_LOCKS
		//if (!g_tw_pid) fprintf(stderr, "Node %d enter, GVT = %lf\n", g_tw_mynode, gvt);
		//else fprintf(stderr, "	Node %d enter, GVT = %lf\n", g_tw_mynode, gvt);
		if (g_tw_tid == 1) tw_pe_fossil_collect(me);
	#elif defined(SL_BARRIER)
		pthread_barrier_wait(&fossil_barrier);
		//if (g_tw_tid == 1)  tw_pe_fossil_collect(me);
		if (!g_tw_tid)  tw_pe_fossil_collect(me);
		pthread_barrier_wait(&fossil_barrier);
	#elif defined(SEPARATE_1)
		if (g_tw_tid > 1) tw_pe_fossil_collect(me);
	#elif defined(SEPARATE_0)
		if (g_tw_tid) tw_pe_fossil_collect(me);
	#else
		tw_pe_fossil_collect(me);
	#endif
	
	me -> stats.s_fossil_collect += tw_clock_read() - measure_fc;
}

void 
finish_gvt(tw_pe * me)
{
	g_tw_gvt_done++;
	
	me -> interval_counter = g_tw_gvt_interval;
	me -> trans_msg_ts = DBL_MAX + 3;
	me -> c2_checked = 0;
	me -> color = white;
}


tw_stime
min_pq_outq(tw_pe * me)
{
	// tw_net_read(me); // might be neccessary

	// return the minimum of pq of PE	
	tw_stime pq_min = tw_pq_minimum(me -> pq);
	me -> pq_min = pq_min;

	// return the minimum of outq and posted_sends of PE. Buffers for mpi.
	// populated during network_send, extracted during send_begin
	tw_stime net_min = tw_net_minimum(me);
	me -> net_min = net_min;

	tw_stime t_msg = me -> trans_msg_ts;
	me -> trans_msg = t_msg;
	
	double lvt = t_msg;

	if (lvt > pq_min) lvt = pq_min;
	if (lvt > net_min) lvt = net_min;
	
	if (lvt > g_tw_ts_end) 
	{
		me -> LVT = g_tw_ts_end + 2;
		return g_tw_ts_end + 2;
	}
	else 
	{
		//me -> LVT = lvt;
		return lvt;
	}
}

static const tw_optdef gvt_opts [] =
{
	TWOPT_GROUP("ROSS MPI GVT"),
	TWOPT_UINT("gvt-interval", g_tw_gvt_interval, "GVT Interval"),
	TWOPT_END()
};

const tw_optdef *
tw_gvt_setup(void) 
{
	#ifdef SL_BARRIER
	pthread_barrier_init(&fossil_barrier, NULL, no_threads);
	#endif
	
	return gvt_opts;
}

void
tw_gvt_force_update(tw_pe * me) 
{
	__sync_add_and_fetch(&gvt_force_global, 1);
	me -> gvt_force++;
	me -> interval_counter = 0;
}

void tw_gvt_step2(tw_pe * me) {}

void
tw_gvt_stats(FILE * f)
{
	fprintf(f, "\nTW GVT Statistics: Selective Mattern \n");
	fprintf(f, "\t%-50s %11d\n", "GVT Interval", g_tw_gvt_interval);
	fprintf(f, "\t%-50s %11d\n", "Batch Size", g_tw_mblock);
	//fprintf(f, "\t%-50s %11d\n", "Forced GVT", gvt_force);
	fprintf(f, "\t%-50s %11d\n", "Total GVT Computations", g_tw_gvt_done);
}
