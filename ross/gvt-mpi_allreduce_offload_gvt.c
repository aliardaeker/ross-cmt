#include <ross.h>

#define TW_GVT_NORMAL 0
#define TW_GVT_COMPUTE 1

static __thread unsigned int gvt_cnt = 0;
static __thread unsigned int gvt_force = 0;
static __thread tw_stat all_reduce_cnt = 0;

static const tw_optdef gvt_opts [] =
{
	TWOPT_GROUP("ROSS MPI GVT"),
	TWOPT_UINT("gvt-interval", g_tw_gvt_interval, "GVT Interval"),
	TWOPT_STIME("report-interval", gvt_print_interval, "percent of runtime to print GVT"),
	TWOPT_END()
};

#ifdef SL_BARRIER	
pthread_barrier_t fossil_barrier;
#endif

pthread_barrier_t barrier;
static int node_clean;

tw_stat local_whites; 
tw_stat other_whites; 

double node_gvt;
double global_gvt;

void 
tw_gvt_step2(tw_pe * me)
{
	if (me -> gvt_status != TW_GVT_COMPUTE) return;
	tw_clock start = tw_clock_read();

	while (1)
	{
		#ifdef DEBUG_M
		if (!g_tw_pid) fprintf(stderr, "%u loops\n", g_tw_tid);
		else fprintf(stderr, "	%u loops\n", g_tw_tid);
		#endif
		
		tw_net_read(me);
		pthread_barrier_wait(&barrier);
	
		#ifdef DEBUG_M
		//if (!g_tw_pid) fprintf(stderr, "%u net read\n", g_tw_tid);
		//else fprintf(stderr, "	%u net read\n", g_tw_tid);
		#endif
		
		if (!g_tw_tid)
		{
			local_whites = 0;
			
			for (int i = 0; i < no_threads; i++) 
			{
				#ifdef DEBUG_M
				//if (!g_tw_pid) fprintf(stderr, "%d -> %lld - %lld\n", i, 
				//		*white_sent_list[i], *white_received_list[i]);
				//else fprintf(stderr, "	%d -> %lld - %lld\n", i, 
				//		*white_sent_list[i], *white_received_list[i]);
				#endif

				local_whites += *white_sent_list[i] - *white_received_list[i];
			}
	
			#ifdef DEBUG_M
			//if (!g_tw_pid) fprintf(stderr, "%u -> %lld\n", g_tw_tid, local_whites);
			//else fprintf(stderr, "	%u -> %lld\n", g_tw_tid, local_whites);
			#endif

			if (!g_tw_pid) 
			{
				send_white_count(1);
				recv_white_count(1);
			}
			else 
			{
				send_white_count(0);
				recv_white_count(0);
			}

			#ifdef DEBUG_M
			//if (!g_tw_pid) fprintf(stderr, "%u syncs %lld + %lld\n", g_tw_tid, local_whites, other_whites);
			//else fprintf(stderr, "	%u syncs %lld + %lld\n", g_tw_tid, local_whites, other_whites);
			#endif

			if (local_whites + other_whites == 0) node_clean = 1;

		}
		
		pthread_barrier_wait(&barrier);
	
		#ifdef DEBUG_M
		//if (!g_tw_pid) fprintf(stderr, "%u clean\n", g_tw_tid);
		//else fprintf(stderr, "	%u clean\n", g_tw_tid);
		#endif

		// Loops until no intransit messages between threads in a machine
		if (node_clean) break;	
	}
	
	if (g_tw_tid > 0) me -> lvt = min_pq_outq(me);

	pthread_barrier_wait(&barrier);
	
	if (!g_tw_tid)
	{
		node_gvt = DBL_MAX;
		
		// Threads 0
		for (int i = 1; i < no_threads; i++) if (*lvt_list[i] < node_gvt) node_gvt = *lvt_list[i];	
		
		#ifdef DEBUG_M
		if (!g_tw_pid) fprintf(stderr, "%u`s node gvt %.0f\n", g_tw_tid, node_gvt);
		else fprintf(stderr, "	%u`s node gvt %.0f\n", g_tw_tid, node_gvt);
		#endif
		
		if (!g_tw_pid) 
		{
			send_gvt(1);
			recv_gvt(1);
		}
		else 
		{
			send_gvt(0);
			recv_gvt(0);
		}

		global_gvt = min(global_gvt, node_gvt);
	}	

	pthread_barrier_wait(&barrier);

	if (global_gvt / g_tw_ts_end > percent_complete && tw_node_eq(&g_tw_mynode, &g_tw_masternode)) gvt_print(global_gvt);

	me -> s_nwhite_sent = 0;
	me -> s_nwhite_recv = 0;
	me -> trans_msg_ts = DBL_MAX;
	me -> GVT = global_gvt;
	me -> gvt_status = TW_GVT_NORMAL;
	me -> stats.s_gvt += tw_clock_read() - start;

	start = tw_clock_read();
	fossil_collect(me);		
	me -> stats.s_fossil_collect += tw_clock_read() - start;

	g_tw_gvt_done++;
	gvt_cnt = 0;
	
	if (!g_tw_tid) node_clean = 0;
}

void
send_gvt(int dest)
{
	// Returns when transmission is completed
	int rv = MPI_Send(&node_gvt,
		    1,
		    MPI_DOUBLE,
		    dest, //(g_tw_pid + 1) % g_tw_machines, // Destination
		    2,
		    MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_SEND Failed");
}

void
recv_gvt(int dest)
{
	// Returns when transmission is completed
	int rv = MPI_Recv(&global_gvt,
		    1,
		    MPI_DOUBLE,
		    dest, //(g_tw_pid + 1) % g_tw_machines, // Destination
		    2,
		    MPI_COMM_WORLD,
		    MPI_STATUS_IGNORE);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_SEND Failed");
}

void 
send_white_count(int dest)
{
	// Returns when transmission is completed
	int rv = MPI_Send(&local_whites,
		    1,
		    MPI_UNSIGNED_LONG_LONG,
		    dest, //(g_tw_pid + 1) % g_tw_machines, // Destination
		    3,
		    MPI_COMM_WORLD);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_SEND Failed");
}

void
recv_white_count(int source)
{
	// Returns when transmission is completed
	int rv = MPI_Recv(&other_whites,
		    1,
		    MPI_UNSIGNED_LONG_LONG,
		    source, //(g_tw_pid + 1) % g_tw_machines, // Destination
		    3,
		    MPI_COMM_WORLD,
		    MPI_STATUS_IGNORE);

	if (rv != MPI_SUCCESS) tw_error(TW_LOC, "MPI_RECV Failed");
}

const tw_optdef *
tw_gvt_setup(void)
{
	pthread_barrier_init(&barrier, NULL, no_threads);
	
	#ifdef SL_BARRIER
	pthread_barrier_init(&fossil_barrier, NULL, no_threads);
	#endif

	node_clean = 0;
	gvt_cnt = 0;

	return gvt_opts;
}

void
tw_gvt_start(void) {}

void
tw_gvt_force_update(tw_pe *me)
{
	gvt_force++;
	gvt_cnt = g_tw_gvt_interval;
}

void
tw_gvt_stats(FILE * f)
{
	fprintf(f, "\nTW GVT Statistics: MPI AllReduce\n");
	fprintf(f, "\t%-50s %11d\n", "GVT Interval", g_tw_gvt_interval);
	fprintf(f, "\t%-50s %11d\n", "Batch Size", g_tw_mblock);
	fprintf(f, "\n");
	fprintf(f, "\t%-50s %11d\n", "Forced GVT", gvt_force);
	fprintf(f, "\t%-50s %11d\n", "Total GVT Computations", g_tw_gvt_done);
	fprintf(f, "\t%-50s %11lld\n", "Total All Reduce Calls", all_reduce_cnt);
	fprintf(f, "\t%-50s %11.2lf\n", "Average Reduction / GVT", (double) ((double) all_reduce_cnt / (double) g_tw_gvt_done));
}

void
tw_gvt_step1(tw_pe *me)
{
	if (me->gvt_status == TW_GVT_COMPUTE || ++gvt_cnt < g_tw_gvt_interval) return;
	me->gvt_status = TW_GVT_COMPUTE;
	//printf("Start GVT:  node=%d\n",g_tw_mynode);
}

tw_stime
min_pq_outq(tw_pe * me)
{
	// return the minimum of pq of PE	
	tw_stime pq_min = tw_pq_minimum(me -> pq);

	// return the minimum of outq and posted_sends of PE. Buffers for mpi.
	// populated during network_send, extracted during send_begin
	tw_stime net_min = tw_net_minimum(me);

	tw_stime lvt = me -> trans_msg_ts;
	
	if (lvt > pq_min) lvt = pq_min;
	if (lvt > net_min) lvt = net_min;
	
	return lvt;
}

void
fossil_collect(tw_pe * me)
{
	#ifdef SL_LOCKS
		//if (!g_tw_pid) fprintf(stderr, "Node %d enter, GVT = %lf\n", g_tw_mynode, gvt);
		//else fprintf(stderr, "	Node %d enter, GVT = %lf\n", g_tw_mynode, gvt);
		if (g_tw_tid == 1) tw_pe_fossil_collect(me);
	#elif defined(SL_BARRIER)
		//if (!g_tw_pid) fprintf(stderr, "Node %d fc, GVT = %lf\n", g_tw_mynode, me -> GVT);
		//else fprintf(stderr, "	Node %d fc, GVT = %lf\n", g_tw_mynode, me -> GVT);

		//pthread_barrier_wait(&fossil_barrier);
		//if (g_tw_tid == 1)  tw_pe_fossil_collect(me);
		
		if (!g_tw_tid)  tw_pe_fossil_collect(me);
		pthread_barrier_wait(&fossil_barrier);
		
		//if (!g_tw_pid) fprintf(stderr, "Node %d fc done 2, GVT = %lf\n", g_tw_mynode, me -> GVT);
		//else fprintf(stderr, "	Node %d fc done 2, GVT = %lf\n", g_tw_mynode, me -> GVT);
			
	#elif defined(SEPARATE_1)
		if (g_tw_tid > 1) tw_pe_fossil_collect(me);
	#elif defined(SEPARATE_0)
		//if (!g_tw_pid) fprintf(stderr, "Node %d enter, GVT = %lf\n", g_tw_mynode, me -> GVT);
		//else fprintf(stderr, "	Node %d enter, GVT = %lf\n", g_tw_mynode, me -> GVT);
		
		if (g_tw_tid) tw_pe_fossil_collect(me);
	#else
		tw_pe_fossil_collect(me);
	#endif
}
