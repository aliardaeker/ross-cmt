#ifndef MT_BARRIER_SYNC
#define MT_BARRIER_SYNC
#include <pthread.h>
#include <stdio.h>
#include <errno.h>

enum {MT_DOUBLE = 1, MT_LONG_LONG} mt_type;
enum {MT_SUM = 1, MT_MIN,MT_MAX} mt_op;

//DJ: barrier sync data stuctures and functions
typedef struct{
        pthread_mutex_t mutex;
        //pthread_cond_t  cnd_var;
	pthread_barrier_t barrier;
        int count;      //Number of threads
        int n_threads; //Number of threads
	double db_result[32];
	long long ll_result[32];
}barrier_t;

barrier_t g_barrier;
barrier_t sum_barrier[2];
barrier_t min_barrier[2];
barrier_t max_barrier;

void init_barrier(barrier_t *barrier,int n);
void barrier_sync(barrier_t *barrier,long tid);
void destroy_barrier(barrier_t *barrier);

int mt_allreduce(void *in_no,void *out_no,int in_cnt,int type,int op,barrier_t *br);
int mt_allreduce_long_sum(unsigned long long * in_no, unsigned long long * out_no,barrier_t *barrier);
int mt_allreduce_double_min(double * in_no, double * out_no, barrier_t *barrier);

int mt_allreduce_long_sum_2(int node, unsigned long long * in_no, unsigned long long * out_no,barrier_t *barrier);
int mt_allreduce_double_min_2(int node, double * in_no, double * out_no, barrier_t *barrier);

#endif
