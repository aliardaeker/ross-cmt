#ifndef INC_network_tcp1_h
#define INC_network_tcp1_h

#include <arpa/inet.h>

#define MAX_NODES 256

typedef struct tw_net_stats tw_net_stats;
typedef struct tw_net_node tw_net_node

typedef unsigned int tw_eventid;
typedef unsigned int tw_node;

typedef struct sockaddr_in tw_socket;
typedef unsigned int tw_port;

extern tw_node		 tw_mynode;

extern tw_port		 g_tw_port;

extern int		 nnodes;
extern int		 nnet_nodes;

extern unsigned short int *pe_map;
extern unsigned int	*g_tw_node_ids;

extern tw_net_node	*g_tw_gvt_node;
extern tw_net_node	*g_tw_barrier_node;
extern tw_net_node	**g_tw_net_node;

extern tw_net_stats	 overall;


struct tw_net_stats
{
	long		 s_nsend;
	long		 s_nrecv;

	tw_stime	 lgvt;
};

struct tw_net_node
{
	tw_node		 id;
	tw_port		 port;

	tw_socket	 socket;
	int		 socket_fd;
	unsigned int	 socket_sz;

	int		*clients;
	int		*servers;

	tw_net_stats	 stats;

	char		 hostname[256];
};

#endif
