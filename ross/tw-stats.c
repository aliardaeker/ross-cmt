#include <ross.h>

#ifndef ROSS_DO_NOT_PRINT
static void
show_lld(const char *name, tw_stat v)
{
	printf("\t%-50s %11lld\n", name, v);
	//fprintf(g_tw_csv, "%lld,", v);
}

static void
show_2f(const char *name, double v)
{
	printf("\t%-50s %11.2f %%\n", name, v);
	//fprintf(g_tw_csv, "%.2f,", v);
}

static void
show_1f(const char *name, double v)
{
	printf("\t%-50s %11.1f\n", name, v);
	//fprintf(g_tw_csv, "%.2f,", v);
}

#ifdef print
static void
show_4f(const char *name, double v)
{
	printf("\t%-50s %11.4lf\n", name, v);
	//fprintf(g_tw_csv, "%.4lf,", v);
}

//static void
//show_8f(const char *name, double v)
//{
//	printf("\t%-50s %11.8lf\n", name, v);
	//fprintf(g_tw_csv, "%.4lf,", v);
//}
#endif
#endif

void
tw_stats(tw_pe * me, int n)
{
	tw_statistics s;

	tw_pe	*pe;
	tw_kp	*kp;
	tw_lp	*lp = NULL;

	int	 i;

	size_t m_alloc, m_waste;

	if (me != g_tw_pe[0]) return;
	if (!g_tw_sim_started) return;

	tw_calloc_stats(&m_alloc, &m_waste);
	bzero(&s, sizeof(s));
  
	// fprintf(stderr, "%lu summing stats\n", me -> id); All the threads in all the machines

	for(pe = NULL; (pe = tw_pe_next(pe));) // In case if a thread has more than one PE, I think 
	{
		//if (me -> id == 0) fprintf(stderr, "me 0 loops\n"); -> Just once

		tw_wtime rt;

		tw_wall_sub(&rt, &pe -> end_time, &pe -> start_time); 		
		s.s_max_run_time = max(s.s_max_run_time, tw_wall_to_double(&rt)); // max wall clock run time

		s.s_total = max(s.s_total, pe -> stats.s_total); 	// max cpu run time
		s.s_total_all = s.s_total;				// total cpu run time -> Bug here: s_total will be max but since PE / Thread 
									// is always 1 it does not matter
									// conforms: perf stat -e cycles exe ..

		//if (me -> id == 0) fprintf(stderr, "t: %f, t_all: %f\n", (double) s.s_total, (double) s.s_total_all);		

		for(i = 0; i < g_tw_nkp; i++)
		{
			kp = tw_getkp(i);
			s.s_nevent_processed += kp -> s_nevent_processed; // Number of events processed
			s.s_e_rbs += kp -> s_e_rbs;			  // Number of rollbacks
			
			#ifdef rollback_stats
     			s.s_remote_rbs+=kp->s_remote_rbs;
			s.s_regional_rbs+=kp->s_regional_rbs;	
			s.s_rb_cause_regional += kp->s_rb_cause_regional;
			s.s_rb_cause_remote += kp->s_rb_cause_remote;
			#endif
			
			s.s_rb_total += kp -> s_rb_total;
			s.s_rb_secondary += kp -> s_rb_secondary;
		}

		for(i = 0; i < g_tw_nlp; i++)
		{
			lp = tw_getlp(i);
			if (lp->type.final) (*lp->type.final) (lp->cur_state, lp);
		}


		s.s_event_process += pe->stats.s_event_process; // CPU time for event processing
		s.s_pq += pe->stats.s_pq;
		s.s_rollback += pe->stats.s_rollback;
		s.s_cancel_q += pe->stats.s_cancel_q;
		s.s_event_abort += pe->stats.s_event_abort;

		s.s_net_read += pe->stats.s_net_read; 	// CPU time for core simulation tasks
		s.s_gvt_func += pe -> stats.s_gvt_func;
		s.s_sched_event += pe -> stats.s_sched_event;
		s.s_batch_event += pe -> stats.s_batch_event;

		s.s_gvt += pe->stats.s_gvt;		// CPU time for GVT related tasks
		s.s_mpi_mattern_c1 += pe -> stats.s_mpi_mattern_c1;
		s.s_mpi_mattern_c2 += pe -> stats.s_mpi_mattern_c2;
		s.s_fossil_collect += pe->stats.s_fossil_collect;

		s.force_gvt += pe -> gvt_force;	// GVT related counters
		s.cm_check_fails += pe -> cm_check_fails;
		s.try_lock_fails += pe -> try_lock_fails;
		s.std_dev += pe -> std_dev;
	

		//s.s_nsend_net_remote += pe->stats.s_nsend_net_remote;

		// s.s_nsend_loc_remote += pe->stats.s_nsend_loc_remote;  // accumulated
		// s.s_nsend_loc += pe -> stats.s_nsend_loc;              // accumulated
		//s.s_nsend_regional += pe -> stats.s_nsend_regional;

		//s.s_nsend_network += pe->stats.s_nsend_network;
		//s.s_nread_network += pe->stats.s_nread_network;
		
		//s.s_cancel += pe->stats.s_cancel;

		//s.s_local_cancel += pe->stats.s_local_cancel;
		//s.s_cancel_regional += pe->stats.s_cancel_regional;
		//s.s_cancel_remote += pe->stats.s_cancel_remote;
		
		//s.s_nsend_remote_rb += pe->stats.s_nsend_remote_rb;

		/*
		s.s_pq_qsize += tw_pq_get_size(me->pq);

		#ifdef rollback_stats
    			s.s_net_remote += pe->stats.s_net_remote;
    			s.s_net_regional += pe->stats.s_net_regional;
		#endif
		
		s.s_thread_barrier += pe -> stats.s_thread_barrier;
		s.s_mpi_barrier += pe -> stats.s_mpi_barrier;

		s.s_event_send += pe -> stats.s_event_send;
		s.s_regional_send += pe -> stats.s_regional_send;
		s.s_remote_send += pe -> stats.s_remote_send;
		s.s_regional_read += pe -> stats.s_regional_read;
		s.s_remote_read += pe -> stats.s_remote_read;

		s.s_send_slower_local += pe -> stats.s_send_slower_local;
		s.s_send_faster_local += pe -> stats.s_send_faster_local;
	
  		s.s_remote_read_test_send += pe -> stats.s_remote_read_test_send;
  		s.s_remote_read_test_recv += pe -> stats.s_remote_read_test_recv;
 		s.s_remote_read_irecv += pe -> stats.s_remote_read_irecv;
  		s.s_remote_read_isend += pe -> stats.s_remote_read_isend;
	
		*/
	}

	s.s_net_events = s.s_nevent_processed - s.s_e_rbs;  // number of committed events, accumulated

	//s.s_fc_attempts = g_tw_fossil_attempts;
	//s.s_rb_primary = s.s_rb_total - s.s_rb_secondary;

	s = * (tw_net_statistics(me, &s)); // s is thread local now, allreduce it per machine first, then between machines
	
	if (!tw_ismaster()) return;

	// fprintf(stderr, "%lu printing stats\n", me -> id); // Just the me -> id == 0 thread prints

#ifndef ROSS_DO_NOT_PRINT
	fprintf(stderr, "\n\n");
	show_4f("Max Wall Clock Run Time per Thread (sec) ", (double) s.s_max_run_time);  // wall clock run time (max)
	show_4f("Max CPU Run Time per Thread (sec) ", (double) s.s_total / g_tw_clock_rate);   // cpu run time (max) 
	show_4f("Total CPU Run Time (sec) ", (double) s.s_total_all / g_tw_clock_rate);   // cpu run time (total)

	printf("\nTW Library Statistics:\n");
	show_lld("Total Events Processed", s.s_nevent_processed); // number of total events 
	show_lld("Events Rolled Back", s.s_e_rbs);		// number of rollbacks
	show_lld("Net Events Processed", s.s_net_events); // number of committed events
	
	printf("\n");
        double rate = ((double) s.s_net_events / s.s_max_run_time);
	show_1f("Event Rate (net events / sec)", rate);

	double eff = 100.0 * ((double) s.s_net_events / (double) s.s_nevent_processed);
	show_2f("Efficiency", eff);
	//show_4f("Disparity", (double) s.std_dev / (double) g_tw_gvt_done);
	show_4f("Disparity (Single Node)", (double) me -> std_dev / (double) g_tw_gvt_done); // Just me -> id == 0 has it

#ifdef ROSS_GVT_mattern_orig
	printf("\nMattern GVT Statistics (per GVT round):\n");
	show_4f("Forced GVTs", (double) s.force_gvt / (double) g_tw_gvt_done);
	show_4f("CM Access Fails", (double) s.cm_check_fails / (double) g_tw_gvt_done);
	show_4f("Try Lock Fails", (double) s.try_lock_fails / (double) g_tw_gvt_done);
#endif

#ifdef print
	/*
	printf("\n");
	show_lld("Total Roll Backs ", s.s_rb_total);
	show_lld("Primary Roll Backs ", s.s_rb_primary);
	show_lld("Secondary Roll Backs ", s.s_rb_secondary);
	show_lld("Events Aborted (part of RBs)", s.s_nevent_abort);
	show_lld("Fossil Collect Attempts", s.s_fc_attempts);
	show_lld("Total GVT Computations", g_tw_gvt_done);
	*/

	/*
	printf("\nTW Communication Statistics:\n");
	
	double loc = s.s_nsend_loc + s.s_nsend_loc_remote;
	double eff_local = 0;
	if (loc) eff_local = (loc - s.s_local_cancel) / loc;

	show_lld("Local Events Sent", loc); // accumulated
	show_lld("Local Events Canceled", s.s_local_cancel);
	show_2f("Efficiency", 100.0 * eff_local);

	double eff_regional = 0;
	if (s.s_nsend_regional) eff_regional = (s.s_nsend_regional - s.s_cancel_regional) / (double) s.s_nsend_regional;

	printf("\n");
	show_lld("Regional Events Sent", s.s_nsend_regional);
	show_lld("Regional Events Canceled", s.s_cancel_regional);
	show_2f("Efficiency", 100.0 * eff_regional);

	double eff_remote = 0;
	if (s.s_nsend_network) eff_remote = (s.s_nsend_network - s.s_cancel_remote) / (double) s.s_nsend_network;
	
	printf("\n");
	show_lld("Remote Events Sent", s.s_nsend_network);
	show_lld("Remote Events Canceled", s.s_cancel_remote);
	show_2f("Efficiency", 100.0 * eff_remote);
	
	double eff_network = 0;
	if (s.s_nread_network) eff_network = s.s_nsend_net_remote / (double) s.s_nread_network;

	printf("\n");
	show_lld("Network Events Sent", s.s_nread_network);
	show_lld("Network Events Canceled", s.s_cancel);
	show_2f("Efficiency", 100.0 * eff_network);
	*/

#ifdef EXPERIMENT
#ifdef ROSS_GVT_mattern_orig
    #ifdef FGVT
        FILE * fp = fopen("./output/fgvt_rates.txt", "a");
        fprintf(fp, "%f\n", rate);
        
	    //FILE * fp_2 = fopen("./output_m/efficiencies.txt", "a");
        //fprintf(fp_2, "%f\n", eff);
	
	    //FILE * fp_3 = fopen("./output_m/total_events_processed.txt", "a");
        //fprintf(fp_3, "%.0f\n", (double) s.s_nevent_processed);

        //FILE * fp_4 = fopen("./output_m/force_gvt.txt", "a");
        //fprintf(fp_4, "%f\n", (double) s.force_gvt / (double) g_tw_gvt_done);
	
        //FILE * fp_5 = fopen("./output_m/cm_check_fails.txt", "a");
        //fprintf(fp_5, "%f\n", (double) s.cm_check_fails / (double) g_tw_gvt_done);
	
        //FILE * fp_6 = fopen("./output_m/try_lock_fails.txt", "a");
        //fprintf(fp_6, "%f\n", (double) s.try_lock_fails / (double) g_tw_gvt_done);
        
	    //FILE * fp_7 = fopen("./output_m/disparity.txt", "a");
        //fprintf(fp_7, "%f\n", (double) s.std_dev / (double) g_tw_gvt_done);
    #else
        FILE * fp = fopen("./output/m_rates.txt", "a");
        fprintf(fp, "%f\n", rate);
    #endif
#endif

#ifdef ROSS_GVT_mpi_allreduce
	FILE * fp = fopen("./output/b_rates.txt", "a");
    fprintf(fp, "%f\n", rate);
        
	//FILE * fp_2 = fopen("./output_b/efficiencies.txt", "a");
    //fprintf(fp_2, "%f\n", eff);
	
	//FILE * fp_3 = fopen("./output_b/total_events_processed.txt", "a");
    //fprintf(fp_3, "%.0f\n", (double) s.s_nevent_processed);
       
	//FILE * fp_7 = fopen("./output_b/disparity.txt", "a");
    //fprintf(fp_7, "%f\n", (double) s.std_dev / (double) g_tw_gvt_done);

#endif
#endif

	/*
	printf("\nTW Memory Statistics:\n");
	show_lld("Events Allocated", g_tw_events_per_pe * g_tw_npe);
	show_lld("Memory Allocated", m_alloc / 1024);
	show_lld("Memory Wasted", m_waste / 1024);

	printf("\nTW Data Structure sizes in bytes (sizeof):\n");
	show_lld("PE struct", sizeof(tw_pe));
	show_lld("KP struct", sizeof(tw_kp));
	show_lld("LP struct", sizeof(tw_lp));
	show_lld("LP Model struct", lp->type.state_sz);
	show_lld("LP RNGs", sizeof(*lp->rng));
	show_lld("Total LP", sizeof(tw_lp) + lp->type.state_sz + sizeof(*lp->rng));
	show_lld("Event struct", sizeof(tw_event));
	show_lld("Event struct with Model", sizeof(tw_event) + g_tw_msg_sz);
	*/

#ifdef ROSS_timing
	printf("\nTW Clock Cycle Statistics (MAX values in secs at %1.4lf GHz):\n", g_tw_clock_rate / 1000000000.0);
	show_4f("Event Processing", (double) s.s_event_process / g_tw_clock_rate);
	show_4f("Priority Queue (enq/deq)", (double) s.s_pq / g_tw_clock_rate);
	show_4f("Event Cancel", (double) s.s_cancel_q / g_tw_clock_rate);
	show_4f("Primary Rollbacks", (double) s.s_rollback / g_tw_clock_rate);
	show_4f("Event Abort", (double) s.s_event_abort / g_tw_clock_rate);
	printf("\n");	
	show_4f("Event Read", (double) s.s_net_read / g_tw_clock_rate);
	show_4f("GVT Function", (double) s.s_gvt_func / g_tw_clock_rate);
	show_4f("Sched Event", (double) s.s_sched_event / g_tw_clock_rate);
	show_4f("Batch Event", (double) s.s_batch_event / g_tw_clock_rate);
	printf("\n");
	show_4f("GVT Round Total", (double) s.s_gvt / g_tw_clock_rate);
	show_4f("GVT Round Total / # GVTs", (double) (s.s_gvt / g_tw_gvt_done) / g_tw_clock_rate);
	
	#ifdef ROSS_GVT_mattern_orig
		show_4f("MPI allredduce cut 1 in GVT", (double) s.s_mpi_mattern_c1 / g_tw_clock_rate);
		show_4f("MPI allredduce cut 2 in GVT", (double) s.s_mpi_mattern_c2 / g_tw_clock_rate);
	#endif

	show_4f("Fossil Collect", (double) s.s_fossil_collect / g_tw_clock_rate);	

	/*
	printf("\n");
	show_4f("Event Send", (double) s.s_event_send / g_tw_clock_rate);
	show_4f("Faster Local Send", (double) s.s_send_faster_local / g_tw_clock_rate);
	show_4f("Slower Local Send", (double) s.s_send_slower_local / g_tw_clock_rate);
	show_4f("Regional Send", (double) s.s_regional_send / g_tw_clock_rate);
	show_4f("Remote Send", (double) s.s_remote_send / g_tw_clock_rate);
	printf("\n");
	show_4f("Event Read", (double) s.s_net_read / g_tw_clock_rate);
	show_4f("Regional Read", (double) s.s_regional_read / g_tw_clock_rate);
	show_4f("Remote Read", (double) s.s_remote_read / g_tw_clock_rate);
	show_4f("MPI Test (posted sends)", (double) s.s_remote_read_test_send / g_tw_clock_rate);
	show_4f("MPI Test (posted recvs)", (double) s.s_remote_read_test_recv / g_tw_clock_rate);
	show_4f("MPI ISend", (double) s.s_remote_read_isend / g_tw_clock_rate);
	show_4f("MPI IRecv", (double) s.s_remote_read_irecv / g_tw_clock_rate);
	*/
#endif

// print message
#ifdef Message_Aggregate
  	printf("Message Aggregate analysis\n");
	show_lld("Total number of messages in transit", total_message_number);
	show_lld("Total number of events in transit", total_event_number);
  	show_4f("Event/Message rate",(double)total_event_number/total_message_number); 
	
	show_lld("Total number of aggregated messages in transit", total_message_aggre_number);
 	show_4f("Aggregate rate",(double)total_message_aggre_number/total_message_number); 
#endif
	
// poll messages
#ifdef prob_stats
	printf("Message Probe Analysis\n");
	show_lld("Successful number of probes", total_sucessful_poll_messages);
	show_lld("Total number of probes", total_poll_messages);
	show_4f("Successful Probe rate",(double)total_sucessful_poll_messages/total_poll_messages); 
#endif

#ifdef rollback_stats
	printf("\nRollback Analysis\n");
	show_lld("Rollbacked events from remote event", s.s_remote_rbs);
	show_lld("Rollbacked events from regional event",s.s_regional_rbs);
	show_4f("Average rolled back events by remote",(double)s.s_remote_rbs/s.s_rb_cause_remote);
	show_4f("Average rolled back events by regional",(double)s.s_regional_rbs/s.s_rb_cause_regional);

	show_lld("Total Roll Backs caused by remote events ", s.s_rb_cause_remote);
	show_lld("Total Roll Backs caused by regional events ", s.s_rb_cause_regional);

	show_2f("Percent of Rollbacks from Remote Events", 
	( (double)s.s_rb_cause_remote / (double)(s.s_rb_cause_remote+ s.s_rb_cause_regional)) * 100.0);
#endif 

tw_gvt_stats(stdout);
#endif
#endif
}
