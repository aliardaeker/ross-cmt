#ifndef INC_gvt_mpi_allreduce_h
#define INC_gvt_mpi_allreduce_h

#ifdef EXPERIMENT
	//static tw_stime gvt_print_interval = 0.1;
#else
	//static tw_stime gvt_print_interval = 0.01;
#endif

//static tw_stime percent_complete = 0.0;
//int interval_counter;

static inline int 
tw_gvt_inprogress(tw_pe * pe)
{
	return pe->gvt_status;
}

static inline void 
gvt_print(tw_pe * me)
{
	/*
	if(gvt_print_interval == 1.0)
		return;

	if(percent_complete == 0.0)
	{
		percent_complete = gvt_print_interval;
		return;
	}
	*/

	printf("GVT #%d: simulation %d%% complete (",
		g_tw_gvt_done,
		(int) min(100, floor(100 * (me -> GVT / g_tw_ts_end))));

	if (me -> GVT == DBL_MAX) printf("GVT = %s", "MAX");
	else printf("GVT = %.4f", me -> GVT);

	printf(").\n");
	//percent_complete += gvt_print_interval;

    #ifdef disparity_check
	//static double std_avg = 0;
	
	if ((int) min(100, floor(100 * (me -> GVT / g_tw_ts_end))) < 100)
	{
		double avg = 0, total = 0, dist_to_mean = 0, std_dev = 0;

		for (int i = 1; i < g_tw_n_threads; i++) total += lvts[i].lvt;	
		avg = total / (g_tw_n_threads - 1);		
		
		for (int i = 1; i < g_tw_n_threads; i++) dist_to_mean += (lvts[i].lvt - avg) * (lvts[i].lvt - avg);
		std_dev = sqrt(dist_to_mean / (g_tw_n_threads - 1));	
		
		me -> std_dev += std_dev;

		//std_avg += std_dev;
		//fprintf(stderr, "Std dev: %.2f\n", std_dev);
	}
	//else fprintf(stderr, "\nStd dev avg: %.2f\n", std_avg / g_tw_gvt_done);
	#endif
}

#endif
