#include <ross.h>

static void dummy_pe_f (tw_pe *pe)
{
}

void
tw_pe_settype(tw_pe * pe, const tw_petype * type)
{
	if (!pe)
		tw_error(TW_LOC, "Undefined PE!");

#define copy_pef(f, d) \
		pe->type.f = type->f ? type->f : d

	copy_pef(pre_lp_init, dummy_pe_f);
	copy_pef(post_lp_init, dummy_pe_f);
	copy_pef(gvt, dummy_pe_f);
	copy_pef(final, dummy_pe_f);
        copy_pef(periodic, dummy_pe_f);

#undef copy_pef
}

tw_pe *
tw_pe_next(tw_pe * last)
{
	if (!last)
		return g_tw_pe[0];
	if (!last->pe_next)
		return NULL;
	return *last->pe_next;
}

void
tw_pe_create(tw_peid id)
{
	g_tw_npe = id;
	g_tw_pe = tw_calloc(TW_LOC, "PE Array", sizeof(*g_tw_pe), id);
}

/*
 * tw_pe_init: initialize individual PE structs
 *
 * id	-- local compute node g_tw_pe array index
 * gid	-- global (across all compute nodes) PE id
 */
void
tw_pe_init(tw_peid id, tw_peid gid)
{
	tw_pe *pe = tw_calloc(TW_LOC, "Local PE", sizeof(*pe), 1);
	tw_petype no_type;
	
	memset(&no_type, 0, sizeof(no_type));

	pe->id = gid;
	memcpy(&pe->node, &g_tw_mynode, sizeof(tw_node));
	tw_pe_settype(pe, &no_type);

	pe->trans_msg_ts = DBL_MAX;
	pe->gvt_status = 0;

	if(id == g_tw_npe-1)
		pe->pe_next = NULL;
	else
		pe->pe_next = &g_tw_pe[id+1];

	if (g_tw_pe[id])
		tw_error(TW_LOC, "PE %u already initialized", pe->id);

	g_tw_pe[id] = pe;

	if(g_tw_rng_default == TW_TRUE)
		g_tw_rng_max = g_tw_nRNG_per_lp;

	g_tw_pe[id]->rng = tw_rand_init(31, 41);
}

void
tw_pe_fossil_collect(tw_pe * me)
{
	tw_kp	* kp = NULL;
	int	 i;

	g_tw_fossil_attempts++;
	//fprintf(stderr, "%lld\n", g_tw_nkp * no_threads);
	//fprintf(stderr, "%lld to fossil collect\n", me -> id);		

	#ifdef SL_LOCKS
		if (g_tw_tid != 1) tw_error(TW_LOC, "Streamlined fossil collect crashed !!!\n");
		int fossil_collect_for;	

		//if (!g_tw_pid) fprintf(stderr, "Fc\n");		
		//else fprintf(stderr, "	Fc\n");	

		// i = g_tw_nkp because thread 0 did not process any event
		for (i = (g_tw_nkp * 2); i < g_tw_nkp * no_threads; i++)
		{
			fossil_collect_for = i / g_tw_nkp;
		
			//if (!g_tw_pid) fprintf(stderr, "%u fetching kp of %d (%d)\n", g_tw_tid, fossil_collect_for, i);
			//else fprintf(stderr, "	%u fetching kp of %d (%d)\n", g_tw_tid, fossil_collect_for, i);
	
			kp = kp_list[i];
		
			pthread_mutex_lock(&kp_locks[fossil_collect_for]);
			tw_eventq_fossil_collect_for_all(&kp -> pevent_q, fossil_collect_for);
			pthread_mutex_unlock(&kp_locks[fossil_collect_for]);	
		
			//if (!g_tw_pid) fprintf(stderr, "%u: kp id %lld\n", g_tw_tid, kp -> id);
			//else fprintf(stderr, "	%u: kp id %lld \n", g_tw_tid, kp -> id);
		}
	
		//if (!g_tw_pid) fprintf(stderr, "Fc done\n");		
		//else fprintf(stderr, "	Fc done\n");	
	#elif defined(SL_BARRIER)
		//if (g_tw_tid != 1) tw_error(TW_LOC, "Streamlined fossil collect crashed !!!\n");
		if (g_tw_tid) tw_error(TW_LOC, "Streamlined fossil collect crashed !!!\n");
		
			//if (!g_tw_pid) fprintf(stderr, "Node %d eventq fc, GVT = %lf\n", g_tw_mynode, me -> GVT);
			//else fprintf(stderr, "	Node %d eventq fc, GVT = %lf\n", g_tw_mynode, me -> GVT);
			
		int fossil_collect_for;	
		//for (i = (g_tw_nkp * 2); i < g_tw_nkp * no_threads; i++)
		for (i = g_tw_nkp; i < g_tw_nkp * no_threads; i++)
		{
			fossil_collect_for = i / g_tw_nkp;
				
			//if (!g_tw_pid) fprintf(stderr, "Node %d eventq fc, GVT = %lf\n", g_tw_mynode, me -> GVT);
			//else fprintf(stderr, "	Node %d eventq fc, GVT = %lf\n", g_tw_mynode, me -> GVT);
				
			kp = kp_list[i];
				
			//if (!g_tw_pid) fprintf(stderr, "Node %d eventq fc done, GVT = %lf\n", g_tw_mynode, me -> GVT);
			//else fprintf(stderr, "	Node %d eventq fc done, GVT = %lf\n", g_tw_mynode, me -> GVT);

			tw_eventq_fossil_collect_for_all(&kp -> pevent_q, fossil_collect_for);
			
			//if (!g_tw_pid) fprintf(stderr, "Node %d eventq fc finish, GVT = %lf\n", g_tw_mynode, me -> GVT);
			//else fprintf(stderr, "	Node %d eventq fc finish, GVT = %lf\n", g_tw_mynode, me -> GVT);
		}
				
		//if (!g_tw_pid) fprintf(stderr, "Node %d eventq fc done, GVT = %lf\n", g_tw_mynode, me -> GVT);
		//else fprintf(stderr, "	Node %d eventq fc done, GVT = %lf\n", g_tw_mynode, me -> GVT);
	#else
		//if (!g_tw_pid) fprintf(stderr, "Node %d eventq fc, GVT = %lf\n", g_tw_mynode, me -> GVT);
		//else fprintf(stderr, "	Node %d eventq fc, GVT = %lf\n", g_tw_mynode, me -> GVT);

		for (i = 0; i < g_tw_nkp; i++)
		{
			kp = tw_getkp(i);
		
			//fprintf(stderr, "%llu`s kp is %lld\n", me -> id, kp -> id);		
			tw_eventq_fossil_collect(&kp -> pevent_q, me);

			#ifdef ROSS_MEMORY
				tw_kp_fossil_memory(kp);
			#endif
		}

		#ifdef ROSS_NETWORK_tcp
			tw_eventq_fossil_collect(&me->sevent_q, me);
		#endif
	#endif
}

#ifdef ROSS_MEMORY
static int next_mem_q = 0;

tw_fd
tw_pe_memory_init(tw_pe * pe, size_t n_mem, size_t d_sz, tw_stime mult)
{
	tw_memoryq	*q;

	int             fd;

	if(!pe->memory_q)
		pe->memory_q = tw_calloc(TW_LOC, "KP memory queues",
					sizeof(tw_memoryq), g_tw_memory_nqueues);

	fd = next_mem_q++;
	pe->stats.s_mem_buffers_used = 0;

	q = &pe->memory_q[fd];

	q->size = 0;
	q->start_size = n_mem;
	q->d_size = d_sz;
	q->grow = mult;

	tw_memory_allocate(q);

	return fd;
}
#endif
