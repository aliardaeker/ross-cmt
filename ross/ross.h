#ifndef INC_ross_h
#define INC_ross_h

//jwang

//#define Message_Aggregate

//#define prob_opt    // infrequent polling 

//#define OptMess // dynamical message size

//#define MemoryPool   // grab event from memory pool

//#define prob_stats   // printf probing information

/** @mainpage Rensselaer's Optimistic Simulation System (ROSS)
    @section intro_sec Introduction
    
    ROSS is an acronym for Rensselaer's Optimistic Simulation System. It is a
    parallel discrete-event simulator that executes on shared-memory
    multiprocessor systems. ROSS is geared for running large-scale simulation
    models (i.e., 100K to even 1 million object models).  The synchronization
    mechanism is based on Time Warp. Time Warp is an optimistic
    synchronization mechanism develop by Jefferson and Sowizral [10, 11] used
    in the parallelization of discrete-event simulation. The distributed
    simulator consists of a collection of logical processes or LPs, each
    modeling a distinct component of the system being modeled, e.g., a server
    in a queuing network. LPs communicate by exchanging timestamped event
    messages, e.g., denoting the arrival of a new job at that server.

    The Time Warp mechanism uses a detection-and-recovery protocol to
    synchronize the computation. Any time an LP determines that it has
    processed events out of timestamp order, it "rolls back" those events, and
    re-executes them. For a detailed discussion of Time Warp as well as other
    parallel simulation protocols we refer the reader to [8]

    ROSS was modeled after a Time Warp simulator called GTW or Georgia Tech
    Time Warp[7]. ROSS helped to demonstrate that Time Warp simulators can be
    run efficiently both in terms of speed and memory usage relative to a
    high-performance sequential simulator.

    To achieve high parallel performance, ROSS uses a technique call Reverse
    Computation. Here, the roll back mechanism in the optimistic simulator is
    realized not by classic state-saving, but by literally allowing to the
    greatest possible extent events to be reverse. Thus, as models are
    developed for parallel execution, both the forward and reverse execution
    code must be written. Currently, both are done by hand. We are
    investigating automatic methods that are able to generate the reverse
    execution code using only the forward execution code as input. For more
    information on ROSS and Reverse Computation we refer the interested reader
    to [4, 5]. Both of these text are provided as additional reading in the
    ROSS distribution.

@section license_sec License
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:
  
    1. Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
  
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
  
    3. All advertising materials mentioning features or use of this software
       must display the following acknowledgement:
  
        This product includes software developed by Mark Anderson, David Bauer,
        Dr. Christopher D. Carothers, Justin LaPre, and Shawn Pearce of the
        Department of Computer Science at Rensselaer Polytechnic
        Institute.
 
    4. Neither the name of the University nor of the developers may be used
       to endorse or promote products derived from this software without
       specific prior written permission.
  
    5. The use or inclusion of this software or its documentation in
       any commercial product or distribution of this software to any
       other party without specific, written prior permission is
       prohibited.
 
    THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS
    IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
    REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
    STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
 
    Copyright (c) 1999-2010 Rensselaer Polytechnic Institute.
    All rights reserved.
*/
// ROSS-MT versions
//#define COND_VAR
#define BARRIER

#define EXPERIMENT	    // Enables printing the rate
//#define DEBUG_ALI  	// For debugging network_mpi
//#define DEBUG_M    	// For debugging mattern_orig and barrier

//#define SIM_SLOW	        // Sets batch size to 1 and LPs to 1 so we can test with minimum number of messages
//#define blocking_MPI 	    // If on MPI_SEND, MPI_RECV, if not MPI_ISEND, MPI_IRECV (Do not trust the prints)
//#define rollback_stats 
#define print

//#define IMBALANCED_DEST       // LPs with smaller ids get more messages than others while eveybody might send to everybody
//#define IMBALANCED_EPG	    // LPs get randomly (uniform) process more EPG (+ 0 .. EPG / 8)
//#define switch_sim_mode 	    // Simulation switches between computation dominated and communication dominated scenerios on the fly 
#define disparity_check	    // LPs report their LVT and disparity is calculated by taking the mean of standard deviations of

//#define FGVT        // Just prints into fgvt_rate.txt. Actual fgvt implementation should be copied into gvt_orig.c

//#define SGVT		// Selectivly apply high EPC, spin wait or some ways of delays to LP which are further away
			        // from others so that disparity would be reduced.

#define SEPARATE_0	// Separates thread 0 from event receiving, processing and sending.
			        // For streamlining the mpi communication in Network and GVT.

//#define SEPARATE_1	// Separates thread 1 from event receiving, processing and sending.
			            // SEPARATE_0 should be on for this to be on.

//#define SL_LOCKS	// For streamlining the fossil collection
			        // Thread 1 fossil collects for all other threads using array of locks. 
			        // SEPARATE_1 should be on for this to be on. Works when g_tw_nkp = 1 only.

//#define SL_BARRIER    // For streamlining the fossil collection
			            // Thread 0 fossil collects for all other threads using barrier. 
			            // SEPARATE_0 should be on for this to be on. Works for any g_tw_nkp.

//#define SL_GVT  	// For streamlining the GVT computation.
			        // Thread 0 computes GVT for all other threads.
			        // SEPARATE_0 should be on for this to be on.

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) ( sizeof((a)) / sizeof((a)[0]) )
#endif

#ifdef __GNUC__
#  define NORETURN __attribute__((__noreturn__))
#else
#  define NORETURN
#  ifndef __attribute__
#    define __attribute__(x)
#  endif
#endif

/*********************************************************************
 *
 * Include ``standard'' headers that most of ROSS will require.
 *
 ********************************************************************/

#include <errno.h>
#include <sys/types.h>
#include <math.h>
#include <limits.h> 
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>

#if !defined(DBL_MAX)
#include <float.h>
#endif

#include <sys/time.h>
#include <time.h>

#ifdef ROSS_INTERNAL
#undef malloc
#undef calloc
#undef realloc
#undef strdup
#undef free

#  define malloc(a) must_use_tw_calloc_not_malloc
#  define calloc(a,b) must_use_tw_calloc_not_calloc
#  define realloc(a,b) must_use_tw_calloc_not_realloc
#  define strdup(b) must_use_tw_calloc_not_strdup
#  define free(b) must_not_use_free
#endif
//DJ:
//#define NUMA_NODE_SIZE 256
//#define MAX_NUMA_NODES 8
//#define MAX_NUMA_NODES 16 // For process per node > 1
//#define MAX_NUMA_NODES 32 // For process per node > 1
//#define MAX_NUMA_NODES 64 // For process per node > 1
//#define MAX_NUMA_NODES 256 // For process per node > 1: no_machines (4) * processes_per_node (64)
#define MAX_NUMA_NODES 512 // For process per node > 1: no_machines (8) * processes_per_node (64)

//jwang
#define Thread_Number  256    // thread number for each host  
#define Machines  8  // number of machines used 

#define event_Group 10// the number of events sent together

#define buffer_size (4+136*event_Group)   // for phold       

#define poll_period 4  // polling period (only when infrequent polling is enabled) 

//DJ
/* tw_peid -- Processing Element "PE" id */
typedef unsigned long long tw_peid;

/* tw_stime -- Simulation time value for sim clock (NOT wall!) */
typedef double tw_stime;

/* tw_lpid -- Logical Process "LP" id */
#if defined(ARCH_bgl) || defined(ARCH_bgp)
typedef unsigned long long tw_lpid;
#else
//typedef unsigned long long tw_lpid;
typedef uintptr_t tw_lpid;
#endif

#include "ross-random.h"

#ifdef ROSS_RAND_clcg4
#  include "rand-clcg4.h"
#endif

#ifdef ROSS_CLOCK_i386
#  include "clock-i386.h"
#endif
#ifdef ROSS_CLOCK_amd64
#  include "clock-amd64.h"
#endif
#ifdef ROSS_CLOCK_ia64
#  include "clock-ia64.h"
#endif
#ifdef ROSS_CLOCK_ppc
#  include "clock-ppc.h"
#endif
#ifdef ROSS_CLOCK_bgl
#  include "clock-bgl.h"
#endif
#ifdef ROSS_CLOCK_none
#  include "clock-none.h"
#endif

#ifdef ROSS_NETWORK_none
#  include "network-none1.h"
#endif
#ifdef ROSS_NETWORK_mpi
#  include "network-mpi1.h"
#endif
#ifdef ROSS_NETWORK_tcp
#  include "network-tcp1.h"
#endif

#include "tw-timing.h"
#include "ross-types.h"
#include "tw-timer.h"
#include "tw-opts.h"
#include "ross-network.h"
#include "ross-gvt.h"
#include "ross-extern.h"
#include "ross-kernel-inline.h"
#include "hash-quadratic.h"

#ifdef ROSS_NETWORK_none
#  include "network-none2.h"
#endif
#ifdef ROSS_NETWORK_mpi
#  include "network-mpi2.h"
#endif
#ifdef ROSS_NETWORK_tcp
#  include "network-tcp2.h"
#  include "socket-tcp.h"
#  include "hash-quadratic.h"
#endif

#ifdef ROSS_GVT_none
#  include "gvt-none.h"
#endif
#ifdef ROSS_GVT_7oclock
#  include "gvt-7oclock.h"
//jwang
#  include "mpi.h"

#endif
#ifdef ROSS_GVT_mpi_allreduce
#  include "mpi.h"
#  include "gvt-mpi_allreduce.h"
#endif

#ifdef ROSS_GVT_mattern_orig
#  include "mpi.h"
#  include "gvt-mattern_orig.h"
#endif

#ifdef ROSS_MEMORY
#  include "tw-memoryq.h"
#  include "tw-memory.h"
#endif

#include "tw-eventq.h"
#include "ross-inline.h"
#include "mt_barrier_sync.h"
#endif
