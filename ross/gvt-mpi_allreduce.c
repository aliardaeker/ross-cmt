#include <ross.h>

#define TW_GVT_NORMAL 0
#define TW_GVT_COMPUTE 1

static __thread unsigned int gvt_cnt = 0;
static __thread unsigned int gvt_force = 0;
static __thread tw_stat all_reduce_cnt = 0;

static const tw_optdef gvt_opts [] =
{
	TWOPT_GROUP("ROSS MPI GVT"),
	TWOPT_UINT("gvt-interval", g_tw_gvt_interval, "GVT Interval"),
	//TWOPT_STIME("report-interval", gvt_print_interval, "percent of runtime to print GVT"),
	TWOPT_END()
};

#ifdef SL_BARRIER	
pthread_barrier_t fossil_barrier;
#endif

static unsigned int g_tw_gvt_max_no_change = 10000;
static __thread unsigned int g_tw_gvt_no_change = 0;

void
tw_gvt_step2(tw_pe * me)
{
	if (me -> gvt_status != TW_GVT_COMPUTE) return;

	tw_clock start_total = tw_clock_read();
	
	tw_stat local_white = 0;
	tw_stat total_white = 0;
	tw_stat node_total_white = 0;

	tw_stime lvt = DBL_MAX;
	tw_stime gvt = DBL_MAX;
	tw_stime node_gvt = DBL_MAX;
	//tw_clock s, thread_b, mpi_b;

	#ifdef DEBUG_M
	if (!g_tw_pid) fprintf(stderr, "%u`s gvt %.0f\n", g_tw_tid, me -> GVT);
	else fprintf(stderr, "	%u`s gvt %.0f\n", g_tw_tid, me -> GVT);
	#endif
	
	while (1)
	{
	  	//s = tw_clock_read();
	    	tw_net_read(me);
	  	//me -> stats.s_net_read += tw_clock_read() - s;
      
		// send message counts to create consistent cut
		local_white = me -> s_nwhite_sent - me -> s_nwhite_recv;
		all_reduce_cnt++;
		
		// reduce local_white with sum, map it to total_white
	  	//thread_b = tw_clock_read();
		mt_allreduce_long_sum(&local_white, &total_white, &sum_barrier[sum_index]);
	  	//me -> stats.s_thread_barrier += tw_clock_read() - thread_b;
		
		sum_index = (sum_index + 1) % 2;

		//BJW RIGHT HERE!!!!!
		if (g_tw_tid == 0)
		{
			//printf("Before MPI_Allreduce: node=%d\n",g_tw_mynode);
	  		//mpi_b = tw_clock_read();
	        	if(MPI_Allreduce(
			     &total_white, // reduce total_white accross machines with sum
			     &node_total_white, // map it to node_total_white
			     1,
			     MPI_LONG_LONG,
			     MPI_SUM,
			     MPI_COMM_WORLD) != MPI_SUCCESS) tw_error(TW_LOC, "MPI_Allreduce for GVT failed");
	  		//me -> stats.s_mpi_barrier += tw_clock_read() - mpi_b;
		}
		else node_total_white = 0;
		
		// reduce node_total_white with sum and map it to total_white
		// just main thread`s node_total_white is reduced since others have 0 anyway.
		// like a broadcast operation
	  	//thread_b = tw_clock_read();
		mt_allreduce_long_sum_2(g_tw_tid, &node_total_white, &total_white, &sum_barrier[sum_index]);
	  	//me -> stats.s_thread_barrier += tw_clock_read() - thread_b;
		sum_index = (sum_index + 1) % 2;
		
		// Ensures no intransit message accross any thread and machines
		if (total_white == 0) break;
	}

	#ifdef DEBUG_M
	if (!g_tw_pid) fprintf(stderr, "%u`s done while %.0f\n", g_tw_tid, me -> GVT);
	else fprintf(stderr, "	%u done while  %.0f\n", g_tw_tid, me -> GVT);
	#endif
	
	#ifdef SEPARATE_1
		if (g_tw_tid > 1) lvt = min_pq_outq(me);
	#elif defined(SEPARATE_0)
		if (g_tw_tid) lvt = min_pq_outq(me);
	#else 
		lvt = min_pq_outq(me);
	#endif

	all_reduce_cnt++;
	
	// reduce lvt with min and map it to gvt
	//thread_b = tw_clock_read();
	mt_allreduce_double_min(&lvt, &gvt, &min_barrier[min_index]);	
	//me -> stats.s_thread_barrier += tw_clock_read() - thread_b;
	
	min_index = (min_index + 1) % 2;

	//BJW RIGHT HERE!!!!!
	if (g_tw_tid == 0)
	{
	  	//mpi_b = tw_clock_read();
		if(MPI_Allreduce(
			&gvt,       // reduce gvt with min 
			&node_gvt,  // map it to node gvt
			1,
			MPI_DOUBLE,
			MPI_MIN,
			MPI_COMM_WORLD) != MPI_SUCCESS)
			tw_error(TW_LOC, "MPI_Allreduce for GVT failed"); 
	  	//me -> stats.s_mpi_barrier += tw_clock_read() - mpi_b;
	
	    // now just thread 0 has the correct gvt in its local gvt
	}
	else node_gvt = DBL_MAX;	
	
	
	// reduce node gvt in main thread with min and map it to gvt to let other threads take it
	//thread_b = tw_clock_read();
	mt_allreduce_double_min_2(g_tw_tid, &node_gvt, &gvt, &min_barrier[min_index]);	
	//me -> stats.s_thread_barrier += tw_clock_read() - thread_b;
	
	min_index = (min_index + 1) % 2;

	gvt = min(gvt, me -> GVT_prev);

	if (gvt != me->GVT_prev) g_tw_gvt_no_change = 0;
	else
	{
		g_tw_gvt_no_change++;
		if (g_tw_gvt_no_change >= g_tw_gvt_max_no_change) 
		{
			tw_error(
				TW_LOC,
				"GVT computed %d times in a row"
				" without changing: GVT = %14.14lf, PREV %14.14lf"
				" -- GLOBAL SYNCH -- out of memory!",
				g_tw_gvt_no_change, gvt, me->GVT_prev);
		}
	}

	if (me->GVT > gvt) tw_error(TW_LOC, "PE %u GVT decreased %g -> %g", me->id, me->GVT, gvt);

	//if (gvt / g_tw_ts_end > percent_complete && tw_node_eq(&g_tw_mynode, &g_tw_masternode)) gvt_print(gvt);
	if (me -> id == 0) gvt_print(me);

	me -> s_nwhite_sent = 0;
	me -> s_nwhite_recv = 0;
	me -> trans_msg_ts = DBL_MAX;
	me -> GVT_prev = DBL_MAX; // me->GVT;
	me -> GVT = gvt;
	me -> gvt_status = TW_GVT_NORMAL;

	//if (!g_tw_pid) report_lvts(me); 

	//if (!g_tw_pid) printf("Node = %d GVT = %lf\n", g_tw_mynode, gvt);
	//else printf("	Node = %d GVT = %lf\n", g_tw_mynode, gvt);

	// update GVT timing stats
	gvt_cnt = 0;

	// only FC if OPTIMISTIC
	if (g_tw_synchronization_protocol == OPTIMISTIC)
	{
		tw_clock start = tw_clock_read();
		fossil_collect(me);		
		me -> stats.s_fossil_collect += tw_clock_read() - start;
	}

	g_tw_gvt_done++;
	me -> stats.s_gvt += tw_clock_read() - start_total;
}

void
report_lvts(tw_pe * me)
{
        //fprintf(stderr, "%u: LVT: %.0f, trans_msg: %.0f, net_min: %.0f, pq_min: %.0f\n", 
        //		g_tw_tid, me -> LVT, me -> trans_msg, me -> net_min, me -> pq_min);

	//lvt_array[g_tw_gvt_done].lvt[g_tw_tid] = me -> LVT;
}


const tw_optdef *
tw_gvt_setup(void)
{
	#ifdef SL_BARRIER
		pthread_barrier_init(&fossil_barrier, NULL, no_threads);
	#endif

	gvt_cnt = 0;
	return gvt_opts;
}

void
tw_gvt_start(void) {}

void
tw_gvt_force_update(tw_pe *me)
{
	gvt_force++;
	gvt_cnt = g_tw_gvt_interval;
}

void
tw_gvt_stats(FILE * f)
{
	fprintf(f, "\nTW GVT Statistics: MPI AllReduce\n");
	fprintf(f, "\t%-50s %11d\n", "GVT Interval", g_tw_gvt_interval);
	fprintf(f, "\t%-50s %11d\n", "Batch Size", g_tw_mblock);
	//fprintf(f, "\n");
	fprintf(f, "\t%-50s %11d\n", "Forced GVT", gvt_force);
	fprintf(f, "\t%-50s %11d\n", "Total GVT Computations", g_tw_gvt_done);
	fprintf(f, "\t%-50s %11lld\n", "Total All Reduce Calls", all_reduce_cnt);
	fprintf(f, "\t%-50s %11.2lf\n", "Average Reduction / GVT", (double) ((double) all_reduce_cnt / (double) g_tw_gvt_done));
}

void
tw_gvt_step1(tw_pe *me)
{
	if (me->gvt_status == TW_GVT_COMPUTE || ++gvt_cnt < g_tw_gvt_interval) return;
	me->gvt_status = TW_GVT_COMPUTE;
	//printf("Start GVT:  node=%d\n",g_tw_mynode);
}
tw_stime
min_pq_outq(tw_pe * me)
{
	// tw_net_read(me); // might be neccessary

	// return the minimum of pq of PE	
	tw_stime pq_min = tw_pq_minimum(me -> pq);
	me -> pq_min = pq_min;

	// return the minimum of outq and posted_sends of PE. Buffers for mpi.
	// populated during network_send, extracted during send_begin
	tw_stime net_min = tw_net_minimum(me);
	me -> net_min = net_min;

	tw_stime t_msg = me -> trans_msg_ts;
	me -> trans_msg = t_msg;
	
	double lvt = t_msg;

	if (lvt > pq_min) lvt = pq_min;
	if (lvt > net_min) lvt = net_min;
	
	if (lvt > g_tw_ts_end) 
	{
		me -> LVT = g_tw_ts_end + 2;
		return g_tw_ts_end + 2;
	}
	else 
	{
		me -> LVT = lvt;
		return lvt;
	}
}

void
fossil_collect(tw_pe * me)
{
	#ifdef SL_LOCKS
		//if (!g_tw_pid) fprintf(stderr, "Node %d enter, GVT = %lf\n", g_tw_mynode, gvt);
		//else fprintf(stderr, "	Node %d enter, GVT = %lf\n", g_tw_mynode, gvt);
		if (g_tw_tid == 1) tw_pe_fossil_collect(me);
	#elif defined(SL_BARRIER)
		//if (!g_tw_pid) fprintf(stderr, "Node %d fc, GVT = %lf\n", g_tw_mynode, me -> GVT);
		//else fprintf(stderr, "	Node %d fc, GVT = %lf\n", g_tw_mynode, me -> GVT);

		//pthread_barrier_wait(&fossil_barrier);
		//if (g_tw_tid == 1)  tw_pe_fossil_collect(me);
		
		if (!g_tw_tid)  tw_pe_fossil_collect(me);
		pthread_barrier_wait(&fossil_barrier);
		
		//if (!g_tw_pid) fprintf(stderr, "Node %d fc done 2, GVT = %lf\n", g_tw_mynode, me -> GVT);
		//else fprintf(stderr, "	Node %d fc done 2, GVT = %lf\n", g_tw_mynode, me -> GVT);
			
	#elif defined(SEPARATE_1)
		if (g_tw_tid > 1) tw_pe_fossil_collect(me);
	#elif defined(SEPARATE_0)
		//if (!g_tw_pid) fprintf(stderr, "Node %d enter, GVT = %lf\n", g_tw_mynode, me -> GVT);
		//else fprintf(stderr, "	Node %d enter, GVT = %lf\n", g_tw_mynode, me -> GVT);
		
		if (g_tw_tid) tw_pe_fossil_collect(me);
	#else
		tw_pe_fossil_collect(me);
	#endif
}
