#include "phold.h"
#include <pthread.h>
#include <mpi.h>

tw_peid
phold_map(tw_lpid gid)
{
	//jwang
	#ifdef New_map
		return (tw_peid) peid_of_lp[gid];
	#else
		return (tw_peid) gid / g_tw_nlp;
	#endif
}

void
phold_init(phold_state * s, tw_lp * lp)
{
	#ifdef SEPARATE_1
	if (g_tw_tid < 2) tw_error(TW_LOC, "Sending events to its own, %ld\n", lp -> gid);
	#endif

	#ifdef SEPARATE_0
	if (g_tw_tid < 1) tw_error(TW_LOC, "Sending events to its own, %ld\n", lp -> gid);
	#endif
	
	int              i;
	if (stagger)
	{
	    for (i = 0; i < g_phold_start_events; i++)
	    {
                tw_event *e = tw_event_new(lp->gid,
                        tw_rand_exponential(lp->rng, mean) + my_lookahead +
                        (tw_stime)(lp->gid % (unsigned int)g_tw_ts_end), lp);

		tw_event_send(e);
	    }
	}
	else
	{
	    for (i = 0; i < g_phold_start_events; i++)
	    {
                tw_event *e = tw_event_new(lp->gid,
                        tw_rand_exponential(lp->rng, mean) + my_lookahead, lp);
		
		tw_event_send(e);
	    }
	}
}

void
phold_event_handler(phold_state * s, tw_bf * bf, phold_message * m, tw_lp * lp)
{ 
	// Do not optimize, do not put in the registers, go to memory at each access
	volatile double result = 0;

	//unsigned long long tsc = get_tsc();
	#ifdef IMBALANCED_EPG
		int add = tw_rand_integer(lp -> rng, 0, epg / 8);
		for (int i = 0; i < epg + add; i++)
		{
        	       	result = result + (i * 315789) / 12345;
        	       	result += 30000;
        	}
	#else
        	for (int i = 0; i < epg; i++)
		    {
                	result = result + (i * 315789) / 12345;
                	result += 30000;
        	}
	#endif
	//unsigned long long tsc_end = get_tsc();
	//tsc = tsc_end - tsc;	
	//fprintf(stderr, "Clock Cycles: %llu, Cycle / Frequency: 
	//	%Lf\n", tsc, (long double) tsc / 1030000000.0); or use g_tw_clock_rate (returns seconds)

	tw_lpid	 dest;
	tw_event * e = NULL;
	
	tw_stime percent = tw_rand_unif(lp->rng);
	int bottom = nlp_per_pe * no_threads;

	
	#ifdef SEPARATE_0
        int machine_no;
        int lp_no;
    #endif

	// Remote Events
	if(percent <= percent_remote)
        { 
		#ifdef SEPARATE_1	
			//dest = tw_rand_integer(lp -> rng,  bottom + (nlp_per_pe * 2), ttl_lps - 1);
		#elif defined(SEPARATE_0)
		    machine_no = tw_rand_integer(lp -> rng, g_tw_pid + 1, g_tw_pid + g_tw_machines - 1);
                        if (machine_no >= g_tw_machines) machine_no -= g_tw_machines;
 
                        lp_no = tw_rand_integer(lp -> rng, nlp_per_pe, nlp_per_pe * no_threads - 1);
                        dest = (no_threads * nlp_per_pe * machine_no) + lp_no;
		#else
                        dest = tw_rand_integer(lp -> rng,  bottom, ttl_lps - 1);
 
                        dest += (lp -> gid / bottom) * bottom;
                        if (dest >= ttl_lps) dest -= ttl_lps;
		#endif
	     	
		// For imbalanced models: LPs with smaller ids get more messages
		#ifdef IMBALANCED_DEST
		int rand = tw_rand_integer(lp -> rng, 0, 1);
		if (rand) 
		{
			int dest_id = dest / bottom;
			//fprintf(stderr, "dest id: %d\n", dest_id);
			
			int moved = dest - dest_id * bottom;
			//fprintf(stderr, "moved: %d\n", moved);

			int last = (moved / nlp_per_pe) / 2;
			//fprintf(stderr, "last: %d\n", last);

			dest -= last * nlp_per_pe;
		}
		#endif		
		
		//printf("Remote Destination = %lu, lp -> gid = %lu\n", dest, lp -> gid);
        	bf -> c1 = 1;  
	} 
	// Local or regional events
	else if (percent <= (percent_remote + percent_regional))
	{
		#ifdef SEPARATE_1
	    		//dest = tw_rand_integer(lp -> rng,
      	        	//	(lp -> gid / bottom) * bottom + (nlp_per_pe * 2),
                 	//	(lp -> gid / bottom) * bottom + bottom - 1);
		#elif defined(SEPARATE_0)
	    		dest = tw_rand_integer(lp -> rng,
      	        		(lp -> gid / bottom) * bottom + nlp_per_pe,
                 		(lp -> gid / bottom) * bottom + bottom - 1);
		#else
	    		dest = tw_rand_integer(lp -> rng,
      	        		(lp -> gid / bottom) * bottom,
                        	(lp -> gid / bottom) * bottom + bottom - 1);
		#endif

		// For imbalanced models: LPs with smaller ids get more messages
		#ifdef IMBALANCED_DEST
		int rand = tw_rand_integer(lp -> rng, 0, 1);
		if (rand) 
		{	
			int moved = dest - (lp -> gid / bottom) * bottom;
			//fprintf(stderr, "moved: %d\n", moved);
		
			int last = (moved / nlp_per_pe) / 2;
			//fprintf(stderr, "last: %d\n", last);
		
			dest -= last * nlp_per_pe;
		}
		#endif		
            	
		//printf("%d.%d Local Destination = %lu, rand = %d\n", g_tw_pid, g_tw_tid, dest, rand);
	    	if (dest == lp -> gid) bf -> c1 = 0;
   	    	else bf -> c1 = 1;	
	}
        else
        {
                bf->c1 = 0;
                dest = lp -> gid;
        }


	//jwang
	#ifndef New_map
		if(dest < 0 || dest >= (g_tw_nlp * tw_nnodes()))
        	{
                	printf("\ng_tw_pid = %d, g_tw_tid = %d, dest = %lu\n", g_tw_pid, g_tw_tid, dest);  
                	//printf("gid = %d, dest = %d, g_tw_nlp = %d\n", lp->gid, dest, g_tw_nlp);
			tw_error(TW_LOC, "Bad Dest\n");
        	}
	#endif
	
	// dest lp id, offset, source lp (sender)
        //fprintf(stderr, "dest: %lu\n", dest);
	//fprintf(stderr, "rand exp: %f, mean: %f, lookahead: %f\n", tw_rand_exponential(lp -> rng, mean), mean, lookahead);
	e = tw_event_new(dest, tw_rand_exponential(lp->rng, mean) + my_lookahead, lp);
        
	tw_event_send(e);
}

void
phold_event_handler_rc(phold_state * s, tw_bf * bf, phold_message * m, tw_lp * lp)
{
	tw_rand_reverse_unif(lp->rng);
	tw_rand_reverse_unif(lp->rng);

	if(bf->c1 == 1)
		tw_rand_reverse_unif(lp->rng);
}

void
phold_finish(phold_state * s, tw_lp * lp)
{
}

tw_lptype       mylps[] = {
	{(init_f) phold_init,
	 (event_f) phold_event_handler,
	 (revent_f) phold_event_handler_rc,
	 (final_f) phold_finish,
	 (map_f) phold_map,
	sizeof(phold_state)},
	{0},
};

const tw_optdef app_opt[] =
{
	TWOPT_GROUP("PHOLD Model"),
	TWOPT_STIME("remote", percent_remote, "desired remote event rate"),
	TWOPT_UINT("nlp", nlp_per_pe, "number of LPs per processor"),
	TWOPT_STIME("mean", g_mean, "exponential distribution mean for timestamps"),
	TWOPT_STIME("mult", mult, "multiplier for event memory allocation"),
	TWOPT_STIME("lookahead", lookahead, "lookahead for events"),
	TWOPT_UINT("start-events", g_phold_start_events, "number of initial messages per LP"),
	TWOPT_UINT("stagger", stagger, "Set to 1 to stagger event uniformly across 0 to end time."),
	TWOPT_UINT("memory", optimistic_memory, "additional memory buffers"),
	TWOPT_CHAR("run", run_id, "user supplied run name"),
	TWOPT_END()
};

typedef struct{
	int argc;
	char **argv;
	char **env;
	int thread_id;
}param;

void set_cpu_affinity (int rank)
{
        /*
        int cpuId;
        //pthread_t thread;
        cpu_set_t cpuMask;
        //thread = pthread_self();

        CPU_ZERO(&cpuMask);
        
        cpuId = g_tw_tid;
        
        CPU_SET(cpuId, &cpuMask);

        if (CPU_ISSET(cpuId, &cpuMask) >= 0)
        {
        	if (sched_setaffinity(0, sizeof(cpuMask), &cpuMask) < 0)
           	//status = pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuMask);
            //if(status !=0)
            {
                printf("Rank %d, cpuID %d\n", rank, cpuId);
                perror("NPH: sched_setaffinity\n");
            }
        }
        else perror("Error in Set Affinity\n");
        */

        int cpuId; //, add = 0;
        pthread_t thread;
        cpu_set_t cpuMask;
        thread = pthread_self();

        CPU_ZERO(&cpuMask);
      
        // Leaves last 4 cores empty always
        //if (rank > 59) add += 4;
        //if (rank > 119) add += 4;
        //if (rank > 179) add += 4;
     
        cpuId = rank;// + add; 
        
        CPU_SET(cpuId, &cpuMask);

        int s = pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuMask);
        if (s != 0) perror("\nCPU Affinity Broken\n\n");
}

void set_cpu_affinity_multi_process (int rank)
{
    int cpuId;
    cpu_set_t cpuMask;

    CPU_ZERO(&cpuMask);

    if (ppn * (no_threads + 1) <= 64) cpuId = rank;
    else if (ppn * (no_threads + 1) <= 128) cpuId = rank / 2 + (rank % 2) * 64;
    else if (ppn * (no_threads + 1) <= 192) cpuId = rank / 3 + (rank % 3) * 64;
    else if (ppn * (no_threads + 1) <= 256) cpuId = rank / 4 + (rank % 4) * 64;
    else perror("CPU AFFINITY IF ELSE\n");

    CPU_SET(cpuId, &cpuMask);

    if (CPU_ISSET(cpuId, &cpuMask) >= 0)
    {
        if (sched_setaffinity(0, sizeof(cpuMask), &cpuMask) < 0) perror("CPU AFFINITY IF ELSE\n");
    }
}

volatile int run_flag;

void *
thread_main(void *arg)
{
	int		 i;
	param *p = (param *) arg;
	
	g_tw_n_threads = no_threads;
    g_tw_machines = no_machines;
        
	//printf("Machines = %d, Threads = %d\n", no_machines, no_threads);
    //printf("Setting affinity for thread %d\n", p->thread_id);    

    //set_cpu_affinity(p -> thread_id);
    //DJ:

	mean = g_mean;
	
    // get rid of error if compiled w/ MEMORY queues
    g_tw_memory_nqueues=1;

    my_lookahead = lookahead;
	
    // set a min my_lookahead of 1.0
	my_lookahead = 1.0;

	tw_opt_add(app_opt);
	
	// Thread 0 callocs streamline arrays here
	tw_init(&p->argc, &p->argv, p->thread_id);

        while (!run_flag) usleep(10000);

	if (my_lookahead > 1.0) tw_error(TW_LOC, "Lookahead > 1.0 .. needs to be less\n");

	//reset mean based on my_lookahead
        mean = mean - my_lookahead;

        g_tw_memory_nqueues = 16; // give at least 16 memory queue event

	#ifdef New_map
  		if(g_tw_tid==0)
		{
			nlp_per_pe=minlps;
			offset_lpid=(ttl_lps/(tw_nnodes()/g_tw_n_threads))*(g_tw_mynode/g_tw_n_threads);
		}
		else
		{
			nlp_per_pe=maxlps;
			offset_lpid= (g_tw_mynode/g_tw_n_threads+1)*minlps+(g_tw_mynode-(g_tw_mynode/g_tw_n_threads+1))*maxlps;
		}
	#else
		offset_lpid = g_tw_mynode * nlp_per_pe;
		ttl_lps = tw_nnodes() * g_tw_npe * nlp_per_pe;
	#endif
	// printf("total lps is %d,my thread id is %d,my node id is %d,my lps is %d,my offset is %d\n",ttl_lps,g_tw_tid,g_tw_mynode,
	// nlp_per_pe,offset_lpid); 
 
	// change here(jwang)
	#ifdef MemoryPool
		if (g_tw_tid == 0) g_tw_events_per_pe = ((mult * nlp_per_pe * g_phold_start_events) + optimistic_memory);
		else g_tw_events_per_pe = (mult * nlp_per_pe * g_phold_start_events) + optimistic_memory;
	#else
		#ifdef SEPARATE_1
			if (g_tw_tid > 1) g_tw_events_per_pe = (mult * nlp_per_pe * g_phold_start_events) + optimistic_memory;
			else g_tw_events_per_pe = 0; 
		#elif defined(SEPARATE_0) 
			if (g_tw_tid) g_tw_events_per_pe = (mult * nlp_per_pe * g_phold_start_events) + optimistic_memory;
			else g_tw_events_per_pe = 0; 
		#else
			// events_per_pe = 10,179.  mult (1.4) x nlp (128) = 179
			g_tw_events_per_pe = (mult * nlp_per_pe * g_phold_start_events) + optimistic_memory;
		#endif
	#endif

	//g_tw_rng_default = TW_FALSE;
	g_tw_lookahead = my_lookahead;

	// jwang
	// KPs and LPs initialized here
	#ifdef New_map
		tw_define_lps(nlp_per_pe, sizeof(phold_message), 0,offset_lpid);
	#else
		tw_define_lps(nlp_per_pe, sizeof(phold_message), 0);
	#endif

	for(i = 0; i < g_tw_nlp; i++) tw_lp_settype(i, &mylps[0]);
 
    if (g_tw_mynode == 0)
	{
		printf("Number of Machines is %d\n", no_machines);
		printf("Each spawning %d threads\n\n", no_threads);
  
	    	printf("========================================\n");
	    	printf("PHOLD Model Configuration..............\n");
	    	printf("   Lookahead..................%.2lf\n", my_lookahead);
	    	printf("   Start-events...............%u\n", g_phold_start_events);
	    	printf("   stagger....................%u\n", stagger);
	    	printf("   Mean.......................%.2lf\n", mean);
	   	    printf("   Mult.......................%.2lf\n", mult);
	    	printf("   Memory.....................%u\n", optimistic_memory);
           	printf("   Remote Percentage..........%.0lf\n", percent_remote * 100);
	    	printf("   Regional Percentage........%.0lf\n", percent_regional * 100);
            	printf("   EPC........................%d\n", epg);
            
	    	#ifdef ROSS_GVT_mattern_orig
            	printf("   Mattern GVT................\n");  
           	#endif
	    
            	#ifdef ROSS_GVT_mpi_allreduce
            	printf("   Barrier....................\n");
            	#endif
            
	    	#ifdef SEPARATE_0 
	    	printf("   Thread 0 Separated.........\n");
	    	#else
	    	printf("   Thread 0 Not Separated.....\n");
	    	#endif
	    	
		#ifdef SEPARATE_1 
	    	printf("   Thread 1 Separated.........\n");
	    	#else
	    	printf("   Thread 1 Not Separated.....\n");
	    	#endif
	    
		#ifdef SL_LOCKS
		printf("   Streamlined FC Locks.......\n");
		#endif
		#ifdef SL_BARRIER
		printf("   Streamlined FC Barrier.....\n");
		#endif
		#ifdef SL_GVT 
		printf("   Streamlined GVT............\n");
		#endif	

		#ifdef IMBALANCED_DEST
		printf("   Imbalanced Communication...\n");
		#endif	

		#ifdef IMBALANCED_EPG
		printf("   Imbalanced EPG.............\n");	
		#endif	
		printf("========================================\n\n");
	}
	
    //fprintf(stderr, "g_tw_pid: %d\n", g_tw_pid); // n = 4, -> 0,1,2,3
    // int nodes = no_machines / ppn; // no_machines is number of processes
      
    // Barry`s MCCA code, seems like not working but not sure
    //int cpuid = (g_tw_pid % ppn * (no_threads + 1)) + g_tw_tid;
    //set_cpu_affinity_multi_process(cpuid); 

    set_cpu_affinity(g_tw_tid + (g_tw_pid % ppn) * no_threads);

	tw_run(no_threads, gvt_interval, &epg, &percent_regional, &percent_remote);
	tw_end();
	
	#ifdef switch_sim_mode
        if (g_tw_mynode == 0) printf("	\nFinal EPG: %d, Regional: %.0f, Remote: %.0f\n", 
			      epg, percent_regional * 100, percent_remote * 100);
    #endif

	if (g_tw_mynode == 0) fprintf(stderr, "\n");

	pthread_exit(NULL);
	return NULL;
}

int main(int argc, char **argv, char **env)
{	
	//fprintf(stderr, "%lu\n", sizeof(int)); -> 4
	//fprintf(stderr, "%lu\n", sizeof(tw_stime)); -> 8
	
	if(argc < 6)
        {
            printf("Usage: phold --sync=x threads percent_regional percent_remote epg gvt_interval processes_per_node\n");
            exit(0);
        }
        
	char * num_t = argv[2];
        no_threads = atoi(num_t);
        //real_no_threads = no_threads;
        argc--;

        char * per = argv[3];
        percent_regional = atof(per);
        argc--;

        char * reg = argv[4];
        percent_remote = atof(reg);
        argc--;
      
        char * _epg = argv[5];
        epg = atof(_epg);
        argc--;
      
        char * gvt_int = argv[6];
        gvt_interval = atof(gvt_int);
        argc--;
	
        char * ppn__ = argv[7];
        ppn = atof(ppn__);
        argc--;


	int i;
	pthread_t *threads;
	param p={argc,argv,env,0},*prm;

	threads=(pthread_t *)malloc(no_threads*sizeof(*threads));
	prm=(param *)malloc(sizeof(param)*no_threads);
	//g_tw_n_threads = no_threads;
        //g_tw_machines= machines;

//jwang
#ifdef New_map
	printf("new map policy is used for each thread\n");
#else
	//printf("old map policy is used for each thread\n");
#endif

	//DJ: initialize data-structures needed for barrier synchronization
        init_barrier(&g_barrier,no_threads);
	for(i=0;i<2;i++){
        init_barrier(&sum_barrier[i],no_threads);
        init_barrier(&min_barrier[i],no_threads);
	}
        init_barrier(&max_barrier,no_threads);
	sum_index=0;
	min_index=0;

	//printf("MPI_Init\n");
	mt_mpi_init(&argc,&argv);
        if (MPI_Comm_size(MPI_COMM_WORLD, &no_machines) != MPI_SUCCESS)
            tw_error(TW_LOC, "Cannot get MPI_Comm_size(MPI_COMM_WORLD)");
        //no_machines = 1;

	run_flag = 0;
	for(i=0;i<no_threads;i++){
		prm[i]=p;
		prm[i].thread_id=i;
		pthread_create(&threads[i],NULL,thread_main,(void *)&prm[i]);
	}

        sleep(1);
        run_flag = 1;
	for(i=0;i<no_threads;i++)
		pthread_join(threads[i],NULL);

        //DJ: clean up barrier synchronization data structures
        destroy_barrier(&g_barrier);
	for(i=0;i<2;i++){
        destroy_barrier(&sum_barrier[i]);
        destroy_barrier(&min_barrier[i]);
	}
        destroy_barrier(&max_barrier);
	if (MPI_Finalize() != MPI_SUCCESS)
    	tw_error(TW_LOC, "Failed to finalize MPI");
	
	free(threads);
	free(prm);
	return 0;	
}
