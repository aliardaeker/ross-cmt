#include <ross.h>

static inline void
link_causality (tw_event *nev, tw_event *cev)
{
	nev -> cause_next = cev -> caused_by_me;
	cev -> caused_by_me = nev;
}

void
tw_event_send(tw_event * event)
{
  	tw_lp * src_lp = event -> src_lp;
  	tw_pe * send_pe = src_lp -> pe;
  	tw_pe * dest_pe = NULL;

  	tw_peid	dest_peid = -1;
  	tw_stime recv_ts = event -> recv_ts;

  	if (event == send_pe -> abort_event) 
	{
    		if (recv_ts < g_tw_ts_end) send_pe -> cev_abort = 1;
    		return;
  	}

  	link_causality(event, send_pe -> cur_event);

  	// call LP remote mapping function to get dest_pe
  	dest_peid = (*src_lp -> type.map) ((tw_lpid) event -> dest_lp);


	#ifdef rollback_stats
    		if ((g_tw_mynode/g_tw_n_threads) != (dest_peid/g_tw_n_threads)) send_pe->stats.s_net_remote++;
    		else if (((g_tw_mynode/g_tw_n_threads) == (dest_peid/g_tw_n_threads)) 
			&& g_tw_mynode!=dest_peid) send_pe->stats.s_net_regional++;
	#endif

  	if (tw_node_eq(tw_net_onnode(dest_peid), &g_tw_mynode))
    	{
      		event -> dest_lp = tw_getlocal_lp((tw_lpid) event -> dest_lp);
      		dest_pe = event -> dest_lp -> pe;

      		if (send_pe == dest_pe && event->dest_lp->kp->last_time <= recv_ts)
		{
	  		/* Fast case, we are sending to our own PE and there is
	   		* no rollback caused by this send.  We cannot have any
	   		* transient messages on local sends so we can return.
	   		*/
			send_pe -> stats.s_nsend_loc++;  
			
			//tw_clock s = tw_clock_read();
			tw_pq_enqueue(send_pe->pq, event);
			//send_pe -> stats.s_send_faster_local += tw_clock_read() - s;
	  		//printf("Fast Local: %u\n", event -> event_id);
	  		return;
		} 
		else
		{
	  		/* Slower, but still local send, so put into top of
	   		* dest_pe->event_q. 
	   		*/
	  		event->state.owner = TW_pe_event_q;
			send_pe->stats.s_nsend_loc_remote++;
			
			//tw_clock s = tw_clock_read();
	  		tw_eventq_push(&dest_pe->event_q, event);
			//send_pe -> stats.s_send_slower_local += tw_clock_read() - s;
	 	 	//printf("Slower Local: %u\n", event -> event_id);
		}
    	}
	else
    	{
      		/* Slowest approach of all; this is not a local event.
       		* We need to send it over the network to the other PE
       		* for processing.
       		*/
	
		//printf("In tw_event_send remote send : dest_peid=%d  e->dest_lp=%ld  from lp=%ld: node=%d\n",
		//	(int)dest_peid,(tw_lpid)event->dest_lp,(tw_lpid)event->src_lp->gid,g_tw_mynode);
      
		//fprintf(stderr, "Remote: %u\n", event -> event_id);
		// 	Send PE can be any thread not just thread 0. 

		send_pe -> stats.s_nsend_net_remote++;
      		event -> state.owner = TW_net_asend;
      		tw_net_send(event, 0);
    	}

	if (tw_gvt_inprogress(send_pe)) send_pe -> trans_msg_ts = min(send_pe->trans_msg_ts, recv_ts);
}

static inline void
local_cancel(tw_pe *d, tw_event *event)
{
	event->state.cancel_q = 1;

	//tw_pe * send_pe = event->src_lp->pe;
      	//send_pe -> stats.s_local_cancel++;
	
	event->cancel_next = d->cancel_q;
	d->cancel_q = event;
}

static inline void
event_cancel(tw_event * event)
{
	tw_pe *send_pe = event->src_lp->pe;
	tw_peid dest_peid;

	//printf("****** IN EVENT_CANCEL\n");
	if(event->state.owner == TW_net_asend || event->state.owner == TW_pe_sevent_q)
    	{
      		/* Slowest approach of all; this has to be sent over the
       		* network to let the dest_pe know it shouldn't have seen
       		* it in the first place.
       		*/
      		
		tw_net_cancel(event);
      		//send_pe->stats.s_nsend_net_remote--;

		if(tw_gvt_inprogress(send_pe)) send_pe->trans_msg_ts = min(send_pe->trans_msg_ts, event->recv_ts);
		return;
    	}

  	dest_peid = event->dest_lp->pe->id;

  	if (send_pe->id == dest_peid)
    	{
      		send_pe -> stats.s_local_cancel++;
      		
		switch (event->state.owner) 
		{
      			case TW_pe_pq:
				/* Currently in our pq and not processed; delete it and
	 			* free the event buffer immediately.  No need to wait.
	 			*/
	
				tw_pq_delete_any(send_pe->pq, event);
				tw_event_free(send_pe, event);
				break;

      			case TW_pe_event_q:
      			
			case TW_kp_pevent_q:
				local_cancel(send_pe, event);
		
				if(tw_gvt_inprogress(send_pe)) send_pe->trans_msg_ts = min(send_pe->trans_msg_ts, event->recv_ts);
				break;

      			default:
				tw_error( TW_LOC,
		 			"unknown fast local cancel owner %d",
		 			event->state.owner);
      		}
	} 
    	else if (tw_node_eq(&send_pe->node, tw_net_onnode(dest_peid))) 
	{
    		/* Slower, but still a local cancel, so put into
     		* top of dest_pe->cancel_q for final deletion.
     		*/

		local_cancel(event->dest_lp->pe, event);
      		send_pe -> stats.s_local_cancel++;
		// Never happens

    		if (tw_gvt_inprogress(send_pe)) send_pe->trans_msg_ts = min(send_pe->trans_msg_ts, event->recv_ts);
	} 
	else 
	{
		//jwang
		printf("mynode is %d,event id is %d, send_pe is %llu, and recv_pe is %d\n",
		g_tw_mynode,event->event_id,event->send_pe,event->recv_pe);
    		tw_error(TW_LOC, "Should be remote cancel!");
  	}
}

void
tw_event_rollback(tw_event * event)
{
	tw_event	* e = event->caused_by_me;
	tw_lp		* dest_lp = event->dest_lp;

  	tw_state_rollback(dest_lp, event);
	//printf("****** IN TW_EVENT_ROLLBACK\n");

 	//if(lvt>e->recv_ts)
 	//printf("lvt is %lf, event is %lf, differ is %lf\n",lvt, e->recv_ts,(e->recv_ts-lvt));
  	if (e)
    	{
      		do
		{
	  		tw_event *n = e->cause_next;
	  		e->cause_next = NULL;

	  		event_cancel(e);
	  		e = n;
		} while (e);
     	 	
		event->caused_by_me = NULL;
    	}

	dest_lp -> kp -> s_e_rbs++;
}
